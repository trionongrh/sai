-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2019 at 02:36 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs_audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `acara`
--

CREATE TABLE `acara` (
  `id_acara` int(11) NOT NULL,
  `nama_acara` varchar(100) NOT NULL,
  `id_iso` int(11) NOT NULL,
  `tujuan` text NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `tanggal_visitasi_mulai` date NOT NULL,
  `tanggal_visitasi_selesai` date NOT NULL,
  `status` enum('belum_mulai','sedang_berjalan','selesai') NOT NULL,
  `pertanyaan` enum('0','1') NOT NULL,
  `publish` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acara`
--

INSERT INTO `acara` (`id_acara`, `nama_acara`, `id_iso`, `tujuan`, `tanggal_mulai`, `tanggal_selesai`, `tanggal_visitasi_mulai`, `tanggal_visitasi_selesai`, `status`, `pertanyaan`, `publish`) VALUES
(31, 'TEST UNTUK REKAP', 1, 'asdasd', '2019-03-05', '2019-08-22', '2019-03-07', '2019-09-19', 'sedang_berjalan', '1', '1'),
(34, 'Sqwe', 1, 'qwewqe', '2019-03-20', '2019-03-22', '2019-04-09', '2019-04-18', 'selesai', '1', '1'),
(35, 'asda', 1, 'asdada', '2019-03-20', '2019-03-21', '2019-03-22', '2019-04-01', 'selesai', '1', '0'),
(36, 'asdsa', 1, 'sadad', '2019-03-20', '2019-03-23', '2019-03-20', '2019-03-30', 'selesai', '1', '0'),
(37, 'test1234', 1, 'qweq', '2019-03-20', '2019-04-05', '2019-04-17', '2019-04-25', 'selesai', '1', '1'),
(38, 'adasd', 1, 'ew', '2019-03-28', '2019-03-30', '2019-04-09', '2019-04-17', 'selesai', '1', '0'),
(39, 'asdawqre', 1, 'adadad', '2019-03-28', '2019-03-30', '2019-04-10', '2019-04-12', 'selesai', '1', '0'),
(40, 'zxczxcxzczxcxzzcxxzc ', 1, 'zxcxzcxzcz', '2019-03-18', '2019-03-22', '2019-04-10', '2019-04-19', 'selesai', '1', '0'),
(41, 'otot', 1, 'eoeo', '2019-04-03', '2019-04-19', '2019-04-03', '2019-04-12', 'sedang_berjalan', '1', '1'),
(42, 'qweqew', 1, 'asdada', '2019-04-03', '2019-04-11', '2019-04-19', '2019-04-29', 'selesai', '1', '1'),
(43, 'Test', 1, 'asdasd', '2019-04-06', '2019-04-30', '2019-05-16', '2019-05-25', 'sedang_berjalan', '1', '1'),
(44, 'sdasdsad', 1, 'ewqeweq', '2019-04-07', '2019-04-17', '2019-04-17', '2019-04-27', 'sedang_berjalan', '0', '1'),
(45, 'qweqeqe', 1, 'dsfsfssd', '2019-04-16', '2019-05-17', '2019-04-07', '2019-04-07', 'belum_mulai', '0', '0'),
(46, 'Test 123', 1, 'Test', '2019-04-07', '2019-04-10', '2019-04-08', '2019-04-10', 'selesai', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(255) NOT NULL,
  `nama_bagian` varchar(255) NOT NULL,
  `id_direktorat` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `nama_bagian`, `id_direktorat`) VALUES
(1, 'Keuangan', 1),
(2, 'Sekertariat', 3),
(3, 'testasd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `direktorat`
--

CREATE TABLE `direktorat` (
  `id_direktorat` int(255) NOT NULL,
  `nama_direktorat` varchar(255) NOT NULL,
  `id_kantor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `direktorat`
--

INSERT INTO `direktorat` (`id_direktorat`, `nama_direktorat`, `id_kantor`) VALUES
(1, 'Keuangan', 3),
(2, 'Kemahasiswaan', 3),
(3, 'Public Relation', 3),
(4, 'Pajak', 7),
(5, 'DD', 9);

-- --------------------------------------------------------

--
-- Table structure for table `iso`
--

CREATE TABLE `iso` (
  `id_iso` int(11) NOT NULL,
  `nama_iso` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iso`
--

INSERT INTO `iso` (`id_iso`, `nama_iso`) VALUES
(1, 'ISO 9000'),
(4, 'ISO 1000');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(225) NOT NULL,
  `jawaban` text NOT NULL,
  `status_jawaban` enum('0','1','2','3','4','5') NOT NULL,
  `sotk` varchar(255) NOT NULL,
  `id_pertanyaan` int(225) NOT NULL,
  `status_audit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `jawaban`, `status_jawaban`, `sotk`, `id_pertanyaan`, `status_audit`) VALUES
(4, 'xxee', '1', 'kantor_3', 63, 0),
(5, 'xxee', '1', 'kantor_3', 64, 0),
(9, 'asas', '0', 'kantor_1', 63, 0),
(10, 'asas', '1', 'kantor_1', 64, 0),
(11, 'asda', '0', 'kantor_1', 63, 0),
(12, 'asda', '0', 'kantor_1', 64, 0),
(13, 'tes', '0', 'kantor_1', 78, 0),
(14, 'asda', '1', 'kantor_1', 79, 0),
(15, 'asda', '1', 'kantor_1', 81, 0),
(16, 'asdsa', '0', 'kantor_1', 82, 0),
(17, 'asda', '1', 'kantor_1', 84, 0),
(18, 'gdsdsdasewq', '1', 'kantor_1', 112, 0),
(19, '12314564', '0', 'kantor_1', 119, 0),
(20, '46546789', '1', 'kantor_1', 120, 0),
(21, '123123tughj', '1', 'kantor_1', 121, 0),
(22, 'sasdasd', '1', 'kantor_1', 99, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kantor`
--

CREATE TABLE `kantor` (
  `id_kantor` int(225) NOT NULL,
  `nama_kantor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kantor`
--

INSERT INTO `kantor` (`id_kantor`, `nama_kantor`) VALUES
(1, 'WR1'),
(2, 'WR2'),
(3, 'Universitas Telkom Bandung'),
(4, 'WR4'),
(7, 'WR5'),
(8, 'Hello'),
(9, 'WR13');

-- --------------------------------------------------------

--
-- Table structure for table `klausul`
--

CREATE TABLE `klausul` (
  `id_klausul` int(11) NOT NULL,
  `kode_klausul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_iso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klausul`
--

INSERT INTO `klausul` (`id_klausul`, `kode_klausul`, `deskripsi`, `id_iso`) VALUES
(1, '4.', 'Konteks Organisasi								', 1),
(2, '4.1', 'Memahami organisasi dan konteksnya', 1),
(3, '4.2', 'Memahami kebutuhan dan harapan dari pihak-pihak yang berkepentingan.', 1),
(4, '4.3', 'Menentukan ruang lingkup sistem manajemen mutu', 1),
(5, '4.4', ' Sistem manajemen mutu dan proses-prosesnya', 1),
(6, '4.4.1', ' Organisasi harus menetapkan, menerapkan, memelihara dan terus menerus meningkatkan sistem manajemen mutu, termasuk proses-proses yang diperlukan dan interaksinya, sesuai dengan persyaratan dari Standar Internasional ini.', 1),
(7, '4.4.2', 'Dalam hal yang diperlukan, organisasi harus :\r\na. Memelihara informasi terdokumentasi untuk mendukung operasional proses-proses;\r\nb. Menyimpan informasi terdokumentasi untuk memiliki keyakinan bahwa proses-proses yang sedang dilakukan berjalan seperti yang direncanakan.', 1),
(8, '5.', 'Kepemimpinan', 1),
(9, '5.1', 'Kepemimpinan dan komitmen', 1),
(10, '5.1.1', 'Umum', 1),
(11, '5.1.2', 'Fokus pelanggan', 1),
(12, '5.2', ' Kebijakan', 1),
(13, ' 5.2.1 ', 'Menetapkan kebijakan mutu', 1),
(14, '5.2.2', 'Komunikasi Kebijakan mutu', 1),
(15, ' 5.3', 'Peran Organisasi, tanggung jawab dan otoritas', 1),
(16, '6', 'Perencanaan', 1),
(17, '6.1', 'Tindakan untuk menangani risiko dan peluang', 1),
(18, '6.1.1', 'Ketika merencanakan sistem manajemen mutu, organisasi harus mempertimbangkan isu-isu dimaksud dalam 4.1 dan persyaratan sebagaimana dimaksud dalam 4.2 dan menentukan risiko dan Peluang yang perlu ditujukan kepada:\r\n    a. Berikan jaminan bahwa sistem manajemen mutu dapat mencapai hasil yang diinginkan;								\r\n    b. Meningkatkan dampak yang diinginkan.								\r\n    c. Mencegah, atau mengurangi, dampak yang tidak diinginkan;								\r\n    d. Mencapai peningkatan.								', 1),
(19, '6.1.2', 'Organisasi harus merencanakan:								\r\na. Tindakan untuk menangani risiko dan peluang;								\r\nb. Cara untuk:								\r\n        1. Mengintegrasikan dan menerapkan tindakan ke dalam proses-proses pada sistem manajemen mutu (lihat 4.4);								\r\n        2. Mengevaluasi efektivitas dari tindakan ini.								', 1),
(20, '6.2', 'Sasaran mutu dan perencanaan untuk mencapainya								', 1),
(21, '6.2.1 ', 'Organisasi harus menetapkan sasaran mutu pada fungsi, tingkat dan proses-proses yang dibutuhkan untuk sistem manajemen mutu.								', 1),
(22, '6.2.2 ', 'Ketika merencanakan bagaimana mencapai sasaran mutu, organisasi harus menetapkan:								\r\n      a. Apa yang akan dilakukan;								\r\n      b. Sumber daya apa yang diperlukan;								\r\n      c. Siapa yang akan bertanggung jawab;								\r\n      d. Kapan akan selesai;								\r\n      e. Bagaimana hasilnya akan dievaluasi.								', 1),
(23, '6.3', 'Perencanaan perubahan								', 1),
(24, ' 7.', 'Dukungan.', 1),
(25, ' 7.1', 'Sumber daya.', 1),
(26, '7.1.1', ' Umum.', 1),
(27, '7.1.2 ', ' Orang.', 1),
(28, '7.1.3', 'Infrastruktur								', 1),
(29, '7.1.4 ', 'Lingkungan untuk pengoperasian proses								', 1),
(30, '7.1.5', 'Pemantauan dan pengukuran sumber daya								', 1),
(31, '7.1.5.1', 'Umum								', 1),
(32, '7.1.5.2', 'Ketelusuran Pengukuran								', 1),
(33, '7.1.6', 'Pengetahuan Organisasi								', 1),
(34, '7.2', 'Kompetensi								', 1),
(35, '7.3', ' Kesadaran								', 1),
(36, '7.4', 'Komunikasi								', 1),
(37, '7.5', 'Informasi terdokumentasi								', 1),
(38, '7.5.1', 'Umum								', 1),
(39, '7.5.2', 'Membuat dan memperbarui								', 1),
(40, '7.5.3', 'Pengendalian informasi terdokumentasi								', 1),
(41, '7.5.3.1', 'Informasi terdokumentasi diperlukan oleh sistem manajemen mutu dan Standar Internasional ini harus dikendalikan untuk memastikan:								\r\n        a. Tersedia dan cocok untuk digunakan, di mana dan kapan diperlukan;								\r\n        b. Terlindungi dengan baik (misalnya dari hilangnya kerahasiaan, penggunaan yang tidak benar, atau kehilangan integritas).								', 1),
(42, '7.5.3.2', '\r\nUntuk mengendalikan informasi terdokumentasi, organisasi harus mengikuti kegiatan berikut, sebagaimana berlaku:								\r\n        a. Distribusi, akses, pengambilan dan penggunaan;								\r\n        b. Penyimpanan dan perlindungan, termasuk perlindungan agar tetap terbaca;								\r\n        c. Pengendalian perubahan (misalnya kontrol versi);								\r\n        d. Retensi dan disposisi.								', 1),
(43, '8.', 'Operasional								', 1),
(44, '8.1', 'Perencanaan dan pengendalian operasional								', 1),
(45, '8.2', 'Persyaratan untuk produk dan layanan								', 1),
(46, '8.2.1', 'Komunikasi pelanggan								', 1),
(47, '8.2.2', 'Penentuan persyaratan untuk produk dan layanan.', 1),
(48, '8.2.3', 'Tinjauan persyaratan yang berkaitan dengan produk dan layanan.', 1),
(49, '8.2.3.1', 'Organisasi harus memastikan bahwa memiliki kemampuan untuk memenuhi persyaratan untuk produk dan layanan yang akan ditawarkan kepada pelanggan. Organisasi harus melakukan tinjauan sebelum berkomitmen untuk memasok produk dan layanan kepada pelanggan.', 1),
(50, '8.2.3.2', 'Organisasi harus menyimpan informasi terdokumentasi.', 1),
(51, '8.2.4', 'Perubahan persyaratan untuk produk dan layanan.', 1),
(52, '8.3', 'Desain dan pengembangan produk dan layanan.', 1),
(53, '8.3.1', 'Umum.', 1),
(54, '8.3.2', 'Perencanaan desain dan pengembangan.', 1),
(55, '8.3.3', 'Desain dan pengembangan Input.', 1),
(56, '8.3.4', 'Pengendalian desain dan pengembangan.', 1),
(57, '8.3.5', 'Output desain dan pengembangan.', 1),
(58, '8.3.6', 'Perubahan desain dan pengembangan.', 1),
(59, '8.4', 'Pengendalian produk dan layanan eksternal yang disediakan.', 1),
(60, '8.4.1', 'Umum.', 1),
(61, '8.4.2', 'Jenis dan tingkat pengendalian.', 1),
(62, '8.4.3', 'Informasi untuk penyedia eksternal.', 1),
(63, '8.5', 'Produksi dan penyediaan layanan.', 1),
(64, '8.5.1', 'Pengendalian produksi dan penyediaan layanan.', 1),
(65, '8.5.2', 'Identifikasi dan Penelusuran.', 1),
(66, '8.5.3', 'Barang milik pelanggan atau penyedia eksternal.', 1),
(67, '8.5.4', 'Perlindungan.', 1),
(68, '8.5.5', 'Kegiatan pasca pengiriman.', 1),
(69, '8.5.6', 'Kendali atas perubahan.', 1),
(70, '8.6', 'Pelepasan atas produk dan layanan.	', 1),
(71, '8.7', 'Kendali atas output yang tidak sesuai.', 1),
(72, '8.7.1', 'Organisasi harus memastikan output yang yang tidak sesuai dengan persyaratan diidentifikasi dan dikendalikan untuk mencegah penggunaan atau pengiriman yang tidak disengaja.', 1),
(73, '8.7.2', 'Organisasi harus menyimpan informasi terdokumentasi yang :								\r\n      a. Menggambarkan ketidaksesuaian;\r\n      b. Menggambarkan tindakan yang akan diambil;', 1),
(74, '9.', 'Evaluasi kinerja								', 1),
(75, '9.1', 'Pemantauan, pengukuran, analisis dan evaluasi								', 1),
(76, '9.1.1', 'Umum								', 1),
(77, '9.1.2', 'Kepuasan pelanggan								', 1),
(78, '9.1.3', 'Analisis dan evaluasi								', 1),
(79, '9.2', 'Audit internal								', 1),
(80, '9.2.1', 'Organisasi harus melakukan audit internal pada selang waktu yang direncanakan untuk memberikan informasi apakah sistem manajemen mutu;								', 1),
(81, '9.2.2', 'Organisasi harus:								\r\n      a. Merencanakan, menetapkan, dan memelihara program audit termasuk frekuensi, metode, tanggung jawab, persyaratan perencanaan dan pelaporan, yang harus mempertimbangkan pentingnya proses-proses yang berkaitan, perubahan yang mempengaruhi organisasi, dan hasil audit sebelumnya;\r\n      b. Menentukan kriteria audit dan ruang lingkup untuk setiap audit;\r\n      c. Memilih auditor dan melaksanakan audit untuk memastikan objektivitas dan ketidakberpihakan proses audit;\r\n      d. Memastikan bahwa hasil audit dilaporkan kepada manajemen yang relevan;\r\n      e. Melakukan koreksi yang diperlukan dan tindakan perbaikan tanpa ditunda;	\r\n      f. Menyimpan informasi terdokumentasi sebagai bukti pelaksanaan program audit dan hasil audit.', 1),
(82, '9.3', 'Tinjauan Manajemen								', 1),
(83, '9.3.1', 'Umum								', 1),
(84, '9.3.2', 'Input tinjauan manajemen								', 1),
(85, '9.3.3', 'Output tinjauan manajemen								', 1),
(86, '10.', 'Peningkatan								', 1),
(87, '10.1', 'Umum								', 1),
(88, '10.2', 'Ketidaksesuaian dan tindakan perbaikan								', 1),
(89, '10.2.1', 'Ketika ketidaksesuaian terjadi, termasuk setiap keluhan yang muncul								', 1),
(90, '10.2.2', 'Organisasi harus menyimpan informasi terdokumentasi sebagai bukti:\r\n      a.  Sifat dari ketidaksesuaian dan tindakan berikutnya yang diambil;\r\n      b. Hasil tindakan perbaikan.', 1),
(91, '10.3', 'Peningkatan terus-menerus.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_session`
--

CREATE TABLE `login_session` (
  `uid` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('admin','auditor','auditee') NOT NULL,
  `sotk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_session`
--

INSERT INTO `login_session` (`uid`, `username`, `password`, `level`, `sotk`) VALUES
(1, 'admin', 'admin', 'admin', ''),
(2, 'auditor', 'auditor', 'auditor', ''),
(10, 'kantor1', 'kantor1', 'auditee', 'kantor_1'),
(11, 'bagian1', 'bagian1', 'auditee', 'bagian_2'),
(12, 'bagian2', 'bagian2', 'auditee', 'bagian_1'),
(13, 'kantor2', 'kantor2', 'auditee', 'kantor_2'),
(14, 'kantor3', 'kantor3', 'auditee', 'kantor_3');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `id_kantor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `id_kantor`) VALUES
(2, 'Bill', 7),
(4, 'Gates', 1),
(6, 'Bill', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id_pertanyaan` int(225) NOT NULL,
  `pertanyaan` text NOT NULL,
  `id_acara` int(11) NOT NULL,
  `id_klausul` int(11) NOT NULL,
  `target_auditee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id_pertanyaan`, `pertanyaan`, `id_acara`, `id_klausul`, `target_auditee`) VALUES
(63, 'tesasd', 31, 1, '[\"kantor_1\",\"kantor_3\",\"direktorat_2\",\"direktorat_3\",\"bagian_1\",\"bagian_2\"]'),
(73, 'sad', 34, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(74, 'asda', 35, 67, '[\"kantor_1\",\"kantor_3\"]'),
(75, 'sds', 37, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(76, 'asda', 37, 3, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(77, 'asda', 37, 8, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(78, 'sadad', 41, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(79, 'asdsa', 41, 1, '[\"kantor_1\",\"kantor_3\",\"kantor_9\"]'),
(80, 'asda', 41, 2, '[\"kantor_2\",\"kantor_3\"]'),
(81, 'asda', 41, 2, '[\"kantor_1\",\"kantor_3\"]'),
(82, 'sdafa', 41, 3, '[\"kantor_1\",\"kantor_3\"]'),
(83, 'dasdas', 41, 3, '[\"kantor_2\",\"kantor_3\"]'),
(84, 'asda', 41, 19, '[\"kantor_1\",\"kantor_3\"]'),
(86, 'asdsad', 42, 1, '[\"kantor_1\",\"kantor_3\"]'),
(87, 'asda', 42, 17, '[\"kantor_1\",\"kantor_3\"]'),
(92, 'asdasd', 43, 2, '[\"kantor_2\",\"kantor_3\"]'),
(99, 'yuyuyuyu', 43, 1, '[\"kantor_1\",\"direktorat_2\",\"direktorat_3\",\"direktorat_4\",\"bagian_1\",\"bagian_2\"]'),
(102, 'asdas', 31, 1, '[\"kantor_4\",\"kantor_7\"]'),
(105, 'asdad', 31, 11, '[\"kantor_3\"]'),
(106, 'asdasd', 31, 11, '[\"kantor_3\"]'),
(107, 'asdasd', 31, 11, '[\"kantor_3\"]'),
(108, 'asdad', 31, 14, '[\"kantor_2\"]'),
(109, 'asdasdeqw', 31, 40, '[\"kantor_3\"]'),
(110, 'qewqeqwe', 31, 30, '[\"kantor_3\"]'),
(111, 'gffaes', 44, 1, '[\"kantor_2\",\"kantor_4\"]'),
(112, 'afffsda', 44, 2, '[\"kantor_1\",\"kantor_7\"]'),
(116, '13232321s', 44, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\",\"kantor_4\",\"kantor_7\"]'),
(118, 'sadsadsad', 44, 8, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(119, 'egtdgfdgfchg', 46, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(120, 'etrextdygdghfghyvfufyub', 46, 1, '[\"kantor_1\",\"kantor_2\",\"kantor_3\"]'),
(121, 'tfyufvytfvggjyfjydvvjhdgh', 46, 91, '[\"kantor_1\",\"kantor_2\"]'),
(122, 'cfcvgdvvyvfcy ', 46, 91, '[\"kantor_2\",\"kantor_3\"]');

-- --------------------------------------------------------

--
-- Table structure for table `risk`
--

CREATE TABLE `risk` (
  `id_risk` int(11) NOT NULL,
  `no_reg` varchar(255) NOT NULL,
  `id_prosedur` int(11) NOT NULL,
  `id_bagian` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `resiko` text NOT NULL,
  `harapan` text NOT NULL,
  `dampak` text NOT NULL,
  `pencegahan` text NOT NULL,
  `probabilitas` int(11) NOT NULL,
  `keparahan` int(11) NOT NULL,
  `kategori` enum('rendah','sedang','tinggi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `risk`
--

INSERT INTO `risk` (`id_risk`, `no_reg`, `id_prosedur`, `id_bagian`, `pic`, `resiko`, `harapan`, `dampak`, `pencegahan`, `probabilitas`, `keparahan`, `kategori`) VALUES
(1, 'REG-0001-4160', 4, 1, 'budi', 'tes2323', 'tes2', 'tes1', 'begitu', 2, 3, 'sedang'),
(3, 'REG-0001-4145', 4, 1, 'Andilaw', 'resiko1', 'harapan1', 'dampak1', 'ee', 1, 1, 'rendah'),
(4, 'REG-0001-4288', 4, 1, 'Sariyo', 'tess', 'tesss', 'asdad', 'begini', 3, 2, 'tinggi'),
(5, 'REG-0002-2890', 1, 2, 'Sudira', 'kjkdsjdk', 'aksdjlakjdlk', 'awewlkqjelkqjelk', 'ajkeajlkes', 1, 3, 'rendah'),
(6, 'REG-0002-4109', 1, 2, 'budi anduk', 'slslsl', 'a.a;a;', 'mdsma,', 'wlqkelqw;ke', 1, 2, 'rendah');

-- --------------------------------------------------------

--
-- Table structure for table `risk_prosedur`
--

CREATE TABLE `risk_prosedur` (
  `id_prosedur` int(11) NOT NULL,
  `nama_prosedur` varchar(255) NOT NULL,
  `id_bagian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `risk_prosedur`
--

INSERT INTO `risk_prosedur` (`id_prosedur`, `nama_prosedur`, `id_bagian`) VALUES
(1, 'prosedur 4', 2),
(4, 'Prosedur penanggulangan', 1),
(7, 'Test', 2),
(8, 'Tes1', 2),
(10, 'Tee', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting_score`
--

CREATE TABLE `setting_score` (
  `id_score` int(11) NOT NULL,
  `nilai_score` int(11) NOT NULL,
  `label_score` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_score`
--

INSERT INTO `setting_score` (`id_score`, `nilai_score`, `label_score`) VALUES
(1, 0, '0'),
(2, 1, '1'),
(3, 2, '2'),
(4, 3, '3'),
(5, 4, '4'),
(6, 5, '5');

-- --------------------------------------------------------

--
-- Table structure for table `temuan`
--

CREATE TABLE `temuan` (
  `id_temuan` int(11) NOT NULL,
  `id_jawaban` int(11) NOT NULL,
  `temuan` text NOT NULL,
  `pic` varchar(255) NOT NULL,
  `perbaikan` varchar(255) NOT NULL,
  `tenggat_mulai` date NOT NULL,
  `tenggat_selesai` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temuan`
--

INSERT INTO `temuan` (`id_temuan`, `id_jawaban`, `temuan`, `pic`, `perbaikan`, `tenggat_mulai`, `tenggat_selesai`) VALUES
(1, 4, 'tes', '', 'tes', '2019-03-05', '2019-03-14'),
(2, 9, 'tes', 'asdaeweqe', 'asde wece', '2019-04-03', '2019-04-25'),
(5, 10, 'testesewqeqeqwe', 'tesss', 'qewqeqe', '2019-04-03', '2019-04-18'),
(6, 5, 'sadad', 'budi', 'qweqwe', '2019-04-03', '2019-04-18'),
(7, 19, 'test', 'test', 'test', '2019-04-08', '2019-04-18'),
(11, 20, 'asda', 'ewwq', 'ewewq', '2019-04-08', '2019-04-08'),
(12, 21, 'qweqwr', 'qweqwe', '31231', '2019-04-08', '2019-04-08');

-- --------------------------------------------------------

--
-- Table structure for table `urusan`
--

CREATE TABLE `urusan` (
  `id_urusan` int(225) NOT NULL,
  `nama_urusan` text NOT NULL,
  `id_bagian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urusan`
--

INSERT INTO `urusan` (`id_urusan`, `nama_urusan`, `id_bagian`) VALUES
(1, 'Bendahara', 1),
(2, 'Asisten', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acara`
--
ALTER TABLE `acara`
  ADD PRIMARY KEY (`id_acara`);

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `direktorat`
--
ALTER TABLE `direktorat`
  ADD PRIMARY KEY (`id_direktorat`);

--
-- Indexes for table `iso`
--
ALTER TABLE `iso`
  ADD PRIMARY KEY (`id_iso`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `kantor`
--
ALTER TABLE `kantor`
  ADD PRIMARY KEY (`id_kantor`);

--
-- Indexes for table `klausul`
--
ALTER TABLE `klausul`
  ADD PRIMARY KEY (`id_klausul`);

--
-- Indexes for table `login_session`
--
ALTER TABLE `login_session`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indexes for table `risk`
--
ALTER TABLE `risk`
  ADD PRIMARY KEY (`id_risk`);

--
-- Indexes for table `risk_prosedur`
--
ALTER TABLE `risk_prosedur`
  ADD PRIMARY KEY (`id_prosedur`);

--
-- Indexes for table `setting_score`
--
ALTER TABLE `setting_score`
  ADD PRIMARY KEY (`id_score`);

--
-- Indexes for table `temuan`
--
ALTER TABLE `temuan`
  ADD PRIMARY KEY (`id_temuan`);

--
-- Indexes for table `urusan`
--
ALTER TABLE `urusan`
  ADD PRIMARY KEY (`id_urusan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acara`
--
ALTER TABLE `acara`
  MODIFY `id_acara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id_bagian` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `direktorat`
--
ALTER TABLE `direktorat`
  MODIFY `id_direktorat` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `iso`
--
ALTER TABLE `iso`
  MODIFY `id_iso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `kantor`
--
ALTER TABLE `kantor`
  MODIFY `id_kantor` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `klausul`
--
ALTER TABLE `klausul`
  MODIFY `id_klausul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `login_session`
--
ALTER TABLE `login_session`
  MODIFY `uid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id_pertanyaan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `risk`
--
ALTER TABLE `risk`
  MODIFY `id_risk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `risk_prosedur`
--
ALTER TABLE `risk_prosedur`
  MODIFY `id_prosedur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `setting_score`
--
ALTER TABLE `setting_score`
  MODIFY `id_score` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `temuan`
--
ALTER TABLE `temuan`
  MODIFY `id_temuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `urusan`
--
ALTER TABLE `urusan`
  MODIFY `id_urusan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `cek_status_acara(selesai)` ON SCHEDULE EVERY 1 SECOND STARTS '2018-12-09 21:57:01' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Ubah status selesai apabila tanggal selesai <= tanggal sekarang' DO UPDATE
    `acara`
SET
    `acara`.`status` = "selesai"
WHERE
    `acara`.`tanggal_selesai` <= CURRENT_DATE()
    AND
    `acara`.`status` = "sedang_berjalan"$$

CREATE DEFINER=`root`@`localhost` EVENT `cek_status_acara(berjalan)` ON SCHEDULE EVERY 1 SECOND STARTS '2018-12-09 21:52:21' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Ubah status acara taggal selesai hari ini' DO UPDATE
    `acara`
SET
    `acara`.`status` = "sedang_berjalan"
WHERE
    `acara`.`tanggal_mulai` <= CURRENT_DATE()
    AND
    `acara`.`status` = "belum_mulai"$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
