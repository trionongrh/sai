<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();

		$this->SetHeaderData('', '0', 'SATUAN AUDIT INTERNAL', 'Telkom University', array(0,0,0), array(0,0,0));
		$this->setFooterData(array(0,0,0), array(0,0,0));

		// set header and footer fonts
		$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set margins
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */