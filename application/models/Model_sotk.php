<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sotk extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('kantor');
		$this->db->order_by('id_kantor','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// read perkategori_produk
	public function read($nama_kantor){
		$query = $this->db->get_where('kantor',array('nama_kantor'  => $nama_kantor));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_kantor){
		$query = $this->db->get_where('kantor',array('id_kantor'  => $id_kantor));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('kantor',$data);
	}

	// Tambah
	public function tambahreturnID ($data) {
		$this->db->insert('kantor',$data);
		return $this->db->insert_id();
	}

	// Delete SOTK
	public function deleteSOTK($sotk){
		$query="";
		if(strpos($sotk, 'kantor')!==false){
			$query = "
					DELETE kantor,direktorat,bagian,urusan
						FROM kantor
						LEFT JOIN direktorat ON direktorat.id_kantor=kantor.id_kantor
						LEFT JOIN bagian ON bagian.id_direktorat=direktorat.id_direktorat
						LEFT JOIN urusan ON urusan.id_bagian=bagian.id_bagian
					WHERE kantor.id_kantor = ".explode('_',$sotk)[1];
		}else if(strpos($sotk, 'direktorat')!==false){
			$query = "
					DELETE direktorat,bagian,urusan
						FROM direktorat
						LEFT JOIN bagian ON bagian.id_direktorat=direktorat.id_direktorat
						LEFT JOIN urusan ON urusan.id_bagian=bagian.id_bagian
					WHERE direktorat.id_direktorat = ".explode('_',$sotk)[1];
		}else if(strpos($sotk, 'bagian')!==false){
			$query = "
					DELETE bagian,urusan
						FROM bagian
						LEFT JOIN urusan ON urusan.id_bagian=bagian.id_bagian
					WHERE bagian.id_bagian = ".explode('_',$sotk)[1];
		}else if(strpos($sotk, 'urusan')!==false){
			$query = "
					DELETE
						FROM urusan
					WHERE urusan.id_urusan = ".explode('_',$sotk)[1];
		}
		return $this->db->query($query);
	}

	public function getSOTK($sotk){
		$s = explode("_",$sotk);
		$this->db->select('nama_'.$s[0].' as nama_sotk, "'.$s[0].'" as tabel');
		$this->db->from($s[0]);
		$this->db->where('id_'.$s[0], $s[1]);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function getSOTKparent($sotk){
		$s = explode("_",$sotk);
		$this->db->select('nama_'.$s[0].' as nama_sotk, "'.$s[0].'" as tabel');
		$this->db->from($s[0]);
		$this->db->where('id_'.$s[0], $s[1]);
		$query = $this->db->get();
		return $query->row_array();
	}


	public function getlistSOTK_tabel($tabel){
		$this->db->select('*');
		$this->db->from($tabel);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getlistSOTK_fromkantor($id_kantor){
		$this->db->select('kantor.id_kantor, kantor.nama_kantor as kantor, direktorat.id_direktorat, direktorat.nama_direktorat as direktorat, bagian.id_bagian, bagian.nama_bagian as bagian, urusan.id_urusan, urusan.nama_urusan as urusan');
		$this->db->from('kantor');
		$this->db->join('direktorat', 'direktorat.id_kantor=kantor.id_kantor', 'left');
		$this->db->join('bagian', 'direktorat.id_direktorat=bagian.id_direktorat', 'left');
		$this->db->join('urusan', 'bagian.id_bagian=urusan.id_bagian', 'left');
		$this->db->where('kantor.id_kantor', $id_kantor);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getlistSOTK_fromkantor2($id_kantor){
		$this->db->select('*');
		$this->db->from('kantor');
		$this->db->where('id_kantor', $id_kantor);
		$query = $this->db->get();
		$data['kantor'] = $query->row_array();

		$this->db->select('*');
		$this->db->from('direktorat');
		$this->db->where('id_kantor', $id_kantor);
		$query = $this->db->get();
		if(!empty($query->result_array())){
			$data['direktorat'] = $query->result_array();
			$list_id_direktorat = '';
			foreach ($data['direktorat'] as $i => $v) {
				$list_id_direktorat .= 'id_direktorat="'.$v['id_direktorat'].'" ';
				if($i!=sizeof($data['direktorat'])-1){
					$list_id_direktorat .= 'OR ';
				}
			}

			$this->db->select('*');
			$this->db->from('bagian');
			$this->db->where($list_id_direktorat);
			$query = $this->db->get();
			if(!empty($query->result_array())){
				$data['bagian'] = $query->result_array();
				$list_id_bagian = '';
				foreach ($data['bagian'] as $i => $v) {
					$list_id_bagian .= 'id_bagian="'.$v['id_bagian'].'" ';
					if($i!=sizeof($data['bagian'])-1){
						$list_id_bagian .= 'OR ';
					}
				}

				$this->db->select('*');
				$this->db->from('urusan');
				$this->db->where($list_id_bagian);
				$query = $this->db->get();
				$data['urusan'] = $query->result_array();

			}
		}

		return $data;
	}

	public function getlistSOTK_fromall(){
		$this->db->select('kantor.id_kantor, kantor.nama_kantor as kantor, direktorat.id_direktorat, direktorat.nama_direktorat as direktorat, bagian.id_bagian, bagian.nama_bagian as bagian, urusan.id_urusan, urusan.nama_urusan as urusan');
		$this->db->from('kantor');
		$this->db->join('direktorat', 'direktorat.id_kantor=kantor.id_kantor', 'left');
		$this->db->join('bagian', 'direktorat.id_direktorat=bagian.id_direktorat', 'left');
		$this->db->join('urusan', 'bagian.id_bagian=urusan.id_bagian', 'left');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getSOTK_by_id_table($id,$tabel){
		if($tabel=='direktorat'){
			$this->db->select('nama_direktorat, id_kantor, "'.$tabel.'" as tabel');
		}else if($tabel=='bagian'){
			$this->db->select('nama_bagian, id_direktorat, "'.$tabel.'" as tabel');
		}else if($tabel=='urusan'){
			$this->db->select('nama_urusan, id_bagian, "'.$tabel.'" as tabel');
		}else{
			$this->db->select('nama_'.$tabel.', "'.$tabel.'" as tabel');
		}
		$this->db->from($tabel);
		$this->db->where('id_'.$tabel, $id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function editsotk($tabel,$data){
		$this->db->where('id_'.$tabel,$data['id_'.$tabel]);
		$this->db->update($tabel,$data);
		return ($this->db->affected_rows() > 0);
	}

	public function listingall_fromkantor($id_kantor = '', $q = ''){
		$res = array();
		$this->db->select('id_kantor as id, nama_kantor as identifier, "kantor" as tabel');
		$this->db->from('kantor');
		$this->db->where('id_kantor', $id_kantor);
		if($q != ''){
			$this->db->like('LOWER(nama_kantor)', strtolower($q));
		}
		$query = $this->db->get();
		$data['kantor'] = $query->result_array();
		foreach ($data['kantor'] as $v) {
			array_push($res, $v);
		}


		
		$this->db->select('id_direktorat as id, nama_direktorat as identifier, "direktorat" as tabel');
		$this->db->from('direktorat');
		$this->db->where('id_kantor', $id_kantor);
		if($q != ''){
			$this->db->like('LOWER(nama_direktorat)', strtolower($q));
		}
		$query = $this->db->get();
		if(!empty($query->result_array())){
			$data['direktorat'] = $query->result_array();
			$list_id_direktorat = '';
			foreach ($data['direktorat'] as $i => $v) {
					array_push($res, $v);
				$list_id_direktorat .= 'id_direktorat="'.$v['id'].'" ';
				if($i!=sizeof($data['direktorat'])-1){
					$list_id_direktorat .= 'OR ';
				}
			}

			if($q != ''){
				$this->db->like('LOWER(nama_bagian)', strtolower($q));
			}
			$this->db->select('id_bagian as id, nama_bagian as identifier, "bagian" as tabel');
			$this->db->from('bagian');
			$this->db->where($list_id_direktorat);
			$query = $this->db->get();
			if(!empty($query->result_array())){
				$data['bagian'] = $query->result_array();
				$list_id_bagian = '';
				foreach ($data['bagian'] as $i => $v) {
					array_push($res, $v);
					$list_id_bagian .= 'id_bagian="'.$v['id'].'" ';
					if($i!=sizeof($data['bagian'])-1){
						$list_id_bagian .= 'OR ';
					}
				}

				if($q != ''){
					$this->db->like('LOWER(nama_urusan)', strtolower($q));
				}
				$this->db->select('id_urusan as id, nama_urusan as identifier, "urusan" as tabel');
				$this->db->from('urusan');
				$this->db->where($list_id_bagian);
				$query = $this->db->get();
				$data['urusan'] = $query->result_array();
				foreach ($data['urusan'] as $v) {
					array_push($res, $v);
				}

			}
		}

		return $res;
	}

	public function listingall($q = ''){
		$sql = "
		SELECT * FROM
		( SELECT
			id_kantor as id,
			nama_kantor as identifier,
			'kantor' as tabel
		FROM kantor
		UNION ALL
		SELECT
			id_direktorat as id,
			nama_direktorat as identifier,
			'direktorat' as tabel
		FROM direktorat
		UNION ALL
		SELECT
			id_bagian as id,
			nama_bagian as identifier,
			'bagian' as tabel
		FROM bagian 
		UNION ALL
		SELECT
			id_urusan as id,
			nama_urusan as identifier,
			'urusan' as tabel
		FROM urusan ) as target_audit
		".(($q!='')? 'WHERE identifier LIKE "%'.$q.'%"' : '');

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function listingkantor($q = ''){
		$sql = "SELECT * FROM (SELECT id_kantor as id, nama_kantor as identifier, 'kantor' as tabel FROM kantor) as target_audit ".(($q!='')? 'WHERE identifier LIKE "%'.$q.'%"' : '');

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function listingdirektorat($q = ''){
		$sql = "SELECT * FROM (SELECT id_direktorat as id, nama_direktorat as identifier, 'direktorat' as tabel FROM direktorat) as target_audit ".(($q!='')? 'WHERE identifier LIKE "%'.$q.'%"' : '');

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function listingbagian($q = ''){
		$sql = "SELECT * FROM (SELECT id_bagian as id, nama_bagian as identifier, 'bagian' as tabel FROM bagian) as target_audit ".(($q!='')? 'WHERE identifier LIKE "%'.$q.'%"' : '');

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function listingurusan($q = ''){
		$sql = "SELECT * FROM (SELECT id_urusan as id, nama_urusan identifier, 'urusan' as tabel FROM urusan) as target_audit ".(($q!='')? 'WHERE identifier LIKE "%'.$q.'%"' : '');

		$query = $this->db->query($sql);
		return $query->result_array();
	}


}