<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sotk1 extends CI_Model {
	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('kantor');
		$this->db->order_by('id_kantor','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// read perkategori_produk
	public function read($nama_kantor){
		$query = $this->db->get_where('kantor',array('nama_kantor'  => $nama_kantor));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_kantor){
		$query = $this->db->get_where('kantor',array('id_kantor'  => $id_kantor));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('kantor',$data);
	}
	
	// Edit 
	public function edit ($data) {
		$this->db->where('id_kantor',$data['id_kantor']);
		$this->db->update('kantor',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_kantor',$data['id_kantor']);
		$this->db->delete('kantor',$data);
	}
}