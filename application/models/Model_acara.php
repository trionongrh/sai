<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_acara extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}


	public function listing($by_status = '', $publish = '') {
		$this->db->select('acara.*, iso.nama_iso');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso','left');
		if($by_status!= ''){
			$this->db->where('acara.status', $by_status);
		}
		if($publish!= ''){
			$this->db->where('acara.publish', $publish);
		}
		$this->db->order_by('FIELD(acara.status, "sedang_berjalan","belum_mulai","selesai"), id_acara DESC');
		
		//$this->db->order_by('acara.status', 'ASC'); 
		$query = $this->db->get();
		return $query->result();
	}


	public function getAcara($id_acara) {
		$this->db->select('*');
		$this->db->from('acara');
		$this->db->where('id_acara', $id_acara);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function listing_auditee() {
		$this->db->select('acara.*, iso.nama_iso');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->where('publish','1');
		$this->db->where('status','sedang_berjalan');
		$this->db->order_by('id_acara','ASC');
		$query = $this->db->get();
		return $query->result();
		
	}

	public function listing_auditee_sotk($sotk) {
		$this->db->select('acara.*, iso.nama_iso, pertanyaan.id_pertanyaan, pertanyaan.target_auditee');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('pertanyaan', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->where('acara.publish','1');
		$this->db->where('acara.status','sedang_berjalan');
		$this->db->like('pertanyaan.target_auditee', $sotk);
		$this->db->order_by('acara.id_acara','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// read perkategori_produk
	public function read($nama_acara){
		$query = $this->db->get_where('acara',array('nama_acara'  => $nama_acara));
		return $query->row();
	}

	public function detailacara($id_acara){
		$query = $this->db->get_where('acara',array('id_acara'  => $id_acara));
		return $query->row();
	}
	
	// detail klausul
	public function detailklausul($id_acara){
		$this->db->select('acara.*, iso.nama_iso, klausul.*, CAST(klausul.kode_klausul AS UNSIGNED) AS kode');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('klausul', 'klausul.id_iso=iso.id_iso');
		$this->db->where('acara.id_acara', $id_acara);
		$this->db->order_by('kode','ASC');
		$query = $this->db->get('acara');
		// echo $this->db->last_query();
		return $query->result();
	}
	
	// Tambah
	public function tambahacara($data) {
		$this->db->insert('acara',$data);
	}



	// Tambah
	public function tambahreturnID ($data) {
		$this->db->insert('acara',$data);
		return $this->db->insert_id();
	}
	
	// Edit 
	public function editacara($idacara,$data) {
		$this->db->where('id_acara',$idacara);
		$this->db->update('acara',$data);
	}
	
	// Delete
	public function deleteacara($id_acara){
		$table = array('acara','pertanyaan');
		$this->db->where('id_acara',$id_acara);
		$this->db->delete($table);
	}

	public function deleteacaranew($id_acara){
		$query = "DELETE acara,pertanyaan,jawaban,temuan
						FROM acara
						LEFT JOIN pertanyaan ON pertanyaan.id_acara=acara.id_acara
						LEFT JOIN jawaban ON jawaban.id_pertanyaan=pertanyaan.id_pertanyaan
						LEFT JOIN temuan ON temuan.id_jawaban=jawaban.id_jawaban
					WHERE acara.id_acara = ".$id_acara;
		return $this->db->query($query);
	}
}