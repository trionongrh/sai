<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pertanyaan extends CI_Model {


	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function detail_pertanyaan($id_acara) {
		$this->db->select('pertanyaan.*, klausul.*');
		$this->db->from('pertanyaan');
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$this->db->order_by('id_pertanyaan','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail_pertanyaan_klausul($id_acara,$id_klausul) {
		$this->db->select('pertanyaan.*');
		$this->db->where('id_acara',$id_acara);
		$this->db->where('id_klausul',$id_klausul);
		$this->db->order_by('id_pertanyaan','ASC');
		$query = $this->db->get('pertanyaan');
		return $query->result_array();
	}

	public function detail_pertanyaan_edit($id_acara) {
		$this->db->select('pertanyaan.*, klausul.*');
		$this->db->from('acara');
		$this->db->join('iso','iso.id_iso=acara.id_iso','left');
		$this->db->join('klausul','iso.id_iso=klausul.id_iso','left');
		$this->db->join('pertanyaan','pertanyaan.id_acara=acara.id_acara AND pertanyaan.id_klausul=klausul.id_klausul','left');
		$this->db->where('acara.id_acara',$id_acara);
		$this->db->order_by('klausul.id_klausul','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail_pertanyaan_auditee($id_acara) {
		$this->db->select('acara.id_acara, acara.nama_acara, acara.publish, iso.nama_iso, klausul.id_klausul, klausul.kode_klausul, klausul.deskripsi, pertanyaan.*, CAST(klausul.kode_klausul AS UNSIGNED) AS kode');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('pertanyaan', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$this->db->order_by('kode','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail_pertanyaan_auditee_new($id_acara,$sotk) {
		$this->db->select('acara.id_acara, acara.nama_acara, acara.publish, iso.nama_iso, klausul.id_klausul, klausul.kode_klausul, klausul.deskripsi, pertanyaan.*, CAST(klausul.kode_klausul AS UNSIGNED) AS kode');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('pertanyaan', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$this->db->like('pertanyaan.target_auditee',$sotk);
		$this->db->order_by('kode','ASC');
		$query = $this->db->get();
		return $query->result();
	}


	public function detail_jawaban_auditee($id_acara) {
		$this->db->select('acara.id_acara, acara.nama_acara, acara.publish, iso.nama_iso, klausul.id_klausul, klausul.kode_klausul, klausul.deskripsi, pertanyaan.*,jawaban.id_jawaban, jawaban.jawaban, jawaban.status_jawaban');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('pertanyaan', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->join('jawaban', 'jawaban.id_pertanyaan=pertanyaan.id_pertanyaan');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$this->db->order_by('id_pertanyaan','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function target_audit(){
		$sql = "
		SELECT * FROM 
		( SELECT
			id_kantor as id,
			nama_kantor as identifier,
			'kantor' as tabel
		FROM kantor
		UNION ALL
		SELECT
			id_direktorat as id,
			nama_direktorat as identifier,
			'direktorat' as tabel
		FROM direktorat
		UNION ALL
		SELECT
			id_bagian as id,
			nama_bagian as identifier,
			'bagian' as tabel
		FROM bagian ) as target_audit
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function target_audit_choose($id,$table){
		$sql = "
		SELECT * FROM
		( SELECT
			id_kantor as id,
			nama_kantor as identifier,
			'kantor' as tabel
		FROM kantor
		UNION ALL
		SELECT
			id_direktorat as id,
			nama_direktorat as identifier,
			'direktorat' as tabel
		FROM direktorat
		UNION ALL
		SELECT
			id_bagian as id,
			nama_bagian as identifier,
			'bagian' as tabel
		FROM bagian ) as target_audit
		WHERE id = ".$id." AND tabel = '".$table."'
		";

		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	// Tambah
	public function tambah_pertanyaan($data) {
		$this->db->insert('pertanyaan',$data);
	}

	// Edit 
	public function editpertanyaan($idpertanyaan,$data) {
		$this->db->where('id_pertanyaan',$idpertanyaan);
		$this->db->update('pertanyaan',$data);
	}

	public function delete_pertanyaan($idpertanyaan) {
		$this->db->where_in('id_pertanyaan',$idpertanyaan);
		$this->db->delete('pertanyaan');
	}
	
}