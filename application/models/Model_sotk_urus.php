<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sotk_urus extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('urusan');
		$this->db->order_by('id_urusan','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function listingwhere($id_bagian) {
		$this->db->select('*');
		$this->db->from('urusan');
		$this->db->order_by('id_urusan','ASC');
		$this->db->where('id_bagian',$id_bagian);
		$query = $this->db->get();
		return $query->result();
	}


	// read perkategori_produk
	public function read($nama_urusan){
		$query = $this->db->get_where('urusan',array('nama_urusan'  => $nama_urusan));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_urusan){
		$query = $this->db->get_where('urusan',array('id_urusan'  => $id_urusan));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('urusan',$data);
	}
	
	// Edit 
	public function edit ($data) {
		$this->db->where('id_urusan',$data['id_urusan']);
		$this->db->update('urusan',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_urusan',$data['id_urusan']);
		$this->db->delete('urusan',$data);
	}
	public function listingall ($id_kantor) {
		$this->db->select('bagian.nama_bagian,bagian.id_bagian');
		$this->db->from('bagian');
		// Join
		$this->db->join('direktorat','direktorat.id_direktorat = bagian.id_direktorat');
		$this->db->join('kantor','kantor.id_kantor = direktorat.id_kantor');
		$this->db->where('kantor.id_kantor',$id_kantor);
		// End join
		$this->db->order_by('id_bagian','ASC');
		$query = $this->db->get();
		return $query->result();
	}
}