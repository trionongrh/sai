<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jawab extends CI_Model {


	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function insertjawaban($data){
		$this->db->insert('jawaban',$data);	
	}

	public function updatejawaban($idjawaban,$data){
		$this->db->where('id_jawaban',$idjawaban);
		$this->db->update('jawaban',$data);
	}

	public function cek_jawaban_auditee($sotk){
		$this->db->select('jawaban.*, pertanyaan.*');
		$this->db->from('jawaban');
		$this->db->join('pertanyaan', 'pertanyaan.id_pertanyaan=jawaban.id_pertanyaan');
		$this->db->where('sotk',$sotk);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function cek_jawaban_auditee_by_pertanyaan($id_pertanyaan,$sotk){
		$this->db->select('jawaban.*, pertanyaan.*');
		$this->db->from('jawaban');
		$this->db->join('pertanyaan', 'pertanyaan.id_pertanyaan=jawaban.id_pertanyaan');
		$this->db->where('jawaban.id_pertanyaan',$id_pertanyaan);
		$this->db->where('jawaban.sotk',$sotk);
		$query = $this->db->get();
		return $query->row_array();
	}



	public function cek_jawaban_auditee_by_pertanyaan2($id_pertanyaan,$sotk){
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_pertanyaan' => $id_pertanyaan))->row_array();
		$data['jawaban'] = $this->db->get_where('jawaban', array('id_pertanyaan' => $id_pertanyaan, 'sotk' => $sotk))->row_array();
		return $data;
	}
	
}