<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_risk extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function tambahProsedur($data){
		return $this->db->insert('risk_prosedur', $data);
	}

	public function listingProsedur(){
		$this->db->select('*');
		$this->db->from('risk_prosedur');
		$query = $this->db->get();
		return $query->result();
	}

	public function listingProsedurAuditee($id){
		$this->db->select('*');
		$this->db->from('risk_prosedur');
		$this->db->where('id_bagian', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function listingBagian(){
		$this->db->select('*');
		$this->db->from('bagian');
		$query = $this->db->get();
		return $query->result();
	}

	public function listingRisk($no_reg = '' , $bag = ''){
		$this->db->select('risk.*, risk_prosedur.nama_prosedur, bagian.nama_bagian');
		$this->db->from('risk');
		$this->db->join('risk_prosedur', 'risk.id_prosedur=risk_prosedur.id_prosedur');
		$this->db->join('bagian', 'bagian.id_bagian=risk.id_bagian');
		if($no_reg != ''){
			$this->db->or_like('LOWER(no_reg)', $no_reg);
		}
		if($bag != ''){
			$this->db->where('risk.id_bagian', $bag);
		}
		$this->db->order_by('risk.no_reg','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function listingRiskAuditee($id, $no_reg = '' ){
		$this->db->select('risk.*,risk_prosedur.nama_prosedur, bagian.nama_bagian');
		$this->db->from('risk');
		$this->db->join('risk_prosedur', 'risk.id_prosedur=risk_prosedur.id_prosedur');
		$this->db->join('bagian', 'bagian.id_bagian=risk_prosedur.id_bagian');
		if($no_reg != ''){
			$this->db->like('LOWER(no_reg)', $no_reg);
		}
		$this->db->where('risk_prosedur.id_bagian', $id);
		$this->db->order_by('risk.no_reg','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function tambahRisk($data){
		$this->db->insert('risk', $data);
		return ($this->db->affected_rows() > 0);
	}

	public function getRisk($id_risk){
		$this->db->select('risk.*,risk_prosedur.nama_prosedur,bagian.nama_bagian');
		$this->db->from('risk');
		$this->db->join('risk_prosedur', 'risk.id_prosedur=risk_prosedur.id_prosedur');
		$this->db->join('bagian', 'risk.id_bagian=bagian.id_bagian');
		$this->db->where('risk.id_risk', $id_risk);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function getRisk_by_REG($reg){
		$this->db->select('risk.*,risk_prosedur.nama_prosedur');
		$this->db->from('risk');
		$this->db->join('risk_prosedur', 'risk.id_prosedur=risk_prosedur.id_prosedur');
		$this->db->like('no_reg', $reg);
		$query = $this->db->get();
		return $query->result();
	}

	public function editRisk($id_risk,$data){
		$this->db->where('id_risk',$id_risk);
		$this->db->update('risk',$data);
		return ($this->db->affected_rows() > 0);
	}

	public function deleteRisk($id_risk){
		$this->db->where('id_risk',$id_risk);
		$this->db->delete('risk');
		return ($this->db->affected_rows() > 0);
	}
}