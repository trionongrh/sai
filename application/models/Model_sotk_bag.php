<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sotk_bag extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('bagian');
		$this->db->order_by('id_bagian','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function listingwhere($id_direktorat) {
		$this->db->select('*');
		$this->db->from('bagian');
		$this->db->order_by('id_bagian','ASC');
		$this->db->where('id_direktorat',$id_direktorat);
		$query = $this->db->get();
		return $query->result();
	}


	// read perkategori_produk
	public function read($nama_bagian){
		$query = $this->db->get_where('bagian',array('nama_bagian'  => $nama_bagian));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_bagian){
		$query = $this->db->get_where('bagian',array('id_bagian'  => $id_bagian));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('bagian',$data);
	}
	
	// Edit 
	public function edit ($data) {
		$this->db->where('id_bagian',$data['id_bagian']);
		$this->db->update('bagian',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_bagian',$data['id_bagian']);
		$this->db->delete('bagian',$data);
	}
}