<?php
class Model_rekap extends CI_Model
{
	public function getRekap($id_jawaban){
		$this->db->where('id_jawaban', $id_jawaban);
		$query = $this->db->get('temuan');
		return $query->row_array();
	}

	public function getAcaraRekap($sotk){
		$this->db->join('jawaban', 'jawaban.id_jawaban=temuan.id_jawaban');
		$this->db->join('pertanyaan', 'jawaban.id_pertanyaan=pertanyaan.id_pertanyaan');
		$this->db->join('acara', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->where('jawaban.sotk', $sotk);
		$acara = $this->db->get('temuan')->result_array();

		$data['acara'] = array();
		$last_acara = 0;
		foreach ($acara as $i => $v) {
			if($v['id_acara']!=$last_acara){
				array_push($data['acara'], $v['id_acara']);	
				$last_acara = $v['id_acara'];
			}
		}

		// print_r($data['acara']);
		if(sizeof($data['acara']) > 0){
			$this->db->join('iso', 'acara.id_iso=iso.id_iso');
			foreach ($data['acara'] as $i => $value) {
				$this->db->or_where('id_acara', $value);
			}
			$this->db->order_by('id_acara','DESC');
			$data = $this->db->get('acara')->result_array();


			return $data;
		} else {
			return array();
		}
	}
}