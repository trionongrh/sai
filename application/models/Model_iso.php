<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_iso extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('iso');
		$this->db->order_by('id_iso','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	//Listing
	public function listingalliso() {
		$this->db->select('*');
		$this->db->from('iso');
		$this->db->order_by('iso.id_iso','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}


	public function listingallklausul($id_iso) {
		$this->db->select('*, CAST(kode_klausul AS UNSIGNED) AS kode');
		$this->db->from('klausul');
		$this->db->where('id_iso', $id_iso);
		$this->db->order_by('kode','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getISO($id_iso){
		$this->db->select('*');
		$this->db->from('iso');
		$this->db->where('id_iso', $id_iso);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function getKlausul($id_iso){
		$this->db->select('*, CAST(kode_klausul AS UNSIGNED) AS kode');
		$this->db->from('klausul');
		$this->db->where('id_iso', $id_iso);
		$this->db->order_by('kode','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Edit 
	public function editiso($data) {
		$this->db->where('id_iso',$data['id_iso']);
		$this->db->update('iso',$data);
		return ($this->db->affected_rows() > 0);
	}

	// Hapus
	public function hapusiso($id_iso) {
		$this->db->where('id_iso',$id_iso);
		$this->db->delete('klausul');
		$this->db->where('id_iso',$id_iso);
		$this->db->delete('iso');
		return ($this->db->affected_rows() > 0);
	}


	//Klausul
	public function editklausul($data) {
		$this->db->where('id_klausul',$data['id_klausul']);
		$this->db->update('klausul',$data);
		return ($this->db->affected_rows() > 0);
	}
	
	public function tambahklausul($data) {
		$this->db->insert('klausul',$data);
		return ($this->db->affected_rows() > 0);
	}

	public function deleteklausul($id_klausul){
		$this->db->where('id_klausul',$id_klausul);
		$this->db->delete('klausul');
		return ($this->db->affected_rows() > 0);
	}
	//End Klausul

	// read perkategori_produk
	public function read($nama_iso){
		$where = array('nama_iso'  => $nama_iso);
		$query = $this->db->get_where('iso',$where);
		return $query->row();
	}
	
	// Tambah
	public function tambah($data) {
		$this->db->insert('iso',$data);
	}

	// Tambah
	public function tambahreturnID ($data) {
		$this->db->insert('iso',$data);
		return $this->db->insert_id();
	}
	
	// Delete
	public function delete($data){
		$this->db->where('id_iso',$data['id_iso']);
		$this->db->delete('iso',$data);
	}
	public function readklausul($kode_klausul){
		$query = $this->db->get_where('klausul',array('kode_klausul'  => $kode_klausul));
		return $query->row();
	}
}