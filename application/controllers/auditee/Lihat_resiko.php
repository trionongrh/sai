<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_resiko extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee" || explode('_', $this->session->userdata('sotk'))[0] != 'bagian') {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_risk');
	}
	public function index()
	{

		if($this->input->get('no_reg')!=''){
			$data['risk'] = $this->model_risk->listingRiskAuditee($this->session->userdata('sotk_name')['id'], $this->input->get('no_reg'));
		}else {
			$data['risk'] = $this->model_risk->listingRiskAuditee($this->session->userdata('sotk_name')['id'], '');
		}
		
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_resiko';
		$this->load->view('auditee/header');
		$this->load->view('auditee/lihat_resiko',$data);
		$this->load->view('auditee/footer', $footer);
	}

	public function update($id_risk)
	{
		$data['risk'] = $this->model_risk->getRisk($id_risk);
		$data['risk']['id_risk'] = $id_risk;
 		$data['prosedur'] = $this->model_risk->listingProsedur();


		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_resiko';
		$this->load->view('auditee/header');
		$this->load->view('auditee/edit_resiko',$data);
		$this->load->view('auditee/footer', $footer);
	}
	public function edit()
	{
		$inputget = $this->input->post();
		$id_risk = $inputget['id_risk'];
		$data = $inputget;
		unset($data['id_risk']);

		if($this->model_risk->editRisk($id_risk,$data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data risk telah diubah.</p>
              </div>');
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Data risk gagal diubah.</p>
              </div>');
		}
		redirect('auditee/lihat_resiko');
	}
}