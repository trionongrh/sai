<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit_visitasi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_jawab');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
        $acara = $this->model_acara->listing_auditee_sotk($_SESSION['sotk']);
		$data['acara'] = array();
		foreach ($acara as $i => $v) {
			$data['acara'][$v['id_acara']] = array(
				'id_acara' 			=> $v['id_acara'],
				'nama_acara' 		=> $v['nama_acara'],
				'id_iso'			=> $v['id_iso'],
				'tujuan'			=> $v['tujuan'],
				'tanggal_mulai'		=> $v['tanggal_mulai'],
				'tanggal_selesai'	=> $v['tanggal_selesai'],
				'status'			=> $v['status'],
				'pertanyaan'		=> $v['pertanyaan'],
				'publish'			=> $v['publish'],
				'nama_iso'			=> $v['nama_iso']
			);
			foreach ($acara as $j => $v2) {
				$data['acara'][$v['id_acara']]['target_auditee'][$j] = $v2['target_auditee'];
			}
		};
		$jawaban = $this->model_jawab->cek_jawaban_auditee($_SESSION['sotk']);
		foreach ($jawaban as $i => $v) {
			$data['jawaban'][$v['id_acara']] = array();
  		}

 		$footer['url_page'] = 'audit_visitasi';
		$this->load->view('auditee/header');
		$this->load->view('auditee/audit_visitasi',$data);
		$this->load->view('auditee/footer',$footer);
    }
}