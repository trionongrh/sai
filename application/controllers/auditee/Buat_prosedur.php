<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class buat_prosedur extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee" || explode('_', $this->session->userdata('sotk'))[0] != 'bagian') {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_risk');
	}
	public function index()
	{

		$this->db->join('bagian', 'bagian.id_bagian=risk_prosedur.id_bagian');
		if($this->input->get('pros') != ''){
			$this->db->where('bagian.id_bagian', $this->input->get('pros'));
		}
		$this->db->order_by('risk_prosedur.id_bagian', 'ASC');
		$data['prosedur'] = $this->db->get_where('risk_prosedur', array('risk_prosedur.id_bagian' => $this->session->userdata('sotk_name')['id']))->result_array();
		// echo '<pre>';
		// var_dump($this->session->userdata());
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'buat_prosedur';
		$this->load->view('auditee/header');
		$this->load->view('auditee/buat_prosedur', $data);
		$this->load->view('auditee/footer', $footer);
	}
	public function buat(){
		$inputpost = $this->input->post();
		// echo '<pre>';
		// var_dump($inputpost);
		// echo '</pre>';
		// exit();
		$data = array(
			'nama_prosedur' => $inputpost['nama_prosedur'],
			'id_bagian' => $inputpost['id_bagian']
		);
		if($this->model_risk->tambahprosedur($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah ditambahkan.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Error</h4>
		        <p>Data prosedur gagal ditambahkan.</p>
		      </div>');

		}
		redirect('auditee/buat_prosedur');

	}

	public function delete($id){
		if($this->db->delete('risk_prosedur', array('id_prosedur' => $id))){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah dihapus.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Sukses</h4>
		        <p>Data prosedur gagal dihapus.</p>
		      </div>');

		}
		redirect('auditee/buat_prosedur');
	}

	public function update(){
		$inputpost = $this->input->post();
		$id = $inputpost['id_prosedur'];
		$data = array(
			'nama_prosedur' => $inputpost['nama_prosedur'], 
			'id_bagian' => $inputpost['id_bagian']
		);

		$this->db->where('id_prosedur', $id);
		if($this->db->update('risk_prosedur', $data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah diubah.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Sukses</h4>
		        <p>Data prosedur gagal diubah.</p>
		      </div>');
		}

		redirect('auditee/buat_prosedur');
	}
}
