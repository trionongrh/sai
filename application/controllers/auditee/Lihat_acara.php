<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_jawab');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$acara = $this->model_acara->listing_auditee_sotk($_SESSION['sotk']);
		$data['acara'] = array();
		foreach ($acara as $i => $v) {
			$data['acara'][$v['id_acara']] = array(
				'id_acara' 			=> $v['id_acara'],
				'nama_acara' 		=> $v['nama_acara'],
				'id_iso'			=> $v['id_iso'],
				'tujuan'			=> $v['tujuan'],
				'tanggal_mulai'		=> $v['tanggal_mulai'],
				'tanggal_selesai'	=> $v['tanggal_selesai'],
				'status'			=> $v['status'],
				'pertanyaan'		=> $v['pertanyaan'],
				'publish'			=> $v['publish'],
				'nama_iso'			=> $v['nama_iso']
			);
			foreach ($acara as $j => $v2) {
				$data['acara'][$v['id_acara']]['target_auditee'][$j] = $v2['target_auditee'];
			}
		};
		$jawaban = $this->model_jawab->cek_jawaban_auditee($_SESSION['sotk']);
		foreach ($jawaban as $i => $v) {
			$data['jawaban'][$v['id_acara']] = array();
  		}
		// echo '<pre>';
		// var_dump($jawaban);
		// echo '</pre>';
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		
 		$footer['url_page'] = 'lihat_acara';
		$this->load->view('auditee/header');
		$this->load->view('auditee/lihat_acara', $data);
		$this->load->view('auditee/footer', $footer);
	}
	public function audit_pertanyaan($id_acara){
		
		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan_auditee_new($id_acara,$this->session->userdata('sotk'));
		foreach ($pertanyaan as $i => $v) {
			$data['acara'] = array(
				'id_acara' => $v->id_acara,
				'nama_acara' => $v->nama_acara,
				'nama_iso'	=> $v->nama_iso,
				'publish' => $v->publish,
			);
			$data['klausul'][$v->id_klausul] = array(
				'kode_klausul' => $v->kode_klausul,
				'deskripsi' => $v->deskripsi,
			);
			foreach ($pertanyaan as $j => $w) {
				$data['klausul'][$w->id_klausul]['pertanyaan'][$w->id_pertanyaan] = array(
					'pertanyaan' => $w->pertanyaan,
					'target_auditee' => $w->target_auditee
				);
			}
		}


		// Untuk Debugging
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		if(isset($data['acara'])){
			if($data['acara']['publish'] == '0') {
				redirect('auditee/lihat_acara');
				exit();
			}
		}else{
			redirect('auditee/lihat_acara');
			exit();
		}

		$footer['url_page'] = 'audit_pertanyaan';
		$this->load->view('auditee/header');
		$this->load->view('auditee/audit_pertanyaan-bak', $data);
		$this->load->view('auditee/footer', $footer);
	}

	public function simpan_selected_jawaban(){
		$inputpost = $this->input->post();

		$data = array(
			'id_pertanyaan' => $inputpost['id_pertanyaan'][0],
			'status_jawaban' => $inputpost['statusjawab'][0],
			'jawaban' => $inputpost['jawaban'][0],
			'sotk' => $this->session->userdata('sotk'),
		);



		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		if($this->db->insert('jawaban',$data)){
			$this->session->set_flashdata('msg-simpan-'.$data['id_pertanyaan'], '<span id="notif_alert" class="badge bg-aqua">Jawaban Tersimpan</span>');
		}else{

		}
		redirect('auditee/lihat_acara/audit_pertanyaan/'.$inputpost['id_acara']);
	}

	public function update_selected_jawaban(){
		$inputpost = $this->input->post();

		$data = array(
			'status_jawaban' => $inputpost['statusjawab'][0],
			'jawaban' => $inputpost['jawaban'][0],
		);

		// echo '<pre>';
		// print_r($inputpost);
		// echo '</pre>';
		// exit();

		$this->db->where('id_jawaban', $inputpost['id_jawaban']);
		if($this->db->update('jawaban',$data)){
			$this->session->set_flashdata('msg-edit-'.$inputpost['id_jawaban'], '<span id="notif_alert" class="badge bg-green">Jawaban Diperbaharui</span>');
		}else{

		}
		redirect('auditee/lihat_acara/audit_pertanyaan/'.$inputpost['id_acara']);
	}

	public function update_audit_pertanyaan($id_acara){
		
		$pertanyaan = $this->model_pertanyaan->detail_jawaban_auditee($id_acara);
		foreach ($pertanyaan as $i => $v) {
			$data['acara'] = array(
				'id_acara' => $v->id_acara,
				'nama_acara' => $v->nama_acara,
				'nama_iso'	=> $v->nama_iso,
				'publish' => $v->publish,
			);
			$data['klausul'][$v->id_klausul] = array(
				'kode_klausul' => $v->kode_klausul,
				'deskripsi' => $v->deskripsi,
			);
			foreach ($pertanyaan as $j => $w) {
				$data['klausul'][$w->id_klausul]['pertanyaan'][$w->id_pertanyaan] = array(
					'pertanyaan' => $w->pertanyaan,
					'id_jawaban' => $w->id_jawaban,
					'jawaban' => $w->jawaban,
					'status_jawaban' => $w->status_jawaban,
					'target_auditee' => $w->target_auditee
				);
			}
		}
		if(isset($data['acara'])){
			if($data['acara']['publish'] == '0') {
				redirect('auditee/lihat_acara');
				exit();
			}
		}else{
			redirect('auditee/lihat_acara');
			exit();
		}

		// Untuk Debugging
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'update_audit_pertanyaan';
		$this->load->view('auditee/header');
		$this->load->view('auditee/update_audit_pertanyaan', $data);
		$this->load->view('auditee/footer', $footer);
	}

	public function input_audit(){
		$inputpost = $this->input->post();
		foreach ($inputpost['id_pertanyaan'] as $i => $v) {
			$data = array(
				'jawaban' => $inputpost['jawaban'][$i],
				'status_jawaban' => $inputpost['statusjawab'][$i],
				'sotk' => $_SESSION['sotk'],
				'id_pertanyaan' => $v,
			);
			$this->model_jawab->insertjawaban($data);
		}
		redirect('auditee/lihat_acara');
	}

	public function update_audit(){
		$inputpost = $this->input->post();

		// // Untuk Debugging
		// echo '<pre>';
		// var_dump($inputpost);
		// echo '</pre>';
		// exit();

		foreach ($inputpost['id_jawaban'] as $i => $v) {
			$data = array(
				'jawaban' => $inputpost['jawaban'][$i],
				'status_jawaban' => $inputpost['statusjawab'][$i]
			);
			$this->model_jawab->updatejawaban($v,$data);
		}
		redirect('auditee/lihat_acara');
	}
}