<?php


class Hasil_audit extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('Login');
		}

		// echo '<pre>';
		// print_r($this->session->userdata());
		// exit();
		$this->load->helper('text');
		$this->load->model('model_rekap');
		$this->load->model('model_acara');
	}
	public function index()
	{
		$data['acara'] = $this->model_rekap->getAcaraRekap($this->session->userdata('sotk'));
		// $data['acara'] =>


		// echo '<pre>';
		// print_r($data);

		$this->load->view('auditee/header');
		$this->load->view('auditee/audit_acara',$data);
		$this->load->view('auditee/footer');
	}


	public function acara($id_acara){
		$this->db->select('temuan.*, acara.*, jawaban.*, klausul.*, pertanyaan.pertanyaan as pty');
		$this->db->where('acara.id_acara', $id_acara);
		$this->db->where('jawaban.sotk', $this->session->userdata('sotk'));
		$this->db->join('pertanyaan', 'pertanyaan.id_acara=acara.id_acara');
		$this->db->join('klausul', 'pertanyaan.id_klausul=klausul.id_klausul');
		$this->db->join('jawaban', 'pertanyaan.id_pertanyaan=jawaban.id_pertanyaan');
		$this->db->join('temuan', 'temuan.id_jawaban=jawaban.id_jawaban');
		$data['temuan'] = $this->db->get('acara')->result_array();


		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$this->load->view('auditee/header');
		$this->load->view('auditee/audit_temuan',$data);
		$this->load->view('auditee/footer');

	}

	public function temuan($id_temuan){
		$this->db->where('id_temuan', $id_temuan);
		$data['rekap'] = $this->db->get('temuan')->row_array();

		$data['jawaban'] = $this->db->get_where('jawaban', array('id_jawaban' => $data['rekap']['id_jawaban']))->row_array();

		$data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_pertanyaan' => $data['jawaban']['id_pertanyaan']))->row_array();

		$data['acara'] = $this->db->get_where('acara', array('id_acara' => $data['pertanyaan']['id_acara']))->row_array();

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// echo sizeof($data['acara']);
		// exit();

		if(sizeof($data['acara']) > 0){
			$this->load->view('auditee/header');
			$this->load->view('auditee/audit_temuan_detail',$data);
			$this->load->view('auditee/footer');
		}else{
			redirect('auditee/hasil_audit');
		}
	}


	public function download($id_temuan){
		$this->load->library('Pdf');
		
		$this->db->where('id_temuan', $id_temuan);

		$data['rekap'] = $this->db->get('temuan')->row_array();

		$data['jawaban'] = $this->db->get_where('jawaban', array('id_jawaban' => $data['rekap']['id_jawaban']))->row_array();

		$data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_pertanyaan' => $data['jawaban']['id_pertanyaan']))->row_array();

		$data['klausul'] = $this->db->get_where('klausul', array('id_klausul' => $data['pertanyaan']['id_klausul']))->row_array();

		$data['acara'] = $this->db->get_where('acara', array('id_acara' => $data['pertanyaan']['id_acara']))->row_array();

		$data['panduan'] = array(
			'judul' => 'Prosedur Operasional /Dokumen Mutu : ',
			'isi' => array('Cek pegawai, seberapa paham mereka terkait dengan service policy dan bagaimana mereka menginternalisasi kebijakan tersebut kedalam fungsi atau kebijakan mereka.','Dokumen kebijakan penyelenggaraan layanan sudah disahkan dan disosialisasikan/dikomunikasikan dengan seluruh fungsi di internal SISFO'),
		);
		$data['panduan']['isi'] = array('','');


		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false); // constructor class library Pdf -> TCPDF

        $pdf->SetTitle('Temuan');
        $pdf->SetHeaderMargin('10');
        $pdf->SetFooterMargin('10');
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Satuan Audit Internal | Auditee');
        $pdf->SetDisplayMode('real', 'default');

        $pdf->AddPage();
		$html = $this->load->view('template_pdf/tes', $data, true);
		$pdf->writeHTML($html, true, false, true, false, '');


        $pdf->Output('temuan_'.strtolower($data['acara']['nama_acara']).'_'.date("d-m-Y").'.pdf');
	}
	

	public function download_all($id_acara){
		$this->load->library('Pdf');

		$sotk = $this->session->userdata('sotk');
		$data['pertanyaan'] = $this->db->like('target_auditee', $sotk)->get_where('pertanyaan', array('id_acara' => $id_acara))->result_array();
		$id_pertanyaan = array();
		$loop= array();
		foreach ($data['pertanyaan'] as $key => $value) {
			array_push($id_pertanyaan, $value['id_pertanyaan']);
		}


		$this->db->where('sotk', $sotk);
		$this->db->where_in('id_pertanyaan', $id_pertanyaan);
		$data['jawaban'] = $this->db->get('jawaban')->result_array();
		$id_jawaban = array();
		foreach ($data['jawaban'] as $key => $value) {
			array_push($id_jawaban, $value['id_jawaban']);
		}


		$this->db->where_in('id_jawaban', $id_jawaban);
		$data['rekap'] = $this->db->get('temuan')->result_array();
		foreach ($data['rekap'] as $key => $value) {
			array_push($loop, 'e');
		}

		$data['klausul'] = array();
		foreach ($data['pertanyaan'] as $key => $value) {
			array_push($data['klausul'], $this->db->get_where('klausul', array('id_klausul' => $value['id_klausul']))->row_array());
		}

		$data['panduan'] = array(
			'judul' => 'Prosedur Operasional /Dokumen Mutu : ',
			'isi' => array('Cek pegawai, seberapa paham mereka terkait dengan service policy dan bagaimana mereka menginternalisasi kebijakan tersebut kedalam fungsi atau kebijakan mereka.','Dokumen kebijakan penyelenggaraan layanan sudah disahkan dan disosialisasikan/dikomunikasikan dengan seluruh fungsi di internal SISFO'),
		);
		$data['panduan']['isi'] = array('','');



		$data['acara'] = $this->db->get_where('acara', array('id_acara' => $id_acara))->row_array();

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Temuan Acara '.$data['acara']['nama_acara']);
        $pdf->SetHeaderMargin('10');
        $pdf->SetFooterMargin('10');
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Satuan Audit Internal | Auditee');
        $pdf->SetDisplayMode('real', 'default');

        foreach ($loop as $key => $value) {
        	$data_push['pertanyaan'] = $data['pertanyaan'][$key];
        	$data_push['jawaban'] = $data['jawaban'][$key];
        	$data_push['klausul'] = $data['klausul'][$key];
        	$data_push['rekap'] = $data['rekap'][$key];
        	$data_push['panduan'] = $data['panduan'];
        	$data_push['acara'] = $data['acara'];
	        $pdf->AddPage();
			$html = $this->load->view('template_pdf/tes', $data_push, true);
			$pdf->writeHTML($html, true, false, true, false, '');
        }
        $pdf->Output('temuan_'.$id_acara.'_('.$sotk.')_'.date("d-m-Y").'.pdf');
	}
}