<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_resiko extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee" || explode('_', $this->session->userdata('sotk'))[0] != 'bagian') {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_risk');
	}
	public function index()
	{
		$data['prosedur'] = $this->model_risk->listingProsedurAuditee($this->session->userdata('sotk_name')['id']);
		$footer['url_page'] = 'buat_resiko';
		$this->load->view('auditee/header');
		$this->load->view('auditee/buat_resiko', $data);
		$this->load->view('auditee/footer', $footer);
	}
	public function input_resiko(){
		$inputpost = $this->input->post();
		$lengkode = strlen($inputpost['id_bagian']);
		$kode = '';
		if($lengkode==1){
			$kode = '000'.$inputpost['id_bagian'];
		}else if($lengkode==2){
			$kode = '00'.$inputpost['id_bagian'];
		}else if($lengkode==3){
			$kode = '0'.$inputpost['id_bagian'];
		}else if($lengkode==4){
			$kode = $inputpost['id_bagian'];
		}

		$reg = 'REG-'.$kode.'-'.$this->randString(4);

		$data = array(
			'no_reg' => $reg,
			'id_bagian' => $inputpost['id_bagian'],
			'pic' => $inputpost['pic'],
			'id_prosedur' => $inputpost['prosedur'],
			'resiko' => $inputpost['resiko'],
			'harapan' => $inputpost['harapan'],
			'dampak' => $inputpost['dampak'],
			'probabilitas' => $inputpost['probabilitas'],
			'keparahan' => $inputpost['keparahan'],
			'pencegahan' => $inputpost['rencana_pencegahan'],
			'kategori' => $inputpost['kategori'],
		);

		if($this->model_risk->tambahRisk($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Resiko <strong>'.$data['no_reg'].'</strong> telah ditambahkan.</p>
              </div>');
		} else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Resiko <strong>'.$data['no_reg'].'</strong> gagal ditambahkan.</p>
              </div>');
		}
		redirect('auditee/buat_resiko');
	}
	private function randString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
}
