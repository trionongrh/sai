<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_sotk extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->model('model_sotk');
	}

	public function index()
	{
		if($this->input->get('sotk')==''){
			if($this->input->get('kantor')!=''){
				$data['sotk'] = $this->model_sotk->listingall_fromkantor($this->input->get('kantor'), $this->input->get('q'));
			}else{
				$data['sotk'] = $this->model_sotk->listingall($this->input->get('q'));
			}
		}else if($this->input->get('sotk')=='kantor'){
			$data['sotk'] = $this->model_sotk->listingkantor($this->input->get('q'));
		}else if($this->input->get('sotk')=='direktorat'){
			$data['sotk'] = $this->model_sotk->listingdirektorat($this->input->get('q'));
		}else if($this->input->get('sotk')=='bagian'){
			$data['sotk'] = $this->model_sotk->listingbagian($this->input->get('q'));
		}else if($this->input->get('sotk')=='urusan'){
			$data['sotk'] = $this->model_sotk->listingurusan($this->input->get('q'));
		}
		// $test1 = $this->model_sotk->listingall_fromkantor('7');

		// echo '<pre>';
		// var_dump($test1);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'lihat_sotk';
		$this->load->view('auditor/header');
		$this->load->view('auditor/lihat_sotk',$data);
		$this->load->view('auditor/footer', $footer);
	}

	public function download(){
		$this->load->library('Pdf');

		if($this->input->get('kantor')!=''){
			$data['sotk'] = $this->model_sotk->getlistSOTK_fromkantor2($this->input->get('kantor'));
			
			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			// exit();
    		$this->load->view('template_pdf/kantor_sotk_pdf', $data);
		}else{
			$data['test'] = $this->model_sotk->getlistSOTK_fromall();
	        $this->load->view('template_pdf/all_sotk_pdf', $data);
		}
	}

}