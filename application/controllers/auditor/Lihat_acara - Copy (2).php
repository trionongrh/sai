<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing();
		$footer['url_page'] = 'lihat_acara';
		$this->load->view('auditor/header');
		$this->load->view('auditor/lihat_acara', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function buat_pertanyaan($id_acara){

		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '1') {
				redirect('auditor/lihat_acara');
				exit();
			}
		}else{
			redirect('auditor/lihat_acara');
			exit();
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);

		$data['targetaudit'] = $this->model_pertanyaan->target_audit();
		// echo '<pre>';
		// var_dump($data['targetaudit']);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_pertanyaan';
		$this->load->view('auditor/header');
		$this->load->view('auditor/buat_pertanyaan', $data);
		$this->load->view('auditor/footer', $footer);

	}

	public function input_pertanyaan(){
		$inputpost = $this->input->post();
		$size = $inputpost['sizeklausul'];


		// echo '<pre>';
		// print_r($inputpost);
		// echo '</pre>';
		// exit();

		$this->model_acara->editacara($inputpost['id_acara'],array('pertanyaan' =>  '1'));
		for($x = 0; $x<$size; $x++){
			if(isset($inputpost['targetaudit'.$x])) {
				foreach ($inputpost['pertanyaan'.$x] as $i => $v) {
				print_r($v);
					$data = array(
						'id_klausul' => $inputpost['id_klausul'.$x], 
						'id_acara' => $inputpost['id_acara'], 
						'pertanyaan' => $v, 
						'target_auditee' => json_encode($inputpost['targetaudit'.$x][$i]), 
					);
					$this->model_pertanyaan->tambah_pertanyaan($data);
				}
			}
		}
		redirect('auditor/lihat_acara');
	}

	public function lihat_pertanyaan($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('auditor/lihat_acara');
				exit();
			}
		}else{
			redirect('auditor/lihat_acara');
			exit();
		}

		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);

		$temp = array (
			'id' => '',
			'tabel' => ''
		);
		foreach ($pertanyaan as $i => $val) {
			$data['klausul']['kode_klausul'][$val->id_klausul] = $val->kode_klausul;
			$data['klausul']['desc_klausul'][$val->id_klausul] = $val->deskripsi;
			$data['klausul']['pertanyaan'][$val->id_klausul][$val->id_pertanyaan] = $val->pertanyaan;
			$targetaudit[$i] = json_decode($val->target_auditee);
			foreach ($targetaudit[$i] as $j => $tgt) {
				$temp['id'] = explode("_", $tgt)[1];
				$temp['tabel'] = explode("_", $tgt)[0];
				$data['klausul']['targetaudit'][$val->id_klausul][$i]['sotk'][$j] = $this->model_pertanyaan->target_audit_choose($temp['id'],$temp['tabel']);
			}
		}
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'lihat_pertanyaan';
		$this->load->view('auditor/header');
		$this->load->view('auditor/lihat_pertanyaan', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function share($id_acara){
		$this->model_acara->editacara($id_acara,array('publish' =>  '1'));
		redirect('auditor/lihat_acara');
	}

	public function ubah_pertanyaan($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('auditor/lihat_acara');
				exit();
			}
		}else{
			redirect('auditor/lihat_acara');
			exit();
		}

		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan_edit($id_acara);

		$temp = array (
			'id' => '',
			'tabel' => ''
		);

		$x = 0;
		foreach ($pertanyaan as $i => $val) {
			$data['klausul'][$x]['id_klausul'] = $val->id_klausul;
			$data['klausul'][$x]['kode_klausul'] = $val->kode_klausul;
			$data['klausul'][$x]['desc_klausul'] = $val->deskripsi;
			$data['klausul'][$x]['pertanyaan'][$val->id_pertanyaan] = $val->pertanyaan;
			$targetaudit[$i] = json_decode($val->target_auditee);
			foreach (array($targetaudit[$i]) as $j => $v) {
				$data['klausul'][$x]['target_auditee'][$val->id_pertanyaan] = $v;
			}
			if(isset($pertanyaan[$i+1]->id_klausul)){
				if($val->id_klausul != $pertanyaan[$i+1]->id_klausul){
					$x++;
				}
			}
		}
		$data['target_audit'] = $this->model_pertanyaan->target_audit();
		// echo '<pre>';
		// var_dump($pertanyaan);
		// echo '</pre>';
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'ubah_pertanyaan';
		$this->load->view('auditor/header');
		$this->load->view('auditor/ubah_pertanyaan', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function edit_pertanyaan(){
		$inputpost = $this->input->post();
		$size = $inputpost['sizeklausul'];


		echo '<pre>';
		var_dump($inputpost);	
		echo '</pre>';
		exit();
	}

	public function hapus_acara($id_acara){
		$data = $this->model_acara->getAcara($id_acara);


		if($this->model_acara->deleteacaranew($id_acara)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Acara telah dihapus.</p>
              </div>');
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Acara tidak dapat dihapus.</p>
              </div>');
		}
		redirect('auditor/lihat_acara');
	}

	
}
