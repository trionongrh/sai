<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->helper('text');
	}
	public function index()
	{
		$data['last_acara'] = $this->db->order_by('id_acara',"desc")->limit(1)->get('acara')->row_array();
		$pertanyaan = $this->db->get_where('pertanyaan', array('id_acara'=>$data['last_acara']['id_acara']))->result_array();
		$targetall = array();
		foreach ($pertanyaan as $key => $value) {
			$target = explode(',',str_replace('"', '', str_replace(']', '', str_replace('[', '', $value['target_auditee']))));
			foreach ($target as $i => $v) {
				if(!in_array($v, $targetall)){
					array_push($targetall, $v);
				}
			}
		}
		$data['last_acara']['jumlah_auditee'] = sizeof($targetall);


		if(!isset($data['last_acara']['nama_acara'])){
			$data['last_acara'] = array(
				'id_acara' => null,
				'nama_acara' => 'Tidak Ada',
				'pertanyaan' => 0,
				'publish' => 0,
				'jumlah_auditee' => 0,
			);
		}

		$footer['url_page'] = "dashboard";
		$this->load->view('auditor/header');
		$this->load->view('auditor/auditordashboard',$data);
		$this->load->view('auditor/footer');
	}
}