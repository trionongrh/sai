<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
		$this->load->model('model_jawab');

	}
	public function index()
	{
		redirect('auditor/lihat_acara');
	}
	public function jawaban($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('admin/lihat_acara');
				exit();
			}
		}else{
			redirect('admin/lihat_acara');
			exit();
		}

		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);

		$temp = array (
			'id' => '',
			'tabel' => ''
		);
		foreach ($pertanyaan as $i => $val) {
			$data['klausul']['kode_klausul'][$val->id_klausul] = $val->kode_klausul;
			$data['klausul']['desc_klausul'][$val->id_klausul] = $val->deskripsi;
			$data['klausul']['pertanyaan'][$val->id_klausul][$val->id_pertanyaan] = $val->pertanyaan;
			$targetaudit[$i] = json_decode($val->target_auditee);
			foreach ($targetaudit[$i] as $j => $tgt) {
				$temp['id'] = explode("_", $tgt)[1];
				$temp['tabel'] = explode("_", $tgt)[0];
				$data['klausul']['targetaudit'][$val->id_klausul][$i]['sotk'][$j] = $this->model_pertanyaan->target_audit_choose($temp['id'],$temp['tabel']);
			}
		}
		
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'audit_jawaban';
		$this->load->view('auditor/header');
		$this->load->view('auditor/audit_jawaban', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function auditee(){
		$inputget = $this->input->get();
		if($inputget['pertanyaan']==''){
			redirect('auditor/lihat_acara');
		}
		$data['pertanyaan'] = $this->model_jawab->cek_jawaban_auditee_by_pertanyaan($inputget['pertanyaan'],$inputget['sotk']);

		$footer['url_page'] = 'audit_jawaban';
		$this->load->view('auditor/header');
		$this->load->view('auditor/rekap',$data);
		$this->load->view('auditor/footer',$footer);

	}
}
