<?php

class Audit_acara extends CI_Controller{

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
		$this->load->model('model_jawab');
		$this->load->model('model_rekap');
		$this->load->model('model_sotk');
	}
	
	public function index()
	{
		$data['acara'] = $this->model_acara->listing('sedang_berjalan','1');
		$footer['url_page'] = 'audit_acara';
		$this->load->view('auditor/header');
		$this->load->view('auditor/audit_acara', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function jawaban($id_acara){
		$data['acara'] = $this->db->get_where('acara', array('id_acara' => $id_acara))->row_array();
		$data['target_auditee'] = array();
		$data['target_detail'] = array();
		$data['error'] = false;

		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$pertanyaan = $this->db->get('pertanyaan')->result_array();
		foreach ($pertanyaan as $i => $v) {
			$target = json_decode($v['target_auditee']);
			foreach ($target as $j => $val) {
				if(!in_array($val,$data['target_auditee'])){
					array_push($data['target_auditee'], $val);
				}
			}
		}
		sort($data['target_auditee']);
		if(sizeof($data['target_auditee']) >= 1){
			foreach ($data['target_auditee'] as $i => $v) {
				array_push($data['target_detail'], $this->model_sotk->getSOTK($v));
			}
		}


		if($this->input->get('sotk') != null && $this->input->get('sotk') != ''){
			if(in_array($this->input->get('sotk'), $data['target_auditee'])){
				$data['pertanyaan'] = $this->model_pertanyaan->detail_pertanyaan_auditee_new($id_acara,$this->input->get('sotk'));
				$data['jawaban'] = array();
				$data['rekap'] = array();
				$data['status_pertanyaan'] = array();
				$data['status_rekap'] = array();
				foreach ($data['pertanyaan'] as $i => $v) {
					$status = $this->db->get_where('jawaban', array('id_pertanyaan' => $v->id_pertanyaan, 'sotk' => $this->input->get('sotk')))->row_array();
					$status_rekap = $this->db->get_where('temuan', array('id_jawaban' => $status['id_jawaban']))->row_array();
					array_push($data['jawaban'], $status);
					array_push($data['rekap'], $status_rekap);
					array_push($data['status_pertanyaan'], (isset($status) ? '1': '0'));
					array_push($data['status_rekap'], (isset($status_rekap) ? '1': '0'));
				}
			}else{
				$data['error'] = true;
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Sotk tidak ada dalam acara ini</p>
		      	</div>');
			}
		}

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'audit_acara';
		$this->load->view('auditor/header');
		$this->load->view('auditor/audit_jawaban', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function audit_submit($id_acara,$id_jawaban){
		$inputpost = $this->input->post();

		$tanggal = explode("-",str_replace(" ", "", $inputpost['tenggat']));
		$tanggal_mulai = explode("/",$tanggal[0])[2]."-".explode("/",$tanggal[0])[0]."-".explode("/",$tanggal[0])[1];
		$tanggal_selesai = explode("/",$tanggal[1])[2]."-".explode("/",$tanggal[1])[0]."-".explode("/",$tanggal[1])[1];

		$data['jawaban'] = array(
			'id_jawaban' => $id_jawaban,
			'status_jawaban' => $inputpost['status_jawaban']
		);

		$data['temuan'] = array(
			'id_jawaban' => $id_jawaban,
			'temuan' => $inputpost['temuan'],
			'pic' => $inputpost['pic'],
			'perbaikan' => $inputpost['perbaikan'],
			'tenggat_mulai' => $tanggal_mulai,
			'tenggat_selesai' => $tanggal_selesai
		);

		// echo '<pre>';
		// print_r($inputpost);
		// print_r($data);
		// echo '</pre>';
		// exit();

		$this->db->where('id_jawaban', $data['jawaban']['id_jawaban']);
		if($this->db->update('jawaban', $data['jawaban'])){
			if($this->db->insert('temuan', $data['temuan'])){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        	<h4>Sukses</h4>
		        	<p>Data temuan telah ditambahkan.</p>
		      	</div>');
			}else{
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data temuan gagal ditambahkan.</p>
		      	</div>');
			}
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data jawaban gagal diubah.</p>
		      	</div>');
		}

		redirect('auditor/audit_acara/jawaban/'.$id_acara.'?sotk='.$inputpost['sotk']);
	}

	public function audit_edit($id_acara,$id_jawaban){
		$inputpost = $this->input->post();

		$tanggal = explode("-",str_replace(" ", "", $inputpost['tenggat']));
		$tanggal_mulai = explode("/",$tanggal[0])[2]."-".explode("/",$tanggal[0])[0]."-".explode("/",$tanggal[0])[1];
		$tanggal_selesai = explode("/",$tanggal[1])[2]."-".explode("/",$tanggal[1])[0]."-".explode("/",$tanggal[1])[1];

		$data['jawaban'] = array(
			'id_jawaban' => $id_jawaban,
			'status_jawaban' => $inputpost['status_jawaban']
		);

		$data['temuan'] = array(
			'id_temuan' => $inputpost['id_temuan'],
			'id_jawaban' => $id_jawaban,
			'temuan' => $inputpost['temuan'],
			'pic' => $inputpost['pic'],
			'perbaikan' => $inputpost['perbaikan'],
			'tenggat_mulai' => $tanggal_mulai,
			'tenggat_selesai' => $tanggal_selesai
		);

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$this->db->where('id_jawaban', $data['jawaban']['id_jawaban']);
		if($this->db->update('jawaban', $data['jawaban'])){
			$this->db->where('id_temuan', $data['temuan']['id_temuan']);
			if($this->db->update('temuan', $data['temuan'])){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        	<h4>Sukses</h4>
		        	<p>Data temuan telah diubah.</p>
		      	</div>');
			}else{
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data temuan gagal diubah.</p>
		      	</div>');
			}
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	        	<h4>Error</h4>
	        	<p>Data jawaban gagal diubah.</p>
	      	</div>');
		}

		redirect('auditor/audit_acara/jawaban/'.$id_acara.'?sotk='.$inputpost['sotk']);	
	}
}