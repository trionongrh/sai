<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditor") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_sotk');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing();
		$footer['url_page'] = 'lihat_acara';
		$this->load->view('auditor/header');
		$this->load->view('auditor/audit_dokumen', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function ajax_get_pertanyaan($id_acara, $id_klausul){

		$data = $this->model_pertanyaan->detail_pertanyaan_klausul($id_acara,$id_klausul);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}

	public function ajax_get_detail_pertanyaan($id_pertanyaan){

		$data = $this->db->get_where('pertanyaan', array('id_pertanyaan'=>$id_pertanyaan))->row_array();

		$this->output
    	->set_content_type('application/json')
    	->set_output(json_encode($data));
	}

	public function buat_pertanyaan($id_acara){

		$data['acara'] = $this->model_acara->detailacara($id_acara); //
		$header['no_action'] = false;
		if(isset($data['acara'])){
			if($data['acara']->status=='selesai') {
				$header['no_action'] = true;
			}
		}else{
			redirect('auditor/lihat_acara');
			exit();
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara); //
		$data['pertanyaan'] = array(); // array kosong agar frontend dapat mengenali variabel index
		foreach ($data['klausul'] as $i => $v) {
			$temp_pertanyaan = $this->model_pertanyaan->detail_pertanyaan_klausul($v->id_acara,$v->id_klausul);
			$data['pertanyaan'][$i] = $temp_pertanyaan; //
			foreach ($data['pertanyaan'][$i] as $j => $va) {
				$temp_target = json_decode($va['target_auditee']);
				foreach ($temp_target as $k => $ve) {
					$data['pertanyaan'][$i][$j]['target'][$k] = $this->model_sotk->getSOTK($ve); //
				}
			}
		}

		$data['targetaudit'] = $this->model_pertanyaan->target_audit();

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_pertanyaan';
		$this->load->view('auditor/header',$header);
		$this->load->view('auditor/buat_pertanyaan', $data);
		$this->load->view('auditor/footer', $footer);

	}

	public function hapus_pertanyaan(){
		$inputpost = $this->input->post();

		// echo '<pre>';
		// print_r($inputpost);
		// echo '</pre>';
		// exit();


		if($this->model_pertanyaan->delete_pertanyaan($inputpost['id_pertanyaan'])){

		}
		$this->session->set_flashdata('script','table.page('.$inputpost['tablepage'].").draw('page');");

		redirect('auditor/lihat_acara/buat_pertanyaan/'.$inputpost['id_acara']);
	}

	public function edit_pertanyaan(){
		$inputpost = $this->input->post();
		$data = array(
			'pertanyaan' => $inputpost['pertanyaan'],
			'target_auditee' => json_encode($inputpost['targetaudit']),
		);

		if($this->model_pertanyaan->editpertanyaan($inputpost['id_pertanyaan'],$data)){

		}
		$this->session->set_flashdata('script','table.page('.$inputpost['tablepage'].").draw('page');");

		redirect('auditor/lihat_acara/buat_pertanyaan/'.$inputpost['id_acara']);
	}

	public function tambah_pertanyaan(){
		$inputpost = $this->input->post();

		// echo '<pre>';
		// print_r($inputpost);
		// echo '</pre>';
		// exit();

		$data = array(
			'id_acara' => $inputpost['id_acara'],
			'id_klausul' => $inputpost['id_klausul'],
			'pertanyaan' => $inputpost['pertanyaan'],
			'target_auditee' => json_encode($inputpost['targetaudit']),
		);

		if($this->model_pertanyaan->tambah_pertanyaan($data)){

		}
		$this->session->set_flashdata('script','table.page('.$inputpost['tablepage'].").draw('page');");

		redirect('auditor/lihat_acara/buat_pertanyaan/'.$inputpost['id_acara']);
	}

	public function share($id_acara){
		$this->model_acara->editacara($id_acara,array('publish' =>  '1'));
		redirect('auditor/lihat_acara');
	}

	public function hapus_acara($id_acara){
		$data = $this->model_acara->getAcara($id_acara);


		if($this->model_acara->deleteacaranew($id_acara)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Acara telah dihapus.</p>
              </div>');
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Acara tidak dapat dihapus.</p>
              </div>');
		}
		redirect('auditor/lihat_acara');
	}

	
}
