<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_user extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_user');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['audit'] = $this->model_pertanyaan->target_audit();
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'buat_user';
 		$this->load->view('admin/header');
		$this->load->view('admin/buat_user',$data);
		$this->load->view('admin/footer',$footer);
	}

	public function buat(){
		$inputpost = $this->input->post();

		$data = array(
			'username' => $inputpost['username'],
			'password' => $inputpost['password'],
			'level' => $inputpost['level'],
		);
		if(isset($inputpost['sotk'])){
			$data['sotk'] = $inputpost['sotk'];
		}
		// echo '<pre>';
		// var_dump($inputpost);
		// echo '</pre>';
		// exit();
		if(!$this->model_user->check_username($data['username'])){
			if($this->model_user->insertUser($data)){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
	                <h4>Sukses</h4>

	                <p>Data user telah ditambahkan.</p>
	              </div>');
			} else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	                <h4>Eror</h4>

	                <p>Data user gagal ditambahkan.</p>
	              </div>');
			}
		} else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	                <h4>Eror</h4>

	                <p>Data user gagal ditambahkan. karna <strong>username</strong> telah terdaftarkan</p>
	              </div>');
		}
		redirect('admin/buat_user');
	}
}
