<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_iso extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_iso');

	}

	public function index()
	{
		$footer['url_page'] = 'buat_iso';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_iso');
		$this->load->view('admin/footer', $footer);
	}

	public function get_all_iso(){
		$iso = $this->model_iso->listing();
		echo json_encode($iso);
	}

	public function input_iso(){
		$inputpost = $this->input->post();
		$data = array(
			'nama_iso' => $inputpost['nama_iso'] 
		);
		if(!$this->model_iso->read($data['nama_iso'])){
			$id_iso = $this->model_iso->tambahreturnID($data);
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data ISO berhasil dibuat.</p>
              </div>');
		} else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data ISO telah ada.</p>
              </div>');
		}
		redirect('admin/lihat_iso');
	}

	public function edit_iso(){
		$inputpost = $this->input->post();
		foreach ($inputpost['klausul'] as $i => $val) {
			$dataklausul = array(
				'kode_klausul' => $val,
				'deskripsi' => $inputpost['deskripsi'][$i],
				'id_iso' => $inputpost['id_iso']
			);
			if(!$this->model_iso->readklausul($val)){
				$this->model_iso->tambahklausul($dataklausul);
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data klausul telah ditambahkan.</p>
              </div>');
			} else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Eror</h4>

            <p>Data klausul telah ada.</p>
            </div>');
				redirect('admin/buat_iso');
				break;
			}
		}
		redirect('admin/buat_iso');


		// echo "<pre>";
		// var_dump($inputpost);
		// echo "</pre>";

	}
}
