<?php

class Audit_acara extends CI_Controller{

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
		$this->load->model('model_jawab');
		$this->load->model('model_rekap');
	}
	
	public function index()
	{
		$data['acara'] = $this->model_acara->listing('sedang_berjalan','1');
		$footer['url_page'] = 'audit_acara';
		$this->load->view('admin/header');
		$this->load->view('admin/audit_acara', $data);
		$this->load->view('admin/footer', $footer);
	}


	public function jawaban($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		
		// print_r($data);
		// exit();
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('admin/audit_acara');
				exit();
			}
		}else{
			redirect('admin/audit_acara');
			exit();
		}

		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);

		$temp = array (
			'id' => '',
			'tabel' => ''
		);
		foreach ($pertanyaan as $i => $val) {
			$data['klausul']['kode_klausul'][$val->id_klausul] = $val->kode_klausul;
			$data['klausul']['desc_klausul'][$val->id_klausul] = $val->deskripsi;
			$data['klausul']['pertanyaan'][$val->id_klausul][$val->id_pertanyaan] = $val->pertanyaan;
			$targetaudit[$i] = json_decode($val->target_auditee);
			foreach ($targetaudit[$i] as $j => $tgt) {
				$temp['id'] = explode("_", $tgt)[1];
				$temp['tabel'] = explode("_", $tgt)[0];
				$data['klausul']['targetaudit'][$val->id_klausul][$i]['sotk'][$j] = $this->model_pertanyaan->target_audit_choose($temp['id'],$temp['tabel']);
				$status = $this->db->get_where('jawaban', array('id_pertanyaan' => $val->id_pertanyaan, 'sotk' => $tgt))->row_array();
				$data['klausul']['targetaudit'][$val->id_klausul][$i]['sotk'][$j]['status'] = $status;
			}
		}
		
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'audit_jawaban';
		$this->load->view('admin/header');
		$this->load->view('admin/audit_jawaban', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function auditee(){
		$inputget = $this->input->get();
		if($inputget['pertanyaan']=='' || $inputget['sotk'] == ''){
			redirect('admin/audit_acara');
		}

		$data = $this->model_jawab->cek_jawaban_auditee_by_pertanyaan2($inputget['pertanyaan'],$inputget['sotk']);
		if(isset($data['jawaban'])){
			$data['rekap'] = $this->model_rekap->getRekap($data['jawaban']['id_jawaban']);	
		}
		

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'audit_jawaban';
		if(isset($data['rekap'])){
			$this->load->view('admin/header');
			$this->load->view('admin/rekap_edit',$data);
			$this->load->view('admin/footer',$footer);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/rekap',$data);
			$this->load->view('admin/footer',$footer);
		}
	}

	public function audit_submit(){
		$inputpost = $this->input->post();

		$tanggal = explode("-",str_replace(" ", "", $inputpost['tenggat']));
		$tanggal_mulai = explode("/",$tanggal[0])[2]."-".explode("/",$tanggal[0])[0]."-".explode("/",$tanggal[0])[1];
		$tanggal_selesai = explode("/",$tanggal[1])[2]."-".explode("/",$tanggal[1])[0]."-".explode("/",$tanggal[1])[1];

		$data['jawaban'] = array(
			'id_jawaban' => $inputpost['id_jawaban'],
			'status_jawaban' => $inputpost['status_jawaban']
		);

		$data['temuan'] = array(
			'id_jawaban' => $inputpost['id_jawaban'],
			'temuan' => $inputpost['temuan'],
			'pic' => $inputpost['pic'],
			'perbaikan' => $inputpost['perbaikan'],
			'tenggat_mulai' => $tanggal_mulai,
			'tenggat_selesai' => $tanggal_selesai
		);

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$this->db->where('id_jawaban', $data['jawaban']['id_jawaban']);
		if($this->db->update('jawaban', $data['jawaban'])){
			if($this->db->insert('temuan', $data['temuan'])){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        	<h4>Sukses</h4>
		        	<p>Data temuan telah ditambahkan.</p>
		      	</div>');
			}else{
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data temuan gagal ditambahkan.</p>
		      	</div>');
			}
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data jawaban gagal diubah.</p>
		      	</div>');
		}

		redirect('admin/audit_acara');
	}

	public function audit_edit(){
		$inputpost = $this->input->post();

		$tanggal = explode("-",str_replace(" ", "", $inputpost['tenggat']));
		$tanggal_mulai = explode("/",$tanggal[0])[2]."-".explode("/",$tanggal[0])[0]."-".explode("/",$tanggal[0])[1];
		$tanggal_selesai = explode("/",$tanggal[1])[2]."-".explode("/",$tanggal[1])[0]."-".explode("/",$tanggal[1])[1];

		$data['jawaban'] = array(
			'id_jawaban' => $inputpost['id_jawaban'],
			'status_jawaban' => $inputpost['status_jawaban']
		);

		$data['temuan'] = array(
			'id_temuan' => $inputpost['id_temuan'],
			'id_jawaban' => $inputpost['id_jawaban'],
			'temuan' => $inputpost['temuan'],
			'pic' => $inputpost['pic'],
			'perbaikan' => $inputpost['perbaikan'],
			'tenggat_mulai' => $tanggal_mulai,
			'tenggat_selesai' => $tanggal_selesai
		);

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$this->db->where('id_jawaban', $data['jawaban']['id_jawaban']);
		if($this->db->update('jawaban', $data['jawaban'])){
			$this->db->where('id_temuan', $data['temuan']['id_temuan']);
			if($this->db->update('temuan', $data['temuan'])){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        	<h4>Sukses</h4>
		        	<p>Data temuan telah diubah.</p>
		      	</div>');
			}else{
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        	<h4>Error</h4>
		        	<p>Data temuan gagal diubah.</p>
		      	</div>');
			}
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	        	<h4>Error</h4>
	        	<p>Data jawaban gagal diubah.</p>
	      	</div>');
		}

		redirect('admin/audit_acara');

	}
}