<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Lihat_iso extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_iso');
	}
	
	public function index()
	{
		$data['iso'] = $this->model_iso->listingalliso();


		foreach ($data['iso'] as $key => $val) {
			$data['iso'][$key]['klausul'] = $this->model_iso->getKlausul($val['id_iso']);
		}

		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'lihat_iso';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_iso', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function lihat_klausul($id_iso = 0){
		if($id_iso==0){
			redirect('admin/lihat_iso');
		}
		$data['iso'] = $this->model_iso->getISO($id_iso);
		$data['klausul'] = $this->model_iso->listingallklausul($id_iso);

		$footer['url_page'] = 'lihat_iso';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_klausul',$data);
		$this->load->view('admin/footer', $footer);
	}


	public function import_klausul($id_iso){
		$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		if(isset($_FILES['excel_file']['name']) && in_array($_FILES['excel_file']['type'], $file_mimes)) {
			$arr_file = explode('.', $_FILES['excel_file']['name']);
			$extension = end($arr_file);
			if($extension == 'csv'){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}
	        $spreadsheet = $reader->load($_FILES['excel_file']['tmp_name']);
	        $sheetData = $spreadsheet->getActiveSheet()->toArray();
	        array_splice($sheetData, 0, 1);

	        foreach ($sheetData as $i => $v) {
	        	$data[$i]['kode_klausul'] = $v[0];
	        	$data[$i]['deskripsi'] = $v[1];
	        	$data[$i]['id_iso'] = $id_iso;
	        }

	  //       echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			// exit();

	        if($this->db->insert_batch('klausul', $data)){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		            <h4>Sukses</h4>

		            <p>Klausul berhasil diimport.</p>
		          </div>');
			}else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		            <h4>Error</h4>

		            <p>Klausul gagal diimport.</p>
		          </div>');
			}
	    }else{
	    	echo 'Error';
	    	$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	            <h4>Error</h4>

	            <p>File tidak dapat diimport.</p>
	          </div>');
	    }

		redirect('admin/lihat_iso/lihat_klausul/'.$id_iso);
	}

	public function edit($id_iso){
		$inputpost = $this->input->post();
		$data = array(
			'id_iso' => $id_iso,
			'nama_iso' => $inputpost['nama_iso']
		);

		if($this->model_iso->editiso($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Iso telah diubah.</p>
          </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Error</h4>

            <p>Iso gagal diubah.</p>
          </div>');
		}

		redirect('admin/lihat_iso');
		
	}

	public function hapus($id_iso){
		if($this->model_iso->hapusiso($id_iso)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Iso telah dihapus.</p>
          </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Error</h4>

            <p>Iso gagal dihapus.</p>
          </div>');
		}

		redirect('admin/lihat_iso');
	}

	public function tambah_klausul(){
		$inputpost = $this->input->post();
		$data = $inputpost;

		if($this->model_iso->tambahklausul($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Klausul telah ditambahkan.</p>
          </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Error</h4>

            <p>Klausul gagal ditambahkan.</p>
          </div>');
		}

		redirect('admin/lihat_iso/lihat_klausul/'.$data['id_iso']);
		
	}

	public function edit_klausul($id_klausul){
		$inputpost = $this->input->post();
		$data = array(
			'id_klausul' => $id_klausul,
			'kode_klausul' => $inputpost['kode_klausul'],
			'deskripsi' => $inputpost['deskripsi']
		);

		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		if($this->model_iso->editklausul($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Klausul telah diubah.</p>
          </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Error</h4>

            <p>Klausul gagal diubah.</p>
          </div>');
		}

		redirect('admin/lihat_iso/lihat_klausul/'.$inputpost['id_iso']);
	}
	public function delete_klausul($id_klausul){
		$inputpost = $this->input->post();

		if($this->model_iso->deleteklausul($id_klausul)){

			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Klausul telah dihapus.</p>
          </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
            <h4>Error</h4>

            <p>Klausul gagal dihapus.</p>
          </div>');
		}

		redirect('admin/lihat_iso/lihat_klausul/'.$inputpost['id_iso']);
	}
}