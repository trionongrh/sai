<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_pegawai extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_pegawai');

	}

	public function index()
	{
		$this->db->join('kantor', 'kantor.id_kantor=pegawai.id_kantor');
		$this->db->order_by('kantor.nama_kantor');
		$data['pegawai'] = $this->db->get('pegawai')->result_array();

		$footer['url_page'] = 'lihat_pegawai';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_pegawai', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function edit($id){
		$data['kantor'] = $this->db->get('kantor')->result();
		$data['pegawai'] = $this->db->get_where('pegawai', array('id_pegawai' => $id))->row_array();

		$footer['url_page'] = 'edit_pegawai';
		$this->load->view('admin/header');
		$this->load->view('admin/edit_pegawai', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function edit_proses(){
		$inputpost = $this->input->post();
		$id = $inputpost['id_pegawai'];
		$data = array(
			'nama_pegawai' => $inputpost['nama_pegawai'], 
			'id_kantor' => $inputpost['id_kantor']
		);

		// print_r($data);
		// exit();


		$this->db->where('id_pegawai', $id);
		if($this->db->update('pegawai',$data)){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data pegawai telah diubah.</p>
              </div>');
		}else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data pegawai gagal diubah.</p>
              </div>');

		}
		redirect('admin/lihat_pegawai');
	}

	public function delete($id){
		$this->db->where('id_pegawai', $id);
		if($this->db->delete('pegawai')){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data pegawai telah dihapus.</p>
              </div>');
		}else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data pegawai gagal dihapus.</p>
              </div>');

		}
		redirect('admin/lihat_pegawai');
	}
}