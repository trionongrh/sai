<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_resiko extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_risk');
	}
	public function index()
	{
		$data['risk'] = $this->model_risk->listingRisk();

		if($this->input->get('no_reg') != '' || $this->input->get('bag')){
			$data['risk'] = $this->model_risk->listingRisk($this->input->get('no_reg'), $this->input->get('bag'));
		}



		$data['bagian'] = $this->db->get('bagian')->result_array();
		
		// $test = $this->model_risk->listingRisk('',2);
		// echo '<pre>';
		// var_dump($test);
		// echo '</pre>';
		// exit();
		

		$footer['url_page'] = 'buat_resiko';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_resiko',$data);
		$this->load->view('admin/footer', $footer);
	}

	public function update($id_risk)
	{
		$data['risk'] = $this->model_risk->getRisk($id_risk);
		$data['bagian'] = $this->model_risk->listingBagian();
		$data['risk']['id_risk'] = $id_risk;
 		$data['prosedur'] = $this->model_risk->listingProsedur();


		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_resiko';
		$this->load->view('admin/header');
		$this->load->view('admin/edit_resiko',$data);
		$this->load->view('admin/footer', $footer);
	}
	public function edit()
	{
		$inputget = $this->input->post();
		$id_risk = $inputget['id_risk'];
		$data = $inputget;
		unset($data['id_risk']);

		if($this->model_risk->editRisk($id_risk,$data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data risk telah diubah.</p>
              </div>');
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Data risk gagal diubah.</p>
              </div>');
		}
		redirect('admin/lihat_resiko');
	}
	public function delete($id_risk)
	{
		if($this->model_risk->deleteRisk($id_risk)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data risk telah dihapus.</p>
              </div>');
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Error</h4>

                <p>Data risk gagal dihapus.</p>
              </div>');
		}
		redirect('admin/lihat_resiko');
	}

	public function download(){
		$this->load->library('Pdf');
		$inputget = $this->input->get();
		$data['risk'] = $this->model_risk->getRisk($inputget['risk']);
		$data['reg'] = $data['risk']['no_reg'];

		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$this->load->view('template_pdf/risk_pdf', $data);
	}
}

		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();