<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_pegawai extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_pegawai');

	}
	public function index()
	{
		$data['kantor'] = $this->db->get('kantor')->result();

		$footer['url_page'] = 'buat_pegawai';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_pegawai',$data);
		$this->load->view('admin/footer', $footer);
	}

	public function add(){
		$inputpost = $this->input->post();
		print_r($inputpost);
		$data  = array(
			'nama_pegawai' => $inputpost['nama_pegawai'],
			'id_kantor' => $inputpost['id_kantor']
		);
		if(!$this->db->get_where('pegawai', $data)->row_array()){
			if($this->model_pegawai->tambah($data)){
					$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		                <h4>Sukses</h4>

		                <p>Data pegawai telah ditambahkan.</p>
		              </div>');
			}else{
					$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		                <h4>Eror</h4>

		                <p>Data pegawai gagal ditambahkan.</p>
		              </div>');
			}

		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		                <h4>Eror</h4>

		                <p>Data pegawai sudah ada.</p>
		              </div>');
		}
		redirect('admin/buat_pegawai');
	}
}