<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->model('model_acara');
		$this->load->model('model_iso');
	}
	public function index()
	{
		$data['dataiso'] = $this->model_iso->listing();
		$footer['url_page'] = 'buat_acara';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_acara', $data);
		$this->load->view('admin/footer', $footer);
	}
	public function input_acara()
	{
		$inputpost = $this->input->post();

		// echo "<pre>";
		// print_r($inputpost);
		// echo "</pre>";
		// exit();

		$tanggal = explode("-",str_replace(" ", "", $inputpost['tanggal']));

		$tanggal_visitasi = explode("-",str_replace(" ", "", $inputpost['tanggalvisitasi']));

		$tanggal_mulai = explode("/",$tanggal[0])[2]."-".explode("/",$tanggal[0])[0]."-".explode("/",$tanggal[0])[1];

		$tanggal_selesai = explode("/",$tanggal[1])[2]."-".explode("/",$tanggal[1])[0]."-".explode("/",$tanggal[1])[1];

		$tanggal_visitasi_mulai = explode("/",$tanggal_visitasi[0])[2]."-".explode("/",$tanggal_visitasi[0])[0]."-".explode("/",$tanggal_visitasi[0])[1];

		$tanggal_visitasi_selesai = explode("/",$tanggal_visitasi[1])[2]."-".explode("/",$tanggal_visitasi[1])[0]."-".explode("/",$tanggal_visitasi[1])[1];

		$data_acara = array(
			'nama_acara' => $inputpost['nama_acara'], 
			'id_iso' => $inputpost['kriteria_audite'],
			'tujuan' => $inputpost['tujuan_audit'],
			'tanggal_mulai' => $tanggal_mulai,
			'tanggal_selesai' => $tanggal_selesai,
			'tanggal_visitasi_mulai' => $tanggal_visitasi_mulai,
			'tanggal_visitasi_selesai' => $tanggal_visitasi_selesai,
		);

		if ($tanggal_mulai > date("Y-m-d")) {
			$data_acara['status'] = "belum_mulai";
		}else if ($tanggal_mulai <= date("Y-m-d")){
			$data_acara['status'] = "sedang_berjalan";
		}

		if(!$this->model_acara->read($inputpost['nama_acara'])){
			$this->model_acara->tambahacara($data_acara);
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
            <h4>Sukses</h4>

            <p>Acara telah ditambahkan.</p>
          </div>');
		} else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
        <h4>Eror</h4>

        <p>Acara telah ada.</p>
        </div>');
		}
		redirect('admin/lihat_acara');
	}
}
