<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_sotk extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_sotk');
		$this->load->model('model_sotk_dir');
		$this->load->model('model_sotk_bag');
		$this->load->model('model_sotk_urus');
	}
	public function index()
	{
		$footer['url_page'] = 'buat_sotk';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_sotk');
		$this->load->view('admin/footer', $footer);
	}

	public function add(){
		$data = $this->input->post();
		$tabel = $data['tabel'];
		unset($data['tabel']);

		$check = $this->db->get_where($tabel,$data)->row_array();

		if(!$check){
			if($this->db->insert($tabel,$data)){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data '.ucfirst($tabel).' telah ditambahkan.</p>
              </div>');

			}
		}else{
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
	                <h4>Eror</h4>

	                <p>Data telah ada.</p>
	              </div>');
		}
		redirect('admin/buat_sotk');
	}

	public function ajax_get(){
		$tabel = $this->input->get('q');
		$data = array();
		if($tabel == 'kantor' || $tabel == 'direktorat' || $tabel == 'bagian'){
			$data = $this->db->get($tabel)->result_array();
		}


		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));

	}
}
