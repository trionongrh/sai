<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_user extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_user'); // load model_user
		$this->load->model('model_sotk'); // load model_user
		$this->load->model('model_pertanyaan'); // load model_user
	}
	public function index()
	{
		$data['user'] = $this->model_user->getUser();
		$data['sotk'] = array();
		foreach ($data['user'] as $i => $v) {
			if($v['sotk']!=""){
				$data['sotk'][$i] = $this->model_sotk->getSOTK($v['sotk']);
			}
		}
		
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'lihat_user';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_user',$data);
		$this->load->view('admin/footer',$footer);
	}

	public function edit($id){
		$data['user'] = $this->model_user->getUser_by_id($id);
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();

		$data['audit'] = $this->model_pertanyaan->target_audit();
		$footer['url_page'] = 'edit_user';
		$this->load->view('admin/header');
		$this->load->view('admin/edit_user',$data);
		$this->load->view('admin/footer',$footer);
	}

	public function update(){
		$inputpost = $this->input->post();
		$data['username'] = $inputpost['username'];
		$data['sotk'] = $inputpost['sotk'];
		if($inputpost['password'] != ''){
			$data['password'] = $inputpost['password'];
		}
		$this->db->where('uid', $inputpost['uid']);
		if($this->db->update('login_session', $data)){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data user telah diubah.</p>
              </div>');
		}else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data user gagal diubah.</p>
              </div>');

		}
		redirect('admin/lihat_user');
	}

	public function delete($id){
		$this->db->where('uid', $id);
		if($this->db->delete('login_session')){
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data user telah dihapus.</p>
              </div>');
		}else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data user gagal dihapus.</p>
              </div>');

		}
		redirect('admin/lihat_user');
	}
}
