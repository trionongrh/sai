<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_sotk extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_sotk');
		$this->load->model('model_sotk_dir');
		$this->load->model('model_sotk_bag');
		$this->load->model('model_sotk_urus');



	}
	public function index()
	{
		$footer['url_page'] = 'buat_sotk';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_sotk');
		$this->load->view('admin/footer', $footer);
	}
	public function bagian($id_kantor)
	{
		$data['direktorat'] = $this->model_sotk_dir->listingwhere($id_kantor);
		$data['id_kantor'] = $id_kantor;
		$footer['url_page'] = 'buat_sotk';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_sotk_bagian',$data);
		$this->load->view('admin/footer',$footer);
	}
	public function urusan($id_kantor)
	{
		// $data['bagian'] = $this->model_sotk_bag->listingwhere($id_direktorat);
		// $data['id_direktorat'] = $id_direktorat;
		$data['bagian'] = $this->model_sotk_urus->listingall($id_kantor);
		$footer['url_page'] = 'buat_sotk';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_sotk_urusan',$data);
		$this->load->view('admin/footer', $footer);
	}
	public function input_sotk()
		{
        //$kntr	= $this->model_sotk->listing();
		//$data = array(	'title'		=> 'Tambah produk',
		//				'kategori'	=> $kntr,
		//				'error'		=> $this->upload->display_errors());

		$i = $this->input;
		$direktorat = $i->post('direktorat');

		//$id_kantor	= url_title($i->post('id_kantor'),'dash',TRUE);
		$data1 = array(	'nama_kantor'	=> $i->post('nama_kantor'));
						//'direktur'		=> $i->post('id_kantor'));

		if(!$this->model_sotk->read($data1['nama_kantor'])){
			$id_kantor = $this->model_sotk->tambahreturnID($data1);

			foreach ($direktorat as $dir => $val) {
				$data_direktorat = array(
					'nama_direktorat' => $val,
					'id_kantor' => $id_kantor
				);
				if(!$this->model_sotk_dir->read($val)){
					$this->model_sotk_dir->tambah($data_direktorat);
				} else {
					$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
			                <h4>Eror</h4>

			                <p>Data direktorat telah ada.</p>
			              </div>');
					redirect('admin/buat_sotk');
					break;
				}
			}
			$this->session->set_flashdata('msg','Kantor dan direktur telah ditambahkan');
			redirect('admin/buat_sotk/bagian/'.$id_kantor);
		} else {
			$this->session->set_flashdata('msg','Data Kantor telah ada');
			redirect('admin/buat_sotk');
		}
	}

	public function edit_sotk_kantor()
		{
		$i = $this->input;
		$direktorat = $i->post('direktorat');
		$id_kantor = $i->post('id_kantor');

		foreach ($direktorat as $dir => $val) {
			$data_direktorat = array(
				'nama_direktorat' => $val,
				'id_kantor' => $id_kantor
			);
			if(!$this->model_sotk_dir->read($val)){
				$this->model_sotk_dir->tambah($data_direktorat);
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data direktorat telah ditambahkan.</p>
              </div>');
			} else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data direktorat telah ada.</p>
              </div>');
				redirect('admin/buat_sotk');
				break;
			}
		}
        redirect('admin/buat_sotk/bagian/'.$id_kantor);
	}

	public function get_all_kantor(){
		$kntr	= $this->model_sotk->listing();
		echo json_encode($kntr);

	}

	public function input_bagian(){
		$i = $this->input;

		// echo '<pre>';
		// print_r($i->post());
		// echo '</pre>';
		//exit();

		$bagian = $i->post('bagian');
		$id_direktorat = $i->post('direktorat');

		$id_kantor = $i->post('id_kantor');


		foreach ($bagian as $key => $value) {
			$data = array(
				'nama_bagian' => $value,
				'id_direktorat' => $id_direktorat[$key]
			);
			//print_r($data);
			if(!$this->model_sotk_bag->read($value)){
				$this->model_sotk_bag->tambah($data);
				$this->session->set_flashdata('msg','Bagian telah ditambahkan');
			} else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data Bagian telah ada, silahkan ke menu edit sotk</p>
              </div>');
				redirect('admin/buat_sotk/bagian'.$id_kantor);
				break;
			}
		}

		redirect('admin/buat_sotk/urusan/'.$id_kantor);
	}

	public function input_urusan(){
		$i = $this->input;
		$urusan = $i->post('urusan');
		$id_bagian = $i->post('bagian');
		foreach ($urusan as $key => $value) {
			$data = array(
				'nama_urusan' => $value,
				'id_bagian' => $id_bagian[$key]
			);
			if(!$this->model_sotk_urus->read($value)){
				$this->model_sotk_urus->tambah($data);
				$this->session->set_flashdata('msg','Urusan telah ditambahkan');
			} else {
				$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data Urusan telah ada, silahkan ke menu edit sotk</p>
              </div>');
				redirect('admin/buat_sotk/urusan');
				break;
			}
		}

		redirect('admin/lihat_sotk');
	}
}
