<?php


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan extends CI_Controller{

  public function __construct() {
    parent::__construct();
    if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
      redirect('Login');
    }
    $this->load->helper('text');
    $this->load->model('model_acara');
    $this->load->model('model_iso');
    $this->load->helper('string');


  }

  public function index(){

    $data['acara'] = $this->db->order_by('id_acara','DESC')->get('acara')->result_array();
    foreach ($data['acara'] as $key => $value) {
      $iso = $this->db->get_where('iso', array('id_iso' => $value['id_iso']))->row_array();
      $data['acara'][$key]['nama_iso'] = $iso['nama_iso'];
    }


    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    // exit();

    $footer['url_page'] = 'laporan_acara';
    $this->load->view('admin/header');
    $this->load->view('admin/laporan',$data);
    $this->load->view('admin/footer', $footer);
  }

  public function acara($id_acara){
    $this->load->model('Model_sotk');

    $data['acara'] = $this->db->get_where('acara', array('id_acara'=>$id_acara))->row_array();

    $data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_acara' => $id_acara))->result_array();


    $id_pertanyaan = array();
    $data['target_audit'] = array();
    $data['n_jawaban'] = array();
    $data['n_pertanyaan'] = array();
    $data['n_ratarata'] = array();
    $data['jawaban'] = array();
    $data['sotk'] = array();

    foreach ($data['pertanyaan'] as $key => $value) {
      array_push($id_pertanyaan, $value['id_pertanyaan']);
      $target_audit = explode(',',str_replace('"', '', str_replace(']','',str_replace('[', '', $value['target_auditee']))));
      foreach ($target_audit as $i => $v) {
        if(!in_array($v, $data['target_audit'])){
          array_push($data['target_audit'],$v);
        }
      }
    }

    foreach ($data['target_audit'] as $i => $v) {
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      array_push($data['jawaban'], $this->db->get_where('jawaban', array('sotk' => $v))->result_array());

      array_push($data['sotk'], $this->Model_sotk->getSOTK($v));

      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["1"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '1'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["2"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '2'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["3"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '3'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["4"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '4'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["5"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '5'))->result_array());
      array_push($data['n_jawaban'], $n_jawab);

      $this->db->where('id_acara', $id_acara);
      $this->db->like('target_auditee', $v);
      $n_tanya = sizeof($this->db->get('pertanyaan')->result_array());
      array_push($data['n_pertanyaan'], $n_tanya);

      $score = ($n_jawab['1']*1) + ($n_jawab['2']*2) + ($n_jawab['3']*3) + ($n_jawab['4']*4) + ($n_jawab['5']*5);
      $n_siap = number_format(($score/$n_tanya), 2);
      // $n_siap = number_format(($n_jawab / $n_tanya) * 100, 2);
      array_push($data['n_ratarata'], $n_siap);
    }


    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    // exit();  

    $footer['url_page'] = 'laporan';
    $this->load->view('admin/header');
    $this->load->view('admin/laporan_acara',$data);
    $this->load->view('admin/footer', $footer);
  }

  public function export_detail($id_acara){
    $this->load->model('Model_sotk');

    $data['acara'] = $this->db->get_where('acara', array('id_acara'=>$id_acara))->row_array();

    $data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_acara' => $id_acara))->result_array();


    $id_pertanyaan = array();
    $data['target_audit'] = array();
    $data['n_jawaban'] = array();
    $data['n_pertanyaan'] = array();
    $data['n_ratarata'] = array();
    $data['jawaban'] = array();
    $data['sotk'] = array();

    foreach ($data['pertanyaan'] as $key => $value) {
      array_push($id_pertanyaan, $value['id_pertanyaan']);
      $target_audit = explode(',',str_replace('"', '', str_replace(']','',str_replace('[', '', $value['target_auditee']))));
      foreach ($target_audit as $i => $v) {
        if(!in_array($v, $data['target_audit'])){
          array_push($data['target_audit'],$v);
        }
      }
    }


    $output_excel = array();

    foreach ($data['target_audit'] as $i => $v) {
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      array_push($data['jawaban'], $this->db->get_where('jawaban', array('sotk' => $v))->result_array());

      array_push($data['sotk'], $this->Model_sotk->getSOTK($v));

      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["1"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '1'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["2"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '2'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["3"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '3'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["4"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '4'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["5"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '5'))->result_array());
      array_push($data['n_jawaban'], $n_jawab);

      $this->db->where('id_acara', $id_acara);
      $this->db->like('target_auditee', $v);
      $n_tanya = sizeof($this->db->get('pertanyaan')->result_array());
      array_push($data['n_pertanyaan'], $n_tanya);

      $score = ($n_jawab['1']*1) + ($n_jawab['2']*2) + ($n_jawab['3']*3) + ($n_jawab['4']*4) + ($n_jawab['5']*5);
      $n_siap = number_format(($score/$n_tanya), 2);
      // $n_siap = number_format(($n_jawab / $n_tanya) * 100, 2);
      array_push($data['n_ratarata'], $n_siap);

      array_push(
        $output_excel,
        array(
          'level' => $data['sotk'][$i]['tabel'],
          'auditee' => $data['sotk'][$i]['nama_sotk'],
          'pertanyaan' => $n_tanya,
          'nilai' => $n_siap
        )
      );
    }


    // echo '<pre>';
    // print_r($output_excel);
    // echo '</pre>';
    // exit();  

    $spreadsheet = new Spreadsheet();
    $spreadsheet->getProperties()
    ->setCreator('YOUR NAME')
    ->setLastModifiedBy('YOUR NAME')
    ->setTitle('Demo Document')
    ->setSubject('Demo Document')
    ->setDescription('Demo Document')
    ->setKeywords('demo php spreadsheet')
    ->setCategory('demo php file');
     
    // NEW WORKSHEET
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setTitle('Testing');
    $sheet->setCellValue('A1', 'No');
    $sheet->setCellValue('B1', 'Level');
    $sheet->setCellValue('C1', 'Auditee');
    $sheet->setCellValue('D1', 'Jumlah Pertanyaan');
    $sheet->setCellValue('E1', 'Nilai Rata-rata');
    $sheet->getStyle("A1:E1")->getFont()->setBold( true );

    $i = 2;
    foreach ($output_excel as $in => $v) {
      $sheet->setCellValue('A'.$i, $i-1);
      $sheet->setCellValue('B'.$i, $v['level']);
      $sheet->setCellValue('C'.$i, $v['auditee']);
      $sheet->setCellValue('D'.$i, $v['pertanyaan']);
      $sheet->setCellValue('E'.$i, $v['nilai']);
      $i++;
    }


    // OUTPUT
    $writer = new Xlsx($spreadsheet);

    // THIS WILL SAVE TO A FILE ON THE SERVER
    // $writer->save('test.xlsx');

    // OR FORCE DOWNLOAD
    $filename = 'Detail-Laporan('.$data['acara']['nama_acara'].')';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
    header('Cache-Control: max-age=0');
    header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');

    $writer->save('php://output');
  }



  public function print_laporan($id_acara){
    $this->load->model('Model_sotk');

    $data['acara'] = $this->db->get_where('acara', array('id_acara'=>$id_acara))->row_array();

    $data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_acara' => $id_acara))->result_array();


    $id_pertanyaan = array();
    $data['target_audit'] = array();
    $data['n_jawaban'] = array();
    $data['n_pertanyaan'] = array();
    $data['n_ratarata'] = array();
    $data['jawaban'] = array();
    $data['sotk'] = array();

    foreach ($data['pertanyaan'] as $key => $value) {
      array_push($id_pertanyaan, $value['id_pertanyaan']);
      $target_audit = explode(',',str_replace('"', '', str_replace(']','',str_replace('[', '', $value['target_auditee']))));
      foreach ($target_audit as $i => $v) {
        if(!in_array($v, $data['target_audit'])){
          array_push($data['target_audit'],$v);
        }
      }
    }

    foreach ($data['target_audit'] as $i => $v) {
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      array_push($data['jawaban'], $this->db->get_where('jawaban', array('sotk' => $v))->result_array());

      array_push($data['sotk'], $this->Model_sotk->getSOTK($v));

      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["1"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '1'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["2"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '2'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["3"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '3'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["4"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '4'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["5"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '5'))->result_array());
      array_push($data['n_jawaban'], $n_jawab);

      $this->db->where('id_acara', $id_acara);
      $this->db->like('target_auditee', $v);
      $n_tanya = sizeof($this->db->get('pertanyaan')->result_array());
      array_push($data['n_pertanyaan'], $n_tanya);

      $score = ($n_jawab['1']*1) + ($n_jawab['2']*2) + ($n_jawab['3']*3) + ($n_jawab['4']*4) + ($n_jawab['5']*5);
      $n_siap = number_format(($score/$n_tanya), 2);
      // $n_siap = number_format(($n_jawab / $n_tanya) * 100, 2);
      array_push($data['n_ratarata'], $n_siap);
    }

    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    // exit();  



    $footer['url_page'] = 'laporan';
    $this->load->view('admin/laporan_print',$data);

  }

  public function ajax_kesiapan($id_acara, $internal = false){

    $this->load->model('Model_sotk');

    $data['acara'] = $this->db->get_where('acara', array('id_acara'=>$id_acara))->row_array();

    $data['pertanyaan'] = $this->db->get_where('pertanyaan', array('id_acara' => $id_acara))->result_array();


    $id_pertanyaan = array();
    $data['target_audit'] = array();
    $data['n_jawaban'] = array();
    $data['n_pertanyaan'] = array();
    $data['n_ratarata'] = array();
    $data['jawaban'] = array();
    $data['sotk'] = array();
    $data['output'] = array();

    foreach ($data['pertanyaan'] as $key => $value) {
      array_push($id_pertanyaan, $value['id_pertanyaan']);
      $target_audit = explode(',',str_replace('"', '', str_replace(']','',str_replace('[', '', $value['target_auditee']))));
      foreach ($target_audit as $i => $v) {
        if(!in_array($v, $data['target_audit'])){
          array_push($data['target_audit'],$v);
        }
      }
    }

    foreach ($data['target_audit'] as $i => $v) {
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      array_push($data['jawaban'], $this->db->get_where('jawaban', array('sotk' => $v))->result_array());

      array_push($data['sotk'], $this->Model_sotk->getSOTK($v));

      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["1"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '1'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["2"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '2'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["3"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '3'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["4"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '4'))->result_array());
      $this->db->where_in('id_pertanyaan', $id_pertanyaan);
      $n_jawab["5"] = sizeof($this->db->get_where('jawaban', array('sotk' => $v, 'status_jawaban' => '5'))->result_array());
      array_push($data['n_jawaban'], $n_jawab);

      $this->db->where('id_acara', $id_acara);
      $this->db->like('target_auditee', $v);
      $n_tanya = sizeof($this->db->get('pertanyaan')->result_array());
      array_push($data['n_pertanyaan'], $n_tanya);

      $score = ($n_jawab['1']*1) + ($n_jawab['2']*2) + ($n_jawab['3']*3) + ($n_jawab['4']*4) + ($n_jawab['5']*5);
      $n_siap = number_format(($score/$n_tanya), 2);
      // $n_siap = number_format(($n_jawab / $n_tanya) * 100, 2);
      array_push($data['n_ratarata'], $n_siap);

      array_push($data['output'], array('label' => $data['sotk'][$i]['nama_sotk'], 'y' => (double)$n_siap));
    }

    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    // exit();

    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data['output']));

    if($internal){
      return $data;
    }
  }

}
