<?php 


class Prosedur extends CI_Controller{
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin" ) {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_risk');
	}

	public function index() {
		$this->db->join('bagian', 'bagian.id_bagian=risk_prosedur.id_bagian');
		if($this->input->get('pros') != ''){
			$this->db->where('bagian.id_bagian', $this->input->get('pros'));
		}
		$this->db->order_by('risk_prosedur.id_bagian', 'ASC');
		$data['prosedur'] = $this->db->get('risk_prosedur')->result_array();
		$data['bagian'] = $this->db->get('bagian')->result_array();

		$footer['url_page'] = 'prosedur';
		$this->load->view('admin/header');
		$this->load->view('admin/prosedur', $data);
		$this->load->view('admin/footer', $footer);

	}

	public function add(){
		$inputpost = $this->input->post();
		$data = array(
			'nama_prosedur' => $inputpost['nama_prosedur'], 
			'id_bagian' => $inputpost['id_bagian']
		);
		// echo '<pre>';
		// print_r($inputpost);
		// echo '</pre>';
		// exit();
		if($this->model_risk->tambahprosedur($data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah ditambahkan.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Error</h4>
		        <p>Data prosedur gagal ditambahkan.</p>
		      </div>');

		}
		redirect('admin/prosedur');
	}
	public function delete($id){
		if($this->db->delete('risk_prosedur', array('id_prosedur' => $id))){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah dihapus.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Sukses</h4>
		        <p>Data prosedur gagal dihapus.</p>
		      </div>');

		}
		redirect('admin/prosedur');
	}

	public function update(){
		$inputpost = $this->input->post();
		$id = $inputpost['id_prosedur'];
		$data = array(
			'nama_prosedur' => $inputpost['nama_prosedur'], 
			'id_bagian' => $inputpost['id_bagian']
		);

		$this->db->where('id_prosedur', $id);
		if($this->db->update('risk_prosedur', $data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
		        <h4>Sukses</h4>
		        <p>Data prosedur telah diubah.</p>
		      </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
		        <h4>Sukses</h4>
		        <p>Data prosedur gagal diubah.</p>
		      </div>');
		}

		redirect('admin/prosedur');
	}
}