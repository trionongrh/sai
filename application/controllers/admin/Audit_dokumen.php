<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit_dokumen extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing();
		$footer['url_page'] = 'lihat_acara';
		$this->load->view('admin/header');
		$this->load->view('admin/audit_dokumen', $data);
		$this->load->view('admin/footer', $footer);
	}
}