<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_sotk extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->model('model_sotk');
	}

	public function index()
	{
		if($this->input->get('sotk')==''){
			if($this->input->get('kantor')!=''){
				$data['sotk'] = $this->model_sotk->listingall_fromkantor($this->input->get('kantor'), $this->input->get('q'));
			}else{
				$data['sotk'] = $this->model_sotk->listingall($this->input->get('q'));
			}
		}else if($this->input->get('sotk')=='kantor'){
			$data['sotk'] = $this->model_sotk->listingkantor($this->input->get('q'));
		}else if($this->input->get('sotk')=='direktorat'){
			$data['sotk'] = $this->model_sotk->listingdirektorat($this->input->get('q'));
		}else if($this->input->get('sotk')=='bagian'){
			$data['sotk'] = $this->model_sotk->listingbagian($this->input->get('q'));
		}else if($this->input->get('sotk')=='urusan'){
			$data['sotk'] = $this->model_sotk->listingurusan($this->input->get('q'));
		}
		// $test1 = $this->model_sotk->listingall_fromkantor('7');

		// echo '<pre>';
		// var_dump($test1);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'lihat_sotk';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_sotk',$data);
		$this->load->view('admin/footer', $footer);
	}

	public function delete($sotk){
		$tes = $this->model_sotk->deleteSOTK($sotk);
		if($tes){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data sotk telah dihapus.</p>
              </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data tidak terhapus.</p>
              </div>');
		}
		redirect('admin/lihat_sotk');
	}

	public function edit_sotk(){
		$inputget=$this->input->get();
		$data_get['sotk'] = $this->model_sotk->getSOTK_by_id_table($inputget['id_sotk'],$inputget['tabel']);

		if(!isset($data_get['sotk'])){
			redirect('admin/lihat_sotk');
		}

		if($inputget['tabel']=='direktorat'){
			$data_get['list'] = $this->model_sotk->getlistSOTK_tabel('kantor');
		}else if($inputget['tabel']=='bagian'){
			$data_get['list'] = $this->model_sotk->getlistSOTK_tabel('direktorat');
		}else if($inputget['tabel']=='urusan'){
			$data_get['list'] = $this->model_sotk->getlistSOTK_tabel('bagian');
		}

		// echo '<pre>';
		// print_r($data_get);
		// echo '</pre>';
		// exit();

		$this->load->view('admin/header');
		$this->load->view('admin/edit_sotk',$data_get);
		$this->load->view('admin/footer');
	}

	public function edit(){
		$inputpost = $this->input->post();
		$tabel = $inputpost['tabel'];
		$data = $inputpost;
		unset($data['tabel']);
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();
		if($this->model_sotk->editsotk($tabel,$data)){
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-success">
                <h4>Sukses</h4>

                <p>Data sotk telah diubah.</p>
              </div>');
		}else {
			$this->session->set_flashdata('msg','<div id="notif_alert" class="callout callout-danger">
                <h4>Eror</h4>

                <p>Data tidak terubah.</p>
              </div>');
		}
		redirect('admin/lihat_sotk');
	}

	public function download(){
		$this->load->library('Pdf');

		if($this->input->get('kantor')!=''){
			$data['sotk'] = $this->model_sotk->getlistSOTK_fromkantor2($this->input->get('kantor'));
			
			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			// exit();
    		$this->load->view('template_pdf/kantor_sotk_pdf', $data);
		}else{
			$data['test'] = $this->model_sotk->getlistSOTK_fromall();
	        $this->load->view('template_pdf/all_sotk_pdf', $data);
		}
	}

}