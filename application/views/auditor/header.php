<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Satuan Audit Internal | Auditor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap/dist/css/bootstrap.min.css'); ?> ">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/font-awesome/css/font-awesome.min.css'); ?> ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/Ionicons/css/ionicons.min.css'); ?> ">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/dist/css/AdminLTE.min.css'); ?> ">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.css'); ?> ">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?> ">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/select2/dist/css/select2.min.css'); ?> ">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/dist/css/skins/_all-skins.min.css'); ?> ">
    <script src="<?php echo base_url ('assets/jquery/dist/jquery.min.js'); ?> "> </script>
    <!-- DataTable -->
    <script src="<?php echo base_url ('assets/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url ('assets/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">

<header class="main-header">

    <!-- Logo -->
    <div class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url ('assets/images/logo/logo-mini.png'); ?>" style="max-width: 100%;
    max-height: 100%;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url ('assets/images/logo/logo.png'); ?>" style="max-width: 100%;
    max-height: 100%;"></span>
        
    </div>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        </a>
        

    </nav>
</header>