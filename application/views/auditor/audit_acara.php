<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit Acara
        <small>list data acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ISO</th>
                  <th>Nama Acara</th>
                  <th>Tanggal Acara</th>
                  <!-- <th>Pertanyaan</th> -->
                  <th>Publish</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $num = 1;
                  foreach ($acara as $i => $val) { ?>
                    <tr>
                      <td><?php echo $num++; ?></td>
                      <td><?php echo $val->nama_iso ?></td>
                      <td><?php echo $val->nama_acara ?></td>
                      <td>
                      <?php
                        $status = "";
                        if($val->status == "belum_mulai"){
                          $status = '<small class="label bg-green">Belum Mulai</small>';
                        } else if ($val->status == "sedang_berjalan") {
                          $status = '<small class="label bg-blue">Sedang Berlangsung</small>';
                        } else if ($val->status == "selesai") {
                          $status = '<small class="label bg-green">Selesai</small>';
                        }
                        $date_start = date_create($val->tanggal_mulai);
                        $date_end = date_create($val->tanggal_selesai);
                        echo date_format($date_start, "d/m/Y").' - '.date_format($date_end, "d/m/Y").' '.$status;
                      ?>
                      </td>
                      <!-- <td> -->
                        <?php 
                        // if($val->pertanyaan == "0"){
                        //   echo '<small class="label bg-yellow">Belum</span>';
                        // } else {
                        //   echo '<small class="label bg-green">Sudah</span>';
                        // }
                        ?>
                      <!-- </td> -->
                      <td>
                        <?php if($val->publish == "0"){
                          echo '<small class="label bg-yellow">Belum</span>';
                        } else {
                          echo '<small class="label bg-green">Sudah</span>';
                        }?>
                      </td>
                      <?php if($_SESSION['level'] == 'admin' || $_SESSION['level'] == 'auditor') { ?>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="<?php echo site_url('auditor/audit_acara/jawaban/'.$val->id_acara) ?>"><i class="fa fa-tasks"></i>Audit Visitasi</a></li>
                            </ul>
                          </div>
                        </td>
                      <?php } ?>
                    </tr>    
                  <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
<div class="modal fade" id="modal-share">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Publish Acara</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin mempublish acara : <strong id="modal-nama-acara"></strong></p>
      </div>
      <div class="modal-footer">
        <form action="<?php echo site_url('auditor/lihat_acara/share') ?>" id="form-share" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> Share</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-delete">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Acara</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus acara : <strong id="modal-nama-acara-delete"></strong></p>
        <p>Pertanyaan yang ada didalamnya juga akan ikut terhapus !</p>
      </div>
      <div class="modal-footer">
        <form action="<?php echo site_url('auditor/lihat_acara/hapus_acara') ?>" id="form-delete" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Delete</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

