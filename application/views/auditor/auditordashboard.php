<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    <?php 
    echo $this->session->flashdata('msg');
    ?>

        <h1>
            Dashboard
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="row">
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $last_acara['nama_acara'] ?></h3>
              <h4>Target auditee : <span><?php echo $last_acara['jumlah_auditee'] ?> Unit</span></h4>

              <p>Acara Terbaru</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
            <?php 
            if($last_acara['pertanyaan'] == 0 && $last_acara['id_acara'] != null){ ?>
            <a href="<?php echo site_url('auditor/lihat_acara/buat_pertanyaan/'.$last_acara['id_acara']) ?>" class="small-box-footer">Buat Pertanyaan <i class="fa fa-arrow-circle-right"></i></a>
            <?php 
            }else if($last_acara['id_acara'] != null)
            { 
            if($last_acara['publish'] == 0){?>
            <a data-toggle="modal" data-target="#modal-share" class="small-box-footer">Sebarkan <i class="fa fa-arrow-circle-right"></i></a>
            <?php 
            }}?>
            <?php 
            if($last_acara['pertanyaan'] == 1 && $last_acara['publish'] == 1){ ?>
            <a href="<?php echo site_url('auditor/audit_acara/jawaban/'.$last_acara['id_acara']) ?>" class="small-box-footer">Audit Acara <i class="fa fa-arrow-circle-right"></i></a>
            <a href="<?php echo site_url('auditor/laporan/acara/'.$last_acara['id_acara']) ?>" class="small-box-footer">Lihat Laporan <i class="fa fa-arrow-circle-right"></i></a>
            <?php 
            } ?>
          </div>
        </div>
      </div>
    </section>
</div>
<div class="modal fade" id="modal-share">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Publish Acara</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin mempublish acara : <strong id="modal-nama-acara"><?php echo $last_acara['nama_acara'] ?></strong></p>
      </div>
      <div class="modal-footer">
        <form action="<?php echo site_url('auditor/lihat_acara/share/'.$last_acara['id_acara']) ?>" id="form-share" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> Sebarkan </button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->