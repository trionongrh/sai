<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('auditor/audit_acara') ?>" class="btn btn-default"><i class="fa fa-angle-left"></i></a>
        Audit Dokumen
        <small>Acara : <strong><a class="label bg-green" href=""><?php echo $acara['nama_acara']; ?></a></strong></small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-primary">
                <div class="box-header">
                  <form class="form-inline" method="GET" style="width: 100%">
                      <div class="form-group col-xs-12">
                        <label>Pilih Target Auditee</label>
                        <select class="form-control select2" name="sotk" id="filter-kantor" onchange="this.form.submit()" style="width: 100%">
                          <option value="">Pilih Target Auditee...</option>
                          <?php 
                            foreach ($target_auditee as $i => $v) {
                              ?>
                              <option <?php echo ($this->input->get('sotk') == $v ? 'selected' : '') ?> value="<?php echo $v ?>"><?php echo $target_detail[$i]['nama_sotk'].' ( '.ucfirst($target_detail[$i]['tabel']).' ) '; ?></option>
                              <?php
                            }
                          ?>
                        </select>
                      </div>
                  </form>
                </div>
            </div>


            <?php 
              if(!$error){
            ?>
            <div <?php echo ($this->input->get('sotk') == '' ? 'hidden' : '') ?> >
              <?php 
                foreach ($pertanyaan as $i => $v) {
                  if($status_rekap[$i] == '1'){ 
                    ?>
                    <div class="box box-info">
                      <div class="box-header">
                        <div class="form-group">
                          <label><span class="badge bg-yellow"><?php echo $v->kode_klausul ?></span> <?php echo $v->deskripsi ?></label>
                        </div>
                      </div>
                      <form action="<?php echo site_url('auditor/audit_acara/audit_edit/'.$this->uri->segment(4).'/'.$jawaban[$i]['id_jawaban']) ?>" method="POST">
                      <input type="hidden" name="sotk" value="<?php echo $this->input->get('sotk') ?>">
                      <input type="hidden" name="id_temuan" value="<?php echo $rekap[$i]['id_temuan'] ?>">
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Pertanyaan</label>
                              <p style="border: 1px solid #ccc; padding-top: 6px; padding-bottom: 6px; padding-left: 12px; padding-right: 12px;"><?php echo $v->pertanyaan ?></p>
                            </div>
                            <div class="form-group">
                              <label>Penjelasan Auditee</label>
                              <p style="border: 1px solid #ccc; padding-top: 6px; padding-bottom: 6px; padding-left: 12px; padding-right: 12px;"><?php echo $jawaban[$i]['jawaban'] ?></p>
                            </div>
                            <div class="form-group">
                              <label>Nilai Jawaban</label>
                              <select class="form-control" name="status_jawaban">
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==0){echo 'selected';} ?> value="0">0 Tidak Memenuhi Kriteria </option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==1){echo 'selected';} ?> value="1">1 Belum memenuhi Kriteria (Sesuai Rubrik) </option>
                                   <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==2){echo 'selected';} ?> value="2">2  Belum memenuhi Kriteria (Sesuai Rubrik)</option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==3){echo 'selected';} ?> value="3">3 Belum memenuhi Kriteria (Sesuai Rubrik) </option>
                                   <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==4){echo 'selected';} ?> value="4">4 Sudah Memenuhi Kriteria </option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==5){echo 'selected';} ?> value="5">5 Melampaui Kriteria Standar </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Catatan Auditor / Temuan</label>
                              <textarea class="form-control" name="temuan" style="resize: vertical;" ><?php echo $rekap[$i]['temuan'] ?></textarea>
                            </div>
                            <div class="form-group">
                              <label>PIC</label>
                              <input type="text" name="pic" class="form-control" value="<?php echo $rekap[$i]['pic'] ?>">
                            </div>
                            <div class="form-group">
                              <label>Rencana Perbaikan dan tindak lanjut</label>
                              <input type="text" name="perbaikan" class="form-control" value="<?php echo $rekap[$i]['perbaikan'] ?>">
                            </div>
                            <div class="form-group">
                              <?php
                                $tanggal_mulai = explode("-", $rekap[$i]['tenggat_mulai'])[1].'/'.explode("-", $rekap[$i]['tenggat_mulai'])[2].'/'.explode("-", $rekap[$i]['tenggat_mulai'])[0];
                                $tanggal_selesai = explode("-", $rekap[$i]['tenggat_selesai'])[1].'/'.explode("-", $rekap[$i]['tenggat_selesai'])[2].'/'.explode("-", $rekap[$i]['tenggat_selesai'])[0];
                                $tanggal = $tanggal_mulai.' - '.$tanggal_selesai;
                              ?>
                              <label>Tenggat Waktu</label>
                              <input type="text" name="tenggat" class="form-control tanggal-tenggat" value="<?php echo $tanggal ?>">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="box-footer">
                        <button class="btn btn-success pull-right" type="submit" <?php echo (isset($jawaban[$i]['jawaban']) ?  '' : 'disabled'); ?> >Perbarui</button>
                      </div>
                      </form>
                    </div>
                    <?php
                  }else{
                    ?>
                    <div class="box box-info">
                      <div class="box-header">
                        <div class="form-group">
                          <label><span class="badge bg-yellow"><?php echo $v->kode_klausul ?></span> <?php echo $v->deskripsi ?></label>
                        </div>
                      </div>
                      <form action="<?php echo site_url('auditor/audit_acara/audit_submit/'.$this->uri->segment(4).'/'.$jawaban[$i]['id_jawaban']) ?>" method="POST">
                      <input type="hidden" name="sotk" value="<?php echo $this->input->get('sotk') ?>">
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Pertanyaan</label>
                              <p style="border: 1px solid #ccc; padding-top: 6px; padding-bottom: 6px; padding-left: 12px; padding-right: 12px;"><?php echo $v->pertanyaan ?></p>
                            </div>
                            <div class="form-group">
                              <label>Penjelasan Auditee</label>
                              <p style="border: 1px solid #ccc; padding-top: 6px; padding-bottom: 6px; padding-left: 12px; padding-right: 12px;"><?php echo $jawaban[$i]['jawaban'] ?></p>
                            </div>
                            <div class="form-group">
                              <label>Nilai Jawaban</label>
                              <select class="form-control" name="status_jawaban">
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==0){echo 'selected';} ?> value="0">0 Tidak Memenuhi Kriteria</option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==1){echo 'selected';} ?> value="1">1 Belum memenuhi Kriteria (Sesuai Rubrik) </option>
                                   <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==2){echo 'selected';} ?> value="2">2 Belum memenuhi Kriteria (Sesuai Rubrik)</option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==3){echo 'selected';} ?> value="3">3 Belum memenuhi Kriteria (Sesuai Rubrik) </option>
                                   <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==4){echo 'selected';} ?> value="4">4 Sudah Memenuhi Kriteria </option>
                                  <option <?php if(isset($jawaban[$i]) && $jawaban[$i]['status_jawaban']==5){echo 'selected';} ?> value="5">5 Melampaui Kriteria Standar </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Catatan Auditor / Temuan</label>
                              <textarea class="form-control" name="temuan" style="resize: vertical;"></textarea>
                            </div>
                            <div class="form-group">
                              <label>PIC</label>
                              <input type="text" name="pic" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Rencana Perbaikan dan tindak lanjut</label>
                              <input type="text" name="perbaikan" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Tenggat Waktu</label>
                              <input type="text" name="tenggat" class="form-control tanggal-tenggat">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="box-footer">
                        <button class="btn <?php echo (isset($jawaban[$i]['jawaban']) ?  'btn-info' : 'btn-light'); ?> pull-right" type="submit" <?php echo (isset($jawaban[$i]['jawaban']) ?  '' : 'disabled'); ?> >Simpan</button>
                      </div>
                      </form>
                    </div>
                    <?php
                  }
                }
              ?>
            </div>
            <?php
              }
            ?>
            
        </div>
      </div>
    </section>
</div>