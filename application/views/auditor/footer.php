<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">PROFILE</li>

      <!-- Sidebar user panel -->
      <div class="user-panel">
          <div class="pull-left image">
              <img src="<?php echo base_url('assets/images/logo/profile-2.png'); ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
              <p>Auditor</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
      </div>
      <li class="treeview">
          <a href="#">
              <i class="fa fa-user"></i> <span>Menu</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo site_url('logout')?>"><i class="fa fa-sign-out"></i>Logout</a></li>
          </ul>
      </li>
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url('auditor/dashboard')?>"><i class="fa fa-home"></i> <span>Halaman Utama</span></a></li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Acara</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('auditor/lihat_acara') ?>"><i class="fa fa-circle-o"></i> Lihat acara</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file-text-o"></i> <span>Audit</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('auditor/audit_acara')?>"><i class="fa fa-circle-o"></i> Audit visitasi</a></li>
        </ul>
      </li>
      <li class="treeview">
          <a href="#">
              <i class="fa fa-pie-chart"></i>
              <span>Laporan</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo site_url('auditor/laporan') ?>"><i class="fa fa-circle-o"></i> Lihat laporan</a></li>
          </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>SOTK</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('auditor/lihat_sotk') ?>"><i class="fa fa-circle-o"></i> Lihat Sotk</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<div class="content-wrapper">

    <script type="text/javascript">
        var base_url = "<?php echo base_url() ?>";
        var site_url = "<?php echo site_url() ?>";
    </script>

    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url ('assets/bootstrap/dist/js/bootstrap.min.js'); ?> "></script>
    <!-- Date Range Picker -->
    <script src="<?php echo base_url ('assets/moment/min/moment.min.js'); ?> "></script>
    <script src="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.js'); ?> "></script>
    <!-- FastClick -->  
    <script src="<?php echo base_url ('assets/select2/dist/js/select2.full.min.js');?>"></script>
    <script src="<?php echo base_url ('assets/fastclick/lib/fastclick.js'); ?> "> </script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url ('assets/dist/js/adminlte.min.js') ?> "> </script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url ('assets/js/repeater.js') ?> "> </script>
    <!-- Sparkline -->
    <script src="<?php echo base_url ('assets/jquery-sparkline/dist/jquery.sparkline.min.js') ?> "> </script>
    <!-- jvectormap  -->
    <script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?> "> </script>
    <script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?> "> </script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url ('assets/jquery-slimscroll/jquery.slimscroll.min.js'); ?> "> </script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--     <script src="<?php echo base_url ('assets/dist/js/pages/dashboard2.js'); ?> "> </script>
 -->    
    <!-- Canvas js -->
    <script src="<?php echo base_url('assets/canvasjs/canvasjs.min.js') ?>"></script>
    <?php if(isset($url_page) && $url_page=="dashboard") { ?>
<!-- ChartJS -->
<script src="<?php echo base_url ('assets/chart.js/Chart.js'); ?> "> </script>
<?php } else if(isset($url_page) && $url_page=="lihat_acara") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
        $('#modal-share').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var name = push.data('name');
          var id = push.data('id');
          $('.modal-body #modal-nama-acara').html(name);
          $('#form-share').attr("action",site_url+"/auditor/lihat_acara/share/"+id);
        })
        $('#modal-delete').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var name = push.data('name');
          var id = push.data('id');
          $('.modal-body #modal-nama-acara-delete').html(name);
          $('#form-delete').attr("action",site_url+"/auditor/lihat_acara/hapus_acara/"+id);
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_pertanyaan") { ?>
    <script type="text/javascript">
        <?php 
        $num = sizeof($klausul);
        for ($x = 0; $x < $num; $x++) { ?>
            var pertanyaan<?php echo $x; ?> = $("<div></div>").append($("#pertanyaan<?php echo $x; ?>")).html();
            var target<?php echo $x; ?> = $("<div></div>").append($("#target<?php echo $x; ?>")).html();
        <?php } ?>
        $(function(){

            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            //$("#kolomPertanyaan<?php echo $x; ?>").html(pertanyaan<?php echo $x; ?>);
            //$("#kolomTarget<?php echo $x; ?>").html(target<?php echo $x; ?>);
            //$('#targetaudit<?php echo $x; ?>').select2();
            $(".pertanyaan<?php echo $x; ?>").height($(".target<?php echo $x; ?>").height());
            <?php } ?>
        })
        <?php 
        $num = sizeof($klausul);
        for ($x = 0; $x < $num; $x++) { ?>
        var key<?php echo $x; ?> = 0;
            $('#actionadd<?php echo $x; ?>').click(function(){
                $('#kolomPertanyaan<?php echo $x; ?>').append(pertanyaan<?php echo $x; ?>);
                $('#kolomTarget<?php echo $x; ?>').append(target<?php echo $x; ?>);
                $('.tg<?php echo $x; ?>:last').attr('name', 'targetaudit<?php echo $x; ?>['+key<?php echo $x; ?>+'][]');
                $(".pertanyaan<?php echo $x; ?>").height($(".target<?php echo $x; ?>").height());
                key<?php echo $x; ?>++;
            })
            $('#actionrmv<?php echo $x; ?>').click(function(){
                $('.pertanyaan<?php echo $x; ?>:last').remove();
                $('.target<?php echo $x; ?>:last').remove();
                key<?php echo $x; ?>--;
            })
        <?php } ?>
    </script>
<?php } else if(isset($url_page) && $url_page=="lihat_pertanyaan") { ?>
    <script type="text/javascript">

        $(function(){

            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
            $('.pertanyaan').height($('.targetaudit').height());
        })

    </script>
<?php } else if(isset($url_page) && $url_page=="ubah_pertanyaan") { ?>
    <script type="text/javascript">
        <?php 
        $num = sizeof($klausul);
        for ($x = 0; $x < $num; $x++) { ?>
            var pertanyaan<?php echo $x; ?> = '<div id="pertanyaan<?php echo $x; ?>" class="pertanyaan<?php echo $x; ?>"><textarea  name="pertanyaan<?php echo $x; ?>[]" class="form-control" style="height: 100%;width: 100%;"></textarea></div>';
            var target<?php echo $x; ?> = '<div id="target<?php echo $x; ?>" class="target<?php echo $x; ?>"><label>Pilih lebih dari satu (Ctrl+Click)</label><label>( Tidak boleh kosong )</label><select class="form-control select2 tg<?php echo $x; ?>" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $x; ?>[][]" data-placeholder="Target audit" required="" style="width: 100%;"><?php foreach ($target_audit as $i => $v) {
                                      if($v->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($v->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($v->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $v->tabel."_".$v->id;
                                      echo '<option value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                                    } ?>
                            </select></div>';
            var key<?php echo $x; ?> = <?php echo sizeof($klausul[$x]['pertanyaan']); ?>;
        <?php } ?>
        $(function(){

            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $(".pertanyaan<?php echo $x; ?>").height($(".target<?php echo $x; ?>").height());
            <?php } ?>
        })
        <?php 
        $num = sizeof($klausul);
        for ($x = 0; $x < $num; $x++) { ?>
            $('#actionadd<?php echo $x; ?>').click(function(){
                $('#kolomPertanyaan<?php echo $x; ?>').append(pertanyaan<?php echo $x; ?>);
                $('#kolomTarget<?php echo $x; ?>').append(target<?php echo $x; ?>);
                $('.tg<?php echo $x; ?>:last').attr('name', 'targetaudit<?php echo $x; ?>['+key<?php echo $x; ?>+'][]');
                $(".pertanyaan<?php echo $x; ?>").height($(".target<?php echo $x; ?>").height());
                key<?php echo $x; ?>++;
            })
            $('#actionrmv<?php echo $x; ?>').click(function(){
                $('.pertanyaan<?php echo $x; ?>:last').remove();
                $('.target<?php echo $x; ?>:last').remove();
                key<?php echo $x; ?>--;
            })
        <?php } ?>
    </script>
<?php } else if(isset($url_page) && $url_page=="laporan") { ?>
    <script type="text/javascript">
        $(function(){
            "use strict";

            /*
             * BAR CHART
             * ---------
             */

            // var bar_data = {
            //   data : [<?php foreach ($target_audit as $i => $v) { ?>['<?php echo $sotk[$i]['nama_sotk'] ?>', <?php echo $n_ratarata[$i] ?>],<?php } ?>],
            //   color: '#3c8dbc'
            // }
            $.ajax({
                method:'GET',
                dataType: 'json',
                url: '<?php echo site_url('auditor/laporan/ajax_kesiapan/'.$this->uri->segment(4)) ?>',
                success: function(resp){
                  console.log(resp);  
                    var chart = new CanvasJS.Chart("line-chart", {
                        animationEnabled: false,
                        theme: "light2", // "light1", "light2", "dark1", "dark2"
                        title: {
                            text: "Grafik Ratarata Kesiapan SOTK (<?php echo $acara['nama_acara'] ?>)"
                        },
                        axisY: {
                            title: "Grafik rata-rata Kesiapan",
                            suffix: "",
                            includeZero: false
                        },
                        axisX: {
                            title: "SOTK"
                        },
                        data: [{
                            type: "column",
                            yValueFormatString: "#,##0.0#\"\"",
                            dataPoints: resp,
                        }]
                    });
                    chart.render();
                    // console.log(chart_data);
                    // console.log(data);
                }
            })
            // var bar_data2 = {
            //   data : [<?php foreach ($target_audit as $i => $v) { ?>['<?php echo $sotk[$i]['nama_sotk'] ?>', <?php echo $n_ratarata[$i] ?>],<?php } ?>],
            //   color: '#ff0066'
            // }
            // $.plot('#line-chart', [bar_data], {
            //   grid  : {
            //     borderWidth: 1,
            //     borderColor: '#f3f3f3',
            //     tickColor  : '#f3f3f3'
            //   },
            //   series: {
            //     bars: {
            //       show    : true,
            //       barWidth: 0.3,
            //       align   : 'center'
            //     }
            //   },
            //   xaxis : {
            //     mode      : 'categories',
            //     tickLength: 0
            //   }
            // })
            /* END BAR CHART */


            $('#dataLaporanAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : true
            })
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="lihat_sotk") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataSOTK').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
        $('#modal-delete-sotk').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var nama = push.data('nama');
          var sotk = push.data('sotk');
          var tabel = sotk.split("_")[0];
          var url = "<?php echo site_url('admin/lihat_sotk/delete/') ?>"+sotk;
          $('#extra-desc').remove();
          var message="Ini adalah SOTK <strong>"+ucfirst(tabel)+"</strong>, apabila dihapus akan menghapus seluruh entitas SOTK pada <strong>"+ucfirst(tabel)+"</strong> ini";
          if(tabel=='kantor' || tabel=='direktorat' || tabel=='bagian'){
            $('#desc').append('<p id="extra-desc">'+message+'</p>');
          }else{
            $('#extra-desc').remove();
          }
          $('.modal-body #modal-nama-acara').html(nama + " ( " + ucfirst(tabel) + " )") ;
          $('#form-delete-sotk').attr('action',url);
          //console.log(push.data());
        })
        function ucfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    </script>
<?php } else if(isset($url_page) && $url_page=="audit_acara") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
          //Initialize Select2 Elements
          $('.select2').select2()
            
        })
    </script>
<?php } ?>



<script>
$(document).ready(function(){
    $("#repeater").createRepeater();
    $('#testselect').select2();
    $("#notif_alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#notif_alert").slideUp(500);
    });
        $("#tanggal").daterangepicker();
        $(".tanggal-tenggat").daterangepicker();
        $("#tanggalvisitasi").daterangepicker();
});
</script>
<!-- Data -->
<script src="<?php echo base_url ('assets/js/data.js') ?> "> </script>
</body>
</html>