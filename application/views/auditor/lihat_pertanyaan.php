<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihat Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                      <?php 
                      foreach ($klausul['kode_klausul'] as $i => $kk) { ?>
                        <tr>
                          <td>
                            <h5><strong><?php echo $kk; ?></strong></h5>
                            <p><?php echo $klausul['desc_klausul'][$i]; ?></p>
                          </td>
                          <td>
                            <?php 
                            $num_soal = 1;
                            foreach ($klausul['pertanyaan'][$i] as $j => $pt) { ?>
                              <div class="pertanyaan">
                              <p><strong><?php echo $num_soal++." )"; ?></strong> <?php echo $pt; ?></p>
                              </div>
                            <?php } ?>
                          </td>
                          <td>
                            <?php 
                            foreach ($klausul['targetaudit'][$i] as $j => $ta) { ?>
                              <div class="targetaudit">
                                <select multiple="multiple" class="form-control" style="width: 100%;">
                                  <?php foreach ($ta['sotk'] as $k => $stk) {
                                    $value = $stk['tabel'].'_'.$stk['id'];
                                    echo '<option value="'.$value.'">'.$stk['identifier'].'</option>';
                                  }?>
                                </select>
                              </div>
                            <?php } ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('auditor/lihat_acara'); ?>" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
