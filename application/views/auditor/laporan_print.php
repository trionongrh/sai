<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Print Laporan Acara <?php echo $acara['nama_acara'] ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap/dist/css/bootstrap.min.css'); ?> ">
  <!-- Date Range Picker -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.css'); ?> ">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/font-awesome/css/font-awesome.min.css'); ?> ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/Ionicons/css/ionicons.min.css'); ?> ">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/dist/css/AdminLTE.min.css'); ?> ">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.css'); ?> ">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?> ">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/select2/dist/css/select2.min.css'); ?> ">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/dist/css/skins/_all-skins.min.css'); ?> ">

    <script src="<?php echo base_url ('assets/jquery/dist/jquery.min.js'); ?> "> </script>
    <!-- DataTable -->
    <script src="<?php echo base_url ('assets/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url ('assets/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>


  <!-- Morris charts -->
  <link rel="stylesheet" href="<?php echo base_url('assets/morris.js/morris.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style type="text/css">
  	.col-print-1 {width:8%;  float:left;}
	.col-print-2 {width:16%; float:left;}
	.col-print-3 {width:25%; float:left;}
	.col-print-4 {width:33%; float:left;}
	.col-print-5 {width:42%; float:left;}
	.col-print-6 {width:50%; float:left;}
	.col-print-7 {width:58%; float:left;}
	.col-print-8 {width:66%; float:left;}
	.col-print-9 {width:75%; float:left;}
	.col-print-10{width:83%; float:left;}
	.col-print-11{width:92%; float:left;}
	.col-print-12{width:100%; float:left;}
  </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
	<div style="background: grey">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
        <small>Laporan SOTK per Acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-8">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
              <div class="box-header">
              </div>
              <div class="box-body">
                <div id="line-chart" style="height: 300px; width: 100%"></div>
              </div>
            </div>
        </div>
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataLaporanAcara" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>SOTK</th>
                      <th>Nama SOTK</th>
                      <th>Jumlah Pertanyaan</th>
                      <th>Nilai Rata-rata</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      $num = 1;
                      foreach ($target_audit as $i => $v) {
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo ucfirst($sotk[$i]['tabel']) ?></td>
                        <td><?php echo $sotk[$i]['nama_sotk'] ?></td>
                        <td style="text-align: right;"><?php echo $n_pertanyaan[$i]; ?></td>
                        <td style="text-align: right;">
                          <?php 
                            echo '<p>'.$n_ratarata[$i].'</p>'; 
                          ?>
                        </td>
                      </tr>
                      <?php 
                      } 
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
</body>
	<script type="text/javascript">
        $(function(){
            "use strict";
            $.ajax({
                method:'GET',
                dataType: 'json',
                url: '<?php echo site_url('auditor/laporan/ajax_kesiapan/'.$this->uri->segment(4)) ?>',
                success: function(resp){
                  console.log(resp);  
                    var chart = new CanvasJS.Chart("line-chart", {
                        animationEnabled: false,
                        theme: "light2", // "light1", "light2", "dark1", "dark2"
                        title: {
                            text: "Grafik Ratarata Kesiapan SOTK (<?php echo $acara['nama_acara'] ?>)"
                        },
                        axisY: {
                            title: "Grafik rata-rata Kesiapan",
                            suffix: "",
                            includeZero: false
                        },
                        axisX: {
                            title: "SOTK"
                        },
                        data: [{
                            type: "column",
                            yValueFormatString: "#,##0.0#\"\"",
                            dataPoints: resp,
                        }]
                    });
                    chart.render();
                    
                    window.print();
                    window.onafterprint = window.close();
                }
            })
        })
    </script>

	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url ('assets/bootstrap/dist/js/bootstrap.min.js'); ?> "></script>
	<!-- Date Range Picker -->
	<script src="<?php echo base_url ('assets/moment/min/moment.min.js'); ?> "></script>
	<script src="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.js'); ?> "></script>
	<!-- FastClick -->  
	<script src="<?php echo base_url ('assets/select2/dist/js/select2.full.min.js');?>"></script>
	<script src="<?php echo base_url ('assets/fastclick/lib/fastclick.js'); ?> "> </script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url ('assets/dist/js/adminlte.min.js') ?> "> </script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url ('assets/js/repeater.js') ?> "> </script>
	<!-- Sparkline -->
	<script src="<?php echo base_url ('assets/jquery-sparkline/dist/jquery.sparkline.min.js') ?> "> </script>
	<!-- jvectormap  -->
	<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?> "> </script>
	<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?> "> </script>
	<!-- SlimScroll -->
	<script src="<?php echo base_url ('assets/jquery-slimscroll/jquery.slimscroll.min.js'); ?> "> </script>

	<!-- Canvas js -->
	<script src="<?php echo base_url('assets/canvasjs/canvasjs.min.js') ?>"></script>


	<!-- FLOT CHARTS -->
	<script src="<?php echo base_url('assets/Flot/jquery.flot.js') ?>"></script>
	<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
	<script src="<?php echo base_url('assets/Flot/jquery.flot.resize.js') ?>"></script>
	<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
	<script src="<?php echo base_url('assets/Flot/jquery.flot.pie.js') ?>"></script>
	<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
	<script src="<?php echo base_url('assets/Flot/jquery.flot.categories.js') ?>"></script>

</html>