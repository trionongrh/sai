  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">PROFILE</li>

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/images/logo/profile-2.png'); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin Bardian</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-user"></i> <span>Menu</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo site_url('logout')?>"><i class="fa fa-sign-out"></i>Logout</a></li>
            </ul>
        </li>
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo site_url('auditor/dashboard')?>"><i class="fa fa-home"></i> <span>Halaman Utama</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Acara</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('auditor/lihat_acara') ?>"><i class="fa fa-circle-o"></i> Lihat acara</a></li>
            <li><a href="<?php echo site_url('auditor/buat_acara') ?>"><i class="fa fa-circle-o"></i> Buat acara</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>SOTK</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Lihat user</a></li>
          </ul>
        </li>
        <li>
          <a href="pages/calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">
  </div>

  <script src="<?php echo base_url ('assets/jquery/dist/jquery.min.js'); ?> "> </script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/bootstrap/dist/js/bootstrap.min.js'); ?> "> </script>
<!-- DataTable -->
<script src="<?php echo base_url ('assets/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url ('assets/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- Date Range Picker -->
<script src="<?php echo base_url ('assets/moment/min/moment.min.js'); ?> "></script>
<script src="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.js'); ?> "></script>
<!-- FastClick -->  
<script src="<?php echo base_url ('assets/select2/dist/js/select2.full.min.js');?>"></script>
<script src="<?php echo base_url ('assets/fastclick/lib/fastclick.js'); ?> "> </script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/dist/js/adminlte.min.js') ?> "> </script>
<!-- Sparkline -->
<script src="<?php echo base_url ('assets/jquery-sparkline/dist/jquery.sparkline.min.js') ?> "> </script>
<!-- jvectormap  -->
<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?> "> </script>
<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?> "> </script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/jquery-slimscroll/jquery.slimscroll.min.js'); ?> "> </script>
<!-- ChartJS -->
<script src="<?php echo base_url ('assets/chart.js/Chart.js'); ?> "> </script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url ('assets/dist/js/pages/dashboard2.js'); ?> "> </script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/dist/js/demo.js'); ?> "> </script>

<?php if(isset($url_page) && $url_page=="lihat_acara") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
        $('#modal-share').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var name = push.data('name');
          var id = push.data('id');
          $('.modal-body #modal-nama-acara').html(name);
          $('#form-share').attr("action",site_url+"/admin/lihat_acara/share/"+id);
        })
        $('#modal-delete').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var name = push.data('name');
          var id = push.data('id');
          $('.modal-body #modal-nama-acara-delete').html(name);
          $('#form-delete').attr("action",site_url+"/admin/lihat_acara/hapus_acara/"+id);
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            <?php } ?>
            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_acara") { ?>
    <script type="text/javascript">
        $(function(){
          $("#tanggal").daterangepicker();
        })
    </script>
<?php } ?>


<script>
$(document).ready(function(){
    $("#notif_alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#notif_alert").slideUp(500);
    });
        $("#tanggal").daterangepicker();
});
</script>