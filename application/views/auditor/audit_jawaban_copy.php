<style type="text/css">
  .legend {
    font-size: 8pt;
    margin-bottom: 2px;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit Visitasi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
                <h4></h4>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                      <?php 
                      foreach ($klausul['kode_klausul'] as $i => $kk) { ?>
                        <tr>
                          <td>
                            <h5><strong><?php echo $kk; ?></strong></h5>
                            <p><?php echo $klausul['desc_klausul'][$i]; ?></p>
                          </td>
                          <td>
                            <?php 
                            $num_soal = 1;
                            $indeks = 0;
                            $id_pertanyaan = array();
                            foreach ($klausul['pertanyaan'][$i] as $j => $pt) { ?>
                              <div class="pertanyaan">
                              <p><strong><?php echo $num_soal++." )"; ?></strong> <?php echo $pt; array_push($id_pertanyaan,$j)?></p>
                              </div>
                            <?php } ?>
                          </td>
                          <td>
                            <?php 
                            foreach ($klausul['targetaudit'][$i] as $j => $ta) { ?>
                              <div class="targetaudit">
                                <table style="width: auto;">
                                  <tr>
                                    <form class="form-inline" action="<?php echo site_url('auditor/audit_acara/auditee')?>" method="GET">
                                      <div class="input-group">
                                          <input type="hidden" name="pertanyaan" value="<?php echo $id_pertanyaan[$indeks]; ?>">
                                          <select name="sotk" class="form-control">
                                          <?php 
                                          foreach ($ta['sotk'] as $k => $stk) {
                                            $value = $stk['tabel'].'_'.$stk['id']; ?>
                                            <option value="<?php echo $value ?>"><?php echo ((isset($stk['status'])) ? '(✓)':'(☓)' ).' '.$stk['identifier'] ?></option>
                                          <?php
                                          }
                                          ?>
                                          </select>
                                          <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default btn-flat">Pilih</button>
                                          </span>
                                    </div>
                                    </form>
                                  </tr>
                                </table>
                              </div>
                            <?php
                            $indeks++;
                             } ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a onclick="window.history.back();" class="btn btn-default">Back</a>
                  <div class="pull-right">
                    <p class="legend">(✓) - Sudah dijawab auditee</p>
                    <p class="legend">(☓) - Belum dijawab auditee</p>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
