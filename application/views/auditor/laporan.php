<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
        <small>List laporan Acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataLaporanAcara" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>ISO</th>
                      <th>Nama Acara</th>
                      <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      $num = 1;
                      foreach ($acara as $i => $v) {
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo $v['nama_iso'] ?></td>
                        <td><?php echo $v['nama_acara'] ?></td>
                        <td>
                          <a href="<?php echo site_url('auditor/laporan/acara/'.$v['id_acara']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
                        </td>
                      </tr>
                      <?php 
                      } 
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>