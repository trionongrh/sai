<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th class="col-xs-2">Standar / Klausul</th>
                        <th class="col-xs-9">Pertanyaan & Target Auditee</th>
                        <?php if(!$no_action){ ?>
                        <th class="col-xs-1">Action</th>
                        <?php } ?>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h5><strong><?php echo $val->kode_klausul ?></strong></h5>
                          <p><?php echo $val->deskripsi ?></p>
                        </td>
                        <td>
                          <?php
                            $num = 1;
                            foreach ($pertanyaan[$i] as $j => $v) {
                          ?>
                          <div class="row" style="margin-left: 0px; margin-right: 0px; border-width: 1px; border-style: solid; border-radius: 5px; border-color: #5e5e5e; padding-top:5px; padding-right: 10px; margin-bottom: 2px; " >
                            <div class="pertanyaan col-xs-12 ">
                              <p><strong class="num"><?php echo 'Pertanyaan '.$num++ ?></strong></p><p style="word-break: break-all;"><?php echo $v['pertanyaan'] ?></p>
                            </div>
                            <div class="target col-xs-12" style="border-width: 1px; border-style: solid; border-radius: 5px; border-color: #aaaaaa; margin: 5px; padding-top: 5px;">
                              <label>Target Auditee</label>
                              <p style="word-break: break-all;">
                              <?php
                              foreach ($v['target'] as $k => $va) {
                                echo ($k == sizeof($v['target'])-1 ? $va['nama_sotk'] : $va['nama_sotk'].', ');
                              }
                              ?>
                              </p>
                            </div>
                          </div>
                          <?php
                            }
                          ?>
                        </td>
                        <?php if(!$no_action){ ?>
                        <td style="text-align: center;">
                          <button type="button" id="actionadd" class="btn btn-info btn-xs" title="Tambah Pertanyaan" data-id="<?php echo $val->id_klausul ?>" data-kode-klausul="<?php echo $val->kode_klausul ?>" data-desk-klausul="<?php echo $val->deskripsi ?>" data-toggle="modal" data-target="#modal-tambah-pertanyaan"><i class="fa fa-plus"></i></button>
                          <button type="button" id="actionedit" class="btn btn-success btn-xs" title="Edit Pertanyaan" data-id="<?php echo $val->id_klausul ?>" data-kode-klausul="<?php echo $val->kode_klausul ?>" data-desk-klausul="<?php echo $val->deskripsi ?>" data-toggle="modal" data-target="#modal-edit-pertanyaan-pilih"><i class="fa fa-edit"></i></button>
                          <button type="button" id="actionrmv" class="btn btn-danger btn-xs" title="Hapus Pertanyaan" data-id="<?php echo $val->id_klausul ?>" data-toggle="modal" data-target="#modal-hapus-pertanyaan-pilih"><i class="fa fa-minus"></i></button>
                        </td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('auditor/lihat_acara') ?>" class="btn btn-default">Kembali</a>
                </div>
                <!-- /.box-footer -->
 
            </div>
        </div>
      </div>
    </section>
</div>

<div class="modal fade" id="modal-tambah-pertanyaan">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-tambah-pertanyaan" action="<?php echo site_url('auditor/lihat_acara/tambah_pertanyaan') ?>" method="POST">
    <input type="hidden" name="id_acara" value="<?php echo $this->uri->segment(4) ?>">
    <input type="hidden" name="tablepage" class="tablepage">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Pertanyaan</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Klausul</label>
            <p id="tambah-klausul" style="word-break: break-all; border-width:1px; border-style: solid;border-radius: 0;box-shadow: none; border-color: #d2d6de; padding: 10px"></p>
            <input type="hidden" name="id_klausul" id="tambah-id-klausul" >
          </div>
          <div class="form-group">
            <label>Buat Pertanyaan</label>
            <textarea class="form-control" name="pertanyaan" rows="5"></textarea>
          </div>
          <div class="form-group">
            <label>Target Auditee</label>
            <label>( Tidak boleh kosong )</label>
            <select class="form-control select2" multiple="multiple" id="targetaudit-tambah" name="targetaudit[]" data-placeholder="Target audit" required=""
                    style="width: 100%;">
                    <?php foreach ($targetaudit as $i => $v) {
                      if($v->tabel == "kantor") {
                        $status = "Kantor";
                      } else if($v->tabel == "direktorat") {
                        $status = "Direktorat";
                      } else if($v->tabel == "bagian") {
                        $status = "Bagian";
                      }
                      $value = $v->tabel."_".$v->id;
                      echo '<option value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                    } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Close</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-edit-pertanyaan-pilih">
  <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Pertanyaan</h4>
        </div>
        <div class="modal-body">
          <select class="form-control" id="pilih-pertanyaan-edit">

          </select>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Close</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Submit</button>
        </div>
      </div>
  </div>
</div>

<div class="modal fade" id="modal-edit-pertanyaan">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-edit-pertanyaan" action="<?php echo site_url('auditor/lihat_acara/edit_pertanyaan') ?>" method="POST">
    <input type="hidden" name="id_acara" value="<?php echo $this->uri->segment(4) ?>">
    <input type="hidden" name="id_pertanyaan" id="id_pertanyaan-edit">
    <input type="hidden" name="tablepage" class="tablepage">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Pertanyaan</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Klausul</label>
            <p id="edit-klausul" style="word-break: break-all; border-width:1px; border-style: solid;border-radius: 0;box-shadow: none; border-color: #d2d6de; padding: 10px"></p>
          </div>
          <div class="form-group">
            <label>Buat Pertanyaan</label>
            <textarea class="form-control" name="pertanyaan" rows="5" id="pertanyaan-edit"></textarea>
          </div>
          <div class="form-group">
            <label>Target Auditee</label>
            <label>( Tidak boleh kosong )</label>
            <select class="form-control select2" multiple="multiple" id="targetaudit-edit" name="targetaudit[]" data-placeholder="Target audit" required=""
                    style="width: 100%;">
                    <?php foreach ($targetaudit as $i => $v) {
                      if($v->tabel == "kantor") {
                        $status = "Kantor";
                      } else if($v->tabel == "direktorat") {
                        $status = "Direktorat";
                      } else if($v->tabel == "bagian") {
                        $status = "Bagian";
                      }
                      $value = $v->tabel."_".$v->id;
                      echo '<option value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                    } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Close</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Submit</button>
        </div>
    </div>
    </form>
  </div>
</div>


<div class="modal fade" id="modal-hapus-pertanyaan-pilih">
  <div class="modal-dialog modal-dialog-centered">
    <form action="<?php echo site_url('auditor/lihat_acara/hapus_pertanyaan') ?>" method="POST">
    <input type="hidden" name="id_acara" value="<?php echo $this->uri->segment(4) ?>">
    <input type="hidden" name="tablepage" class="tablepage">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Pertanyaan</h4>
        </div>
        <div class="modal-body">
          <label>Pilih Pertanyaan</label>
          <select class="form-control select2" multiple="multiple" id="pilih-pertanyaan-hapus" name="id_pertanyaan[]" style="width: 100%" required="">

          </select>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Close</button>
            <button type="submit" class="btn btn-danger" onsubmit="return false"><i class="fa fa-trash"></i> Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var site_url = '<?php echo site_url() ?>';
  var base_url = '<?php echo base_url() ?>';
  var id_acara = <?php echo $this->uri->segment(4) ?>;

  var table = $('#dataAcara').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  });
  var info = table.page.info().page;
  $('.tablepage').val(info);

  $('#dataAcara').on('page.dt',function(){
      info = table.page.info().page;
      $('.tablepage').val(info);
  })

  $(function(){
    <?php echo $this->session->flashdata('script'); ?>
    $('#targetaudit-tambah').select2();
    $('#targetaudit-edit').select2();
  })

  var last_klausul = null;
  $('#modal-tambah-pertanyaan').on('show.bs.modal', function(e){
    var data = e.relatedTarget.dataset;
    if(last_klausul!=data.id){
      $('#targetaudit-tambah').val(null).trigger("change"); 
    }
    last_klausul = data.id;
    $('#tambah-id-klausul').val(data.id);
    $('#tambah-klausul').html("<strong>"+data.kodeKlausul + "</strong> <span>"+ data.deskKlausul +"</span>");
  })

  //Modal edit BEGIN
  $('#modal-edit-pertanyaan-pilih').on('show.bs.modal', function(e){
    var data = e.relatedTarget.dataset;
    var pil_per = $('#pilih-pertanyaan-edit');
    pil_per.append('<option>Pilih Pertanyaan...</option>');

    $.ajax({
        method:'GET',
        dataType: 'json',
        url: site_url+'/auditor/lihat_acara/ajax_get_pertanyaan/'+id_acara+'/'+data.id,
        success: function(resp){
          console.log(resp);
          $.each(resp, function(i,v){
            var label = 'Pertanyaan '+(i+1);
            var option = '<option value="'+v.id_pertanyaan+'">'+label+'</option>';
            pil_per.append(option);
          })
        }
    })

    
    $('#edit-klausul').html("<strong>"+data.kodeKlausul + "</strong> <span>"+ data.deskKlausul +"</span>");

    $('#pilih-pertanyaan-edit').on('change', function(e){
      var id_pertanyaan = $(this).val();

      $.ajax({
        method:'GET',
        dataType: 'json',
        url: site_url+'/auditor/lihat_acara/ajax_get_detail_pertanyaan/'+id_pertanyaan,
        success: function(resp){
          $('#pertanyaan-edit').html(resp.pertanyaan);
          $('#targetaudit-edit').val($.parseJSON(resp.target_auditee)).trigger('change');
        }
      })

      $('#id_pertanyaan-edit').val(id_pertanyaan);

      // $('#modal-edit-pertanyaan-pilih').modal('hide');
      $('#modal-edit-pertanyaan').modal('show');
    })
  })
  $('#modal-edit-pertanyaan-pilih').on('hide.bs.modal', function(e){
    var pil_per = $('#pilih-pertanyaan-edit');
    pil_per.html("");
  })
  //Modal edit END

  //Modal hapus BEGIN
   $('#modal-hapus-pertanyaan-pilih').on('show.bs.modal', function(e){
    var data = e.relatedTarget.dataset;
    var pil_per = $('#pilih-pertanyaan-hapus');

    $.ajax({
        method:'GET',
        dataType: 'json',
        url: site_url+'/auditor/lihat_acara/ajax_get_pertanyaan/'+id_acara+'/'+data.id,
        success: function(resp){
          $.each(resp, function(i,v){
            var label = 'Pertanyaan '+(i+1);
            var option = '<option value="'+v.id_pertanyaan+'">'+label+'</option>';
            pil_per.append(option);
          })
        }
    })
    $('#pilih-pertanyaan-hapus').select2();

  })
   $('#modal-hapus-pertanyaan-pilih').on('hide.bs.modal', function(e){
    var pil_per = $('#pilih-pertanyaan-hapus');
    pil_per.html("");
  })
   //Modal hapus END
</script>