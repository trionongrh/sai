<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
        <small>Laporan SOTK per Acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
              <div class="box-header">
                  <h3 class="box-title">Grafik</h3>
              </div>
              <div class="box-body">
                <div id="line-chart" style="height: 300px;"></div>
              </div>
            </div>
        </div>
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataLaporanAcara" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>SOTK</th>
                      <th>Nama SOTK</th>
                      <th>Jumlah Pertanyaan</th>
                      <th>Nilai Rata-rata</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      $num = 1;
                      foreach ($target_audit as $i => $v) {
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo ucfirst($sotk[$i]['tabel']) ?></td>
                        <td><?php echo $sotk[$i]['nama_sotk'] ?></td>
                        <td style="text-align: right;"><?php echo $n_pertanyaan[$i]; ?></td>
                        <td style="text-align: right;">
                          <?php 
                            echo '<p>'.$n_ratarata[$i].'</p>'; 
                          ?>
                        </td>
                      </tr>
                      <?php 
                      } 
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-default" onclick="window.history.back();">Back</a>
                    <a href="<?php echo site_url('auditor/laporan/print_laporan/'.$this->uri->segment(4)) ?>" class="btn btn-success pull-right" target="_blank"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>