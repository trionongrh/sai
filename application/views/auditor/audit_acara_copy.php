<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit Acara
        <small>list data acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                  <form class="form-inline" method="GET" style="width: 100%">
                      <div class="form-group col-xs-12">
                        <label>Pilih Target Auditee</label>
                        <select class="form-control select2" name="sotk" id="filter-kantor" onchange="this.form.submit()" style="width: 100%">
                          <option selected="selected" value="">Semua</option>
                          <option value="test">test</option>
                        </select>
                      </div>
                  </form>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover" hidden="">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>ISO</th>
                      <th>Nama Acara</th>
                      <th>Tanggal Acara</th>
                      <th>Pertanyaan</th>
                      <th>Publish</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>