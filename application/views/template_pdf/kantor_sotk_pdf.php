<?php 
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Daftar SOTK');
        $pdf->SetHeaderMargin('10');
        $pdf->SetFooterMargin('10');
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Satuan Audit Internal | Admin');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
		$html='<h2>Daftar SOTK Kantor</h2>
                    <table border="1" cellpadding="5">
                        <tr bgcolor="#66666">
                            <th width="100%" align="center"><b>Kantor</b></th>
                        </tr>';
        $html.='        <tr>
                            <td style="margin:20px;">'.$sotk['kantor']['nama_kantor'].'</td>
                        </tr>';
        $html.='</table>';
        if(isset($sotk['direktorat'])){
            $html.='
                        <table border="1" cellpadding="5">
                            <tr bgcolor="#66666">
                                <th width="100%" align="center"><b>Direktorat</b></th>
                            </tr>';
            foreach ($sotk['direktorat'] as $i => $vd) 
            {
                $html.='<tr>
                        <td>'.$vd['nama_direktorat'].'</td>
                        </tr>';
            }
            $html.='</table>';
            if(isset($sotk['bagian'])){
                $html.='
                            <table border="1" cellpadding="5">
                                <tr bgcolor="#66666">
                                    <th width="100%" align="center"><b>Bagian</b></th>
                                </tr>';
                foreach ($sotk['bagian'] as $i => $vd) 
                {
                    $html.='<tr>
                            <td>'.$vd['nama_bagian'].'</td>
                            </tr>';
                }
                $html.='</table>';
                if(isset($sotk['urusan'])){
                    $html.='
                                <table border="1" cellpadding="5">
                                    <tr bgcolor="#66666">
                                        <th width="100%" align="center"><b>Urusan</b></th>
                                    </tr>';
                    foreach ($sotk['urusan'] as $i => $vd) 
                    {
                        $html.='<tr>
                                <td>'.$vd['nama_urusan'].'</td>
                                </tr>';
                    }
                    $html.='</table>';
                }
            }
        }
		$pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('list_kantor_sotk_'.date("d-m-Y").'.pdf', 'I');
?>

