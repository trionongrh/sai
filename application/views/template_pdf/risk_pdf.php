<?php 
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Risk');
$pdf->SetHeaderMargin('10');
$pdf->SetFooterMargin('10');
$pdf->SetAutoPageBreak(true);
$pdf->setCellHeightRatio(2);
$pdf->SetAuthor('Satuan Audit Internal | Admin');
$pdf->SetDisplayMode('real', 'default');
$pdf->AddPage();
$num=1;
$html= <<<EOD
<h2>Risk</h2>
<table border="1" cellpadding="5">
EOD;
$background = '';
if($risk['kategori'] == 'rendah'){
    $background = '#36d836';
} else if($risk['kategori'] == 'sedang'){
    $background = '#ddff00';
} else if($risk['kategori'] == 'tinggi'){
    $background = '#dd0c08';
}
$bgtitle = '#919191';
$html.='
    <tr>
        <td width="20%" bgcolor="'.$bgtitle.'"><b>No Reg</b></td>
        <td width="50%">'.$risk['no_reg'].'</td>
        <td width="30%" bgcolor="'.$bgtitle.'" align="center"><b>Kategori Keparahan</b></td>
    </tr>
    <tr>
        <td width="20%" bgcolor="'.$bgtitle.'"><b>Bagian</b></td>
        <td width="50%">'.ucfirst($risk['nama_bagian']).'</td>
        <td width="30%" rowspan="2" bgcolor="'.$background.'" align="center" style="line-height:50px;">'.ucfirst($risk['kategori']).'</td>
    </tr>
    <tr>
        <td width="20%" bgcolor="'.$bgtitle.'"><b>Prosedur</b></td>
        <td width="50%">'.ucfirst($risk['nama_prosedur']).'</td>
    </tr>
    <tr>
        <td width="20%" rowspan="3" bgcolor="'.$bgtitle.'"><b>Resiko</b></td>
        <td width="80%" rowspan="3">'.ucfirst($risk['resiko']).'</td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
    <tr>
        <td width="20%" rowspan="3" bgcolor="'.$bgtitle.'"><b>Harapan</b></td>
        <td width="80%" rowspan="3">'.ucfirst($risk['harapan']).'</td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
    <tr>
        <td width="20%" rowspan="3" bgcolor="'.$bgtitle.'"><b>Dampak</b></td>
        <td width="80%" rowspan="3">'.ucfirst($risk['dampak']).'</td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
    <tr>
        <td width="0%"></td>
    </tr>
';
$html.=<<<EOD
</table>
EOD;

$pdf->writeHTML($html, true, false, true, false, '');
$pdf_name = 'risk_'.$reg.'_'.date("d-m-Y").'.pdf';
$pdf->Output($pdf_name, 'I');
?>

