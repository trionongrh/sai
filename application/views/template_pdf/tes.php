<style type="text/css">
    .font-normal{
        font-size: 10px;
    }
    .font-fat{
        font-size: 12px;
        font-weight: bold;
        line-height: 1;
    }
    .font-bold{
        font-size: 10px;
        font-weight: bold;
    }
    .text-center{
        text-align: center;
    }
</style>

<table cellpadding="5">
    <tr>
        <th width="100%" align="right" style="font-size: 10pt"><b><?php echo $this->session->userdata('sotk_name')['nama_sotk']; ?></b></th>
    </tr>
</table>

<table border="0" cellpadding="5">
    <tr>
        <th width="100%" align="center"></th>
    </tr>
</table>

<table border="1" cellpadding="5">
    <tr bgcolor="#CCCCCC">
        <th width="100%" align="center"><b>HASIL DAN PEMBAHASAN</b></th>
    </tr>
    <tr>
        <th width="100%" align="center"><b><?php echo $acara['nama_acara'] ?></b></th>
    </tr>
</table>

<table border="0" cellpadding="5">
    <tr>
        <th width="100%" align="center"></th>
    </tr>
</table>

<table border="1" cellpadding="5">
        <tr>
            <td width="80%" colspan="2" style="margin: 20px"><span class="font-fat"><?php echo $klausul['kode_klausul'] ?></span><p class="font-normal"><?php echo $klausul['deskripsi'] ?></p></td>
            <td width="20%" colspan="2" style="margin: 20px"><span class="font-fat text-center">Nilai</span></td>
        </tr>
        <tr>
            <td style="margin: 20px" colspan="2"><span class="font-bold"><?php echo $panduan['judul'] ?></span><p class="font-normal"><?php echo $panduan['isi'][0] ?></p><p class="font-normal"><?php echo $panduan['isi'][1] ?></p></td>
            <td style="margin: 20px; text-align:center;" colspan="2" rowspan="2"><?php
            $status = "";
            switch ($jawaban['status_jawaban']) {
              case '0':
                $status = 'Tidak Memenuhi Kriteria';
                break;
              case '1':
                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                break;
              case '2':
                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                break;
              case '3':
                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                break;
              case '4':
                $status = 'Sudah Memenuhi Kriteria';
                break;
              case '5':
                $status = 'Melampaui Kriteria Standar';
                break;
              
              default:
                $status = '';
                break;
            }
            echo $jawaban['status_jawaban'].' - '.$status;
            ?></td>
        </tr>
        <tr>
            <td style="margin: 20px" colspan="2"><span class="font-bold"><?php echo $pertanyaan['pertanyaan'] ?></span></td>
        </tr>
        <tr>
            <td style="margin: 20px"><span class="font-bold">Penjelasan Auditee (Bukti fisik dokumen atau link yang berisi bukti)</span></td>
            <td style="margin: 20px" colspan="3"><p class="font-normal"><?php echo $jawaban['jawaban'] ?></p></td>
        </tr>
        <tr>
            <td style="margin: 20px"><span class="font-bold">Catatan Kesesuaian / Ketidaksesuaian</span></td>
            <td style="margin: 20px" colspan="3"><p class="font-normal"><?php echo $rekap['temuan'] ?></p></td>
        </tr>
        <tr>
            <td style="margin: 20px"><span class="font-bold">Rencana Perbaikan & Tindak Lanjut</span></td>
            <td style="margin: 20px" colspan="3"><p class="font-normal"><?php echo $rekap['perbaikan'] ?></p></td>
        </tr>
        <tr>
            <td style="margin: 20px"><span class="font-bold">Tenggat Waktu</span></td>
            <?php
                $date = array(date_create($rekap['tenggat_mulai']),date_create($rekap['tenggat_selesai']));
            ?>
            <td style="margin: 20px" colspan="3"><p class="font-normal"><?php echo date_format($date[0], 'd/m/Y').' - '.date_format($date[1], 'd/m/Y'); ?></p></td>
        </tr>
        <tr>
            <td style="margin: 20px"><span class="font-bold">PIC</span></td>
            <td style="margin: 20px" colspan="3"><p class="font-normal"><?php echo $rekap['pic'] ?></p></td>
        </tr>
</table>