<?php 
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Daftar SOTK');
        $pdf->SetHeaderMargin('10');
        $pdf->SetFooterMargin('10');
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Satuan Audit Internal | Admin');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $num=1;
		$html='<h2>Daftar SOTK</h2>
                    <table border="1" cellpadding="5">
                        <tr bgcolor="#66666">
                            <th width="5%" align="center"><b>No</b></th>
                            <th width="25%" align="center"><b>Kantor</b></th>
                            <th width="25%" align="center"><b>Direktorat</b></th>
                            <th width="25%" align="center"><b>Bagian</b></th>
                            <th width="20%" align="center"><b>Urusan</b></th>
                        </tr>';
        foreach ($test as $i => $v) 
            {
            	if($i!=0){
            		if($test[$i-1]['kantor'] == $v['kantor']){

            		}else{
                		$num++;
            		}
            	}
                $html.='<tr>
                        <td align="center">'.$num.'</td>
                        <td>'.$v['kantor'].'</td>
                        <td>'.$v['direktorat'].'</td>
                        <td>'.$v['bagian'].'</td>
                        <td>'.$v['urusan'].'</td>
                    	</tr>';
            }
        $html.='</table>';
		$pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('list_all_sotk_'.date("d-m-Y").'.pdf', 'I');
?>

