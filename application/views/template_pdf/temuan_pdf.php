<?php 
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Temuan');
        $pdf->SetHeaderMargin('10');
        $pdf->SetFooterMargin('10');
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Satuan Audit Internal | Auditee');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
		$html='<table border="1" cellpadding="5">
                        <tr>
                            <th width="80%" style="margin:20px;"><b>Kantor</b></th>
                            <th width="20%" align="center"><b>Kantor</b></th>
                        </tr>';
        $html.='        <tr>
                            <td style="margin:20px;"></td>
                            <td style="margin:20px;"></td>
                        </tr>';
        $html.='</table>';
		$pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('temuan_'.date("d-m-Y").'.pdf', 'I');
?>

