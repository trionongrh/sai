<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
        <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('admin/lihat_user/update'); ?>" method="POST" class="form-horizontal">
                    <div class="box-body">
                        <input type="hidden" name="uid" value="<?php echo $user['uid'] ?>">
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Username</label>

                            <div class="col-sm-10">
                                    <input required="" type="text" class="form-control" id="inputUsername" name="username" placeholder="Username" value="<?php echo $user['username'] ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
                                <p>kosongkan apabila tidak ingin mengubah password</p>
                            </div>
                        </div>
                        <div id="formauditee" class="form-group">
                            <label for="inputAuditee" class="col-sm-2 control-label">Pilih auditee</label>

                            <div class="col-sm-10">
                                <select id="sotk" name="sotk" class="form-control">
                                    <?php 
                                    foreach ($audit as $key => $v) { 
                                      if($v->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($v->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($v->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $v->tabel."_".$v->id;
                                      echo '<option '.(($value == $user['sotk']) ? 'selected': '').' value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>'; 
                                    }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Save</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>