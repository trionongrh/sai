<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Input Bagian</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/buat_sotk/input_bagian')?>" class="form-horizontal" method="post" >
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-2 control-label">Nama Bagian</label>
                        <input type="hidden" name="id_kantor" value="<?php echo $id_kantor; ?>">

                        <div class="col-sm-10">
                            <div id="repeater">
                                <div class="repeater-heading" align="right">
                                    <button type="button" class="btn btn-primary repeater-add-btn">Tambah Bagian</button>
                                </div>
                                <div class="clearfix"></div>
                                    
                                <div class="items">
                                    <div class="item-content">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="Bagian" data-skip-name="true" data-name="bagian[]" required>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" data-skip-name="true" data-name="direktorat[]">
                                                    <?php 
                                                    foreach ($direktorat as $key) {
                                                        echo '<option value="'.$key->id_direktorat.'">'.$key->nama_direktorat.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button id="remove-btn" onclick="$(this).parents('.items').remove()" class="btn btn-danger">X</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">Next</button>
                </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
