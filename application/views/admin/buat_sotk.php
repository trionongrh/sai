<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-8 col-md-offset-2">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat SOTK</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/buat_sotk/add')?>" class="form-horizontal" method="post" id="form_sotk">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-3 control-label">SOTK</label>

                            <div class="col-sm-9">
                                <select class="form-control" id="selectSOTK" name="tabel">
                                    <option value="null">Pilih SOTK...</option>
                                    <?php
                                    $size_kantor = sizeof($this->db->get('kantor')->result_array());
                                    $size_direktorat = sizeof($this->db->get('direktorat')->result_array());
                                    $size_bagian = sizeof($this->db->get('bagian')->result_array());
                                    echo '<option value="kantor">Kantor</option>';
                                    if($size_kantor > 0){
                                        echo '<option value="direktorat">Direktorat</option>';    
                                    }
                                    if($size_direktorat > 0){
                                        echo '<option value="bagian">Bagian</option>';
                                    }
                                    if($size_bagian > 0){
                                        echo '<option value="urusan">Urusan</option>';    
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="formNama">
                            <label for="inputNama" class="col-sm-3 control-label" id="labelNama">Nama SOTK</label>

                            <div class="col-sm-9" id="textkantor">
                                <input type="text" class="form-control" name="" placeholder="" id="inputNama" >
                            </div>
                        </div>

                        <div class="form-group" id="formParent">
                            <label for="parent" class="col-sm-3 control-label" id="labelParent"></label>

                            <div class="col-sm-9" id="textkantor">
                                <select class="form-control" name="" id="inputParent">
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Next</button>
                    </div>
                <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

    var frmNama = $('#formNama');
    var lblName = $('#labelNama');
    var ipNama = $('#inputNama');
    var frmParent = $('#formParent');
    var lblParent = $('#labelParent');
    var ipParent = $('#inputParent');
    $( document ).ready(function() {
        frmParent.hide();
        frmNama.hide();
    });
    $( "#selectSOTK" ).change(function(e) {
        var val = e.currentTarget.value;
        switch(val) {
          case "kantor":
            frmNama.show();
            frmParent.hide();
            lblName.html('Nama Kantor');
            ipNama.attr('name', 'nama_kantor');
            ipParent.removeAttr('name');
            break;
          case "direktorat":
            ipParent.html('');
            call_ajax('kantor');
            frmNama.show();
            frmParent.show();
            lblName.html('Nama Direktorat');
            lblParent.html('Kantor');
            ipNama.attr('name', 'nama_direktorat');
            ipParent.attr('name', 'id_kantor');
            break;
          case "bagian":
            ipParent.html('');
            call_ajax('direktorat');
            frmNama.show();
            frmParent.show();
            lblName.html('Nama Bagian');
            lblParent.html('Direktorat');
            ipNama.attr('name', 'nama_bagian');
            ipParent.attr('name', 'id_direktorat');
            break;
          case "urusan":
            ipParent.html('');
            call_ajax('bagian');
            frmNama.show();
            frmParent.show();
            lblName.html('Nama Urusan');
            lblParent.html('Bagian');
            ipNama.attr('name', 'nama_urusan');
            ipParent.attr('name', 'id_bagian');
            break;
          default:
            frmParent.hide();
            frmNama.hide();
            ipNama.removeAttr('name');
            ipParent.removeAttr('name');
            break;
        }
    });
    var site_url = '<?php echo site_url(); ?>';
    var base_url = '<?php echo base_url(); ?>';
    function call_ajax(tabel){
        $.ajax({
            method:'GET',
            dataType: 'json',
            data: {q: tabel},
            url: site_url+'/admin/buat_sotk/ajax_get',
            success: function(data){
                // console.log(data);
                if(tabel == 'kantor'){
                    $.each(data, function(i,v){
                        ipParent.append('<option value="'+v.id_kantor+'">'+v.nama_kantor+'</option>');
                    })
                } else if(tabel == 'direktorat'){
                    $.each(data, function(i,v){
                        ipParent.append('<option value="'+v.id_direktorat+'">'+v.nama_direktorat+'</option>');
                    })
                } else if(tabel == 'bagian'){
                    $.each(data, function(i,v){
                        ipParent.append('<option value="'+v.id_bagian+'">'+v.nama_bagian+'</option>');
                    })
                }
            }
        })
    }
</script>