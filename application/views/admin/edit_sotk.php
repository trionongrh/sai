<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-4"></div>
        <div class="col-md-4">
        <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit SOTK <?php echo ucfirst($sotk['tabel']) ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('admin/lihat_sotk/edit'); ?>" method="POST" class="form-horizontal">
                    <input type="hidden" name="tabel" value="<?php echo $_GET['tabel'] ?>">
                    <input type="hidden" name="id_<?php echo $_GET['tabel'] ?>" value="<?php echo $_GET['id_sotk'] ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputnama" class="col-sm-2 control-label">Nama <?php echo ucfirst($sotk['tabel']); ?></label>

                            <div class="col-sm-10">
                                    <input required="" type="text" class="form-control" id="inputnama" name="nama_<?php echo $sotk['tabel']; ?>" value="<?php echo $sotk['nama_'.$sotk['tabel']] ?>">
                            </div>
                        </div>
                        <?php
                        if($sotk['tabel']=='direktorat'){ ?>

                        <div class="form-group">
                            <label for="kantor" class="col-sm-2 control-label">Pilih Kantor</label>

                            <div class="col-sm-10">
                                <select id="kantor" name="id_kantor" class="form-control">
                                    <?php 
                                    foreach ($list as $i => $val) { 
                                        if($val['id_kantor']==$sotk['id_kantor']){
                                        ?>

                                        <option selected value="<?php echo $val['id_kantor'] ?>"><?php echo $val['nama_kantor'] ?></option>
                                    <?php
                                        } else { ?>
                                        <option value="<?php echo $val['id_kantor'] ?>"><?php echo $val['nama_kantor'] ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <?php
                        } else if($sotk['tabel']=='bagian'){ ?>

                        <div class="form-group">
                            <label for="direktorat" class="col-sm-2 control-label">Pilih Direktorat</label>

                            <div class="col-sm-10">
                                <select id="direktorat" name="id_direktorat" class="form-control">
                                    <?php 
                                    foreach ($list as $i => $val) { 
                                        if($val['id_direktorat']==$sotk['id_direktorat']){
                                        ?>

                                        <option selected value="<?php echo $val['id_direktorat'] ?>"><?php echo $val['nama_direktorat'] ?></option>
                                    <?php
                                        } else { ?>
                                        <option value="<?php echo $val['id_direktorat'] ?>"><?php echo $val['nama_direktorat'] ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <?php
                        } else if($sotk['tabel']=='urusan'){ ?>

                        <div class="form-group">
                            <label for="bagian" class="col-sm-2 control-label">Pilih Bagian</label>

                            <div class="col-sm-10">
                                <select id="bagian" name="id_bagian" class="form-control">
                                    <?php 
                                    foreach ($list as $i => $val) { 
                                        if($val['id_bagian']==$sotk['id_bagian']){
                                        ?>

                                        <option selected value="<?php echo $val['id_bagian'] ?>"><?php echo $val['nama_bagian'] ?></option>
                                    <?php
                                        } else { ?>
                                        <option value="<?php echo $val['id_bagian'] ?>"><?php echo $val['nama_bagian'] ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <?php
                        } 
                        ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="<?php echo site_url('admin/lihat_sotk') ?>" class="btn btn-default pull-left">Back</a>
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>