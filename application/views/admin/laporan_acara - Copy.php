<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
        <small>Laporan SOTK per Acara</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
              <div class="box-header">
                  <h3 class="box-title">Grafik</h3>
              </div>
              <div class="box-body">
                <div id="line-chart" style="height: 300px;"></div>
              </div>
            </div>
        </div>
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
              <div class="box-header">
                  <h3 class="box-title">Data Terkecil dan Terbesar</h3>
              </div>
              <div class="box-body">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="col-xs-6 text-center"><strong>Nilai Terkecil</strong></td>
                      <td class="col-xs-6 text-center"><strong>Nilai Terbesar</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center"><?php echo $minmax['min']['nama_sotk'] ?></td>
                      <td class="text-center"><?php echo $minmax['max']['nama_sotk'] ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataLaporanAcara" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>SOTK</th>
                      <th>Nama SOTK</th>
                      <th>Jumlah Pertanyaan</th>
                      <th>Sesuai</th>
                      <th>Kesiapan</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      $num = 1;
                      foreach ($target_audit as $i => $v) {
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo ucfirst($sotk[$i]['tabel']) ?></td>
                        <td><?php echo $sotk[$i]['nama_sotk'] ?></td>
                        <td><?php echo $n_pertanyaan[$i]; ?></td>
                        <td><?php echo $n_jawaban[$i]; ?></td>
                        <td>
                          <?php 
                            echo '<p>'.$n_kesiapan[$i].'%</p>'; 
                          ?>
                          <div class="progress">
                            <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="<?php echo $n_kesiapan[$i] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $n_kesiapan[$i] ?>%">
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php 
                      } 
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-default" onclick="window.history.back();">Back</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>