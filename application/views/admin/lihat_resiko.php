<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Risk Register
        <small>list risk</small>
      </h1>


    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                  <form method="GET" class="form-inline pull-right">
                    <select id="filter-bag" name="bag" class="form-control">
                      <option value="">Semua</option>
                      <?php
                      foreach ($bagian as $bag) {
                        ?>
                        <option <?php echo (($this->input->get('bag')==$bag['id_bagian']) ? 'selected' : '') ?> value="<?php echo $bag['id_bagian'] ?>"><?php echo $bag['nama_bagian']; ?></option>
                        <?php 
                      }
                      ?>
                    </select>
                    <input type="text" name="no_reg" class="form-control" placeholder="No Reg">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    <!-- <a href="<?php echo site_url('admin/lihat_resiko/download') ?>" class="btn btn-success pull-right"><i class="fa fa-download"></i>&nbsp;Download</a> -->
                  </form>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataRisk" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="col-sm-1">NO REG</th>
                  <th>Bagian</th>
                  <th>PIC</th>
                  <th>Prosedur</th>
                  <th class="col-sm-3">Resiko</th>
                  <th class="col-sm-2">Probabilitas Keparahan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($risk as $i => $val) { ?>
                    <tr>
                      <td><?php echo $val->no_reg; ?></td>
                      <td><?php echo $val->nama_bagian; ?></td>
                      <td><?php echo $val->pic; ?></td>
                      <td><?php echo $val->nama_prosedur ?></td>
                      <td><?php echo $val->resiko ?></td>
                      <td class="text-center">
                        <?php 
                        if($val->kategori == 'rendah') {
                          echo '<span class="label bg-green">Rendah</span>';
                        } else if($val->kategori == 'sedang') { 
                          echo '<span class="label bg-yellow">Sedang</span>';
                        } else if($val->kategori == 'tinggi') { 
                          echo '<span class="label bg-red">Tinggi</span>';
                        } 
                        ?>
                      </td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a href="<?php echo site_url('admin/lihat_resiko/update/'.$val->id_risk) ?>"><i class="fa fa-edit"></i>Ubah Risk</a></li>
                            <li><a data-id_risk="<?php echo $val->id_risk; ?>" data-reg="<?php echo $val->no_reg; ?>" data-risk="<?php echo $val->resiko ?>" data-kat="<?php echo $val->kategori ?>" data-toggle="modal" data-target="#modal-delete-risk"><i class="fa fa-trash"></i>Hapus Risk</a></li>
                            <li><a href="<?php echo site_url('admin/lihat_resiko/download?risk='.$val->id_risk) ?>"><i class="fa fa-download"></i>Download Risk</a></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<div class="modal fade" id="modal-delete-risk">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-delete-risk" action="<?php echo site_url('admin/lihat_resiko/delete/') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Risk</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <p>Apakah anda yakin ingin menghapus risk : </p>
            <table>
              <tr>
                <td><strong>No Reg</strong></td>
                <td>&emsp;:&ensp;</td>
                <td><span id="text_reg"></span></td>
              </tr>
              <tr>
                <td><strong>Resiko</strong></td>
                <td>&emsp;:&ensp;</td>
                <td><span id="text_risk"></span></td>
              </tr>
              <tr>
                <td><strong>Keparahan</strong></td>
                <td>&emsp;:&ensp;</td>
                <td id="kategori_risk"></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Close</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>


<script type="text/javascript">
  $(function(){
    $('#filter-bag').change(function(){
      this.form.submit();
    })
  })
</script>