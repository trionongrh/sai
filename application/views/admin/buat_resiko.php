<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Risk Register
        <small>risk</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="<?php echo site_url('admin/buat_resiko/input_resiko')?>" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputBagian" class="col-sm-2 control-label">Bagian</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="id_bagian" id="inputBagian">
                                    <?php
                                        foreach ($bagian as $i => $v) {?>
                                            <option value="<?php echo $v->id_bagian ?>"><?php echo $v->nama_bagian?></option>
                                    <?php    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPIC" class="col-sm-2 control-label">PIC</label>
                            <div class="col-sm-4">
                                <input type="text" name="pic" placeholder="Masukan PIC" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Prosedur</label>
                            <div class="col-sm-4">
                                <select id="inputProsedur" name="prosedur" class="form-control">
                                    <?php
                                        foreach ($prosedur as $i => $v) {?>
                                            <option value="<?php echo $v->id_prosedur ?>"><?php echo $v->nama_prosedur?></option>
                                    <?php    }
                                    ?>
                                </select>
                            </div>
                            <a href="<?php echo site_url('admin/prosedur'); ?>" class="btn btn-info col-sm-2">Tambah Prosedur</a>
                        </div>
                        <div class="form-group">
                            <label for="inputResiko" class="col-sm-2 control-label">Resiko</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="resiko" placeholder="Masukkan resiko"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHarapan" class="col-sm-2 control-label">Harapan</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="harapan" placeholder="Masukkan harapan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDampak" class="col-sm-2 control-label">Dampak</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="dampak" placeholder="Masukkan dampak"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPencegahan" class="col-sm-2 control-label">Rencana Pencegahan</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="rencana_pencegahan" placeholder="Masukkan rencana pencegahan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="probabilitas" class="col-sm-2 control-label">Probabilitas</label>
                            <div class="col-sm-10">
                                <select id="probabilitas" name="probabilitas" class="form-control">
                                    <option value="0">Pilih Probabilitas</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keparahan" class="col-sm-2 control-label">Keparahan</label>
                            <div class="col-sm-10">
                                <select id="keparahan" name="keparahan" class="form-control">
                                    <option value="0">Pilih Keparahan</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kategori" class="col-sm-2 control-label">Kategori</label>
                            <div class="col-sm-3">
                                <span id="kategori" class="form-control" ></span>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" readonly="" id="textCategory" name="kategori">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        call_ajax($('#inputBagian').val());
    })
    $('#inputBagian').change(function(e){
        var id_bagian = e.currentTarget.value;
        // console.log(id_bagian);
        call_ajax(id_bagian);
    })
    function call_ajax(id_bagian){
        $.ajax({
            method:'POST',
            dataType: 'json',
            data: {id: id_bagian},
            url: site_url+'/admin/buat_resiko/ajax_get_bagian',
            success: function(data){
                // console.log(data);
                set_select(data);
            }
        })
    }

    function set_select(data){
        $('#inputProsedur').html('');
        $.each(data, function(i,e){
            $('#inputProsedur').append('<option value="'+e.id_prosedur+'">'+e.nama_prosedur+'</option>')
        })
    }
</script>