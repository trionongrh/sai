<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form action="<?php echo site_url('admin/lihat_acara/edit_pertanyaan') ?>" method="POST" class="form-horizontal">
                <input type="hidden" name="id_acara" value="<?php echo $acara->id_acara; ?>">
                <input type="hidden" name="sizeklausul" value="<?php echo sizeof($klausul); ?>">
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th>Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h5><strong><?php echo $val['kode_klausul'] ?></strong></h5>
                          <p><?php echo $val['desc_klausul'] ?></p>
                          <input type="hidden" name="id_klausul<?php echo $num; ?>" value="<?php echo $i ?>">
                        </td>
                        <td id="kolomPertanyaan<?php echo $num; ?>" class="row">
                          <?php 
                          $x = 0;
                          foreach ($val['pertanyaan'] as $j => $v) { ?>
                            <div id="pertanyaan<?php echo $num; ?>" class="pertanyaan<?php echo $num; ?>"><textarea  name="pertanyaan<?php echo $num; ?>[<?php echo $x++; ?>]" class="form-control" style="height: 100%;width: 100%;"><?php echo $v; ?></textarea></div>
                          <?php } ?>                          
                        </td>
                        <td id="kolomTarget<?php echo $num; ?>">
                          <?php 
                          $x = 0;
                          foreach ($val['pertanyaan'] as $j => $v) { ?>
                            <div id="target<?php echo $num; ?>" class="target<?php echo $num; ?>"><label>Pilih lebih dari satu (Ctrl+Click)</label><label>( Tidak boleh kosong )</label><select class="form-control select2 tg<?php echo $num; ?>" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $num; ?>[<?php echo $x++; ?>][]" data-placeholder="Target audit" required="" style="width: 100%;"><?php foreach ($target_audit as $k => $c) {
                                      if($c->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($c->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($c->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $c->tabel."_".$c->id;
                                      $found=false;
                                      foreach ($val['target_auditee'] as $l => $te) {
                                        foreach ($te as $m => $tek) {
                                          if($value==$tek){
                                            echo '<option selected value="'.$value.'">'.$c->identifier.' ( '.$status.' )</option>';
                                            $found=true;
                                            break;
                                          }
                                        }
                                        if($found){
                                          break;
                                        }
                                      }
                                      if(!$found){
                                        echo '<option value="'.$value.'">'.$c->identifier.' ( '.$status.' )</option>';
                                      }
                                    } ?>
                            </select></div>
                          <?php } ?>  
                        </td>
                        <td id="kolomAction<?php echo $num; ?>" style="text-align: center;">
                          <button type="button" id="actionadd<?php echo $num; ?>" class="btn btn-info" ><i class="fa fa-plus"></i></button>
                          <button type="button" id="actionrmv<?php echo $num++; ?>" class="btn btn-danger" ><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('admin/lihat_acara'); ?>" class="btn btn-default">Cancel</a>
                  <button type="submit" id="btnSubmit" class="btn btn-info pull-right">Submit</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
        </div>
      </div>
    </section>
</div>
