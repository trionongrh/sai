<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Buat Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form action="<?php echo site_url('admin/lihat_acara/input_pertanyaan') ?>" method="POST" class="form-horizontal">
                <input type="hidden" name="id_acara" value="<?php echo $acara->id_acara; ?>">
                <input type="hidden" name="sizeklausul" value="<?php echo sizeof($klausul); ?>">
                <div class="box-body">
                  <table id="none" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th class="col-xs-4">Standar / Klausul</th>
                        <th class="col-xs-4">Pertanyaan</th>
                        <th class="col-xs-3">Target Auditee</th>
                        <th class="col-xs-1">Aksi</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h5><strong><?php echo $val->kode_klausul ?></strong></h5>
                          <p><?php echo $val->deskripsi ?></p>
                          <input type="hidden" name="id_klausul<?php echo $num; ?>" value="<?php echo $val->id_klausul ?>">
                        </td>
                        <td id="kolomPertanyaan<?php echo $num; ?>" class="row">
                          <div id="pertanyaan<?php echo $num; ?>" class="pertanyaan<?php echo $num; ?>">
                            <textarea  name="pertanyaan<?php echo $num; ?>[]" class="form-control" style="height: 100%;width: 100%;"></textarea>
                          </div>
                        </td>
                        <td id="kolomTarget<?php echo $num; ?>">
                          <div id="target<?php echo $num; ?>" class="target<?php echo $num; ?>">
                            <label>Pilih lebih dari satu (Ctrl+Click)</label>
                            <label>( Tidak boleh kosong )</label>
                            <select class="form-control select2 tg<?php echo $num; ?>" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $num; ?>[0][]" data-placeholder="Target audit" required=""
                                    style="width: 100%;">
                                    <?php foreach ($targetaudit as $i => $v) {
                                      if($v->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($v->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($v->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $v->tabel."_".$v->id;
                                      echo '<option value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                                    } ?>
                            </select>
                          </div>
                        </td>
                        <td id="kolomAction<?php echo $num; ?>" style="text-align: center;">
                          <button type="button" id="actionadd<?php echo $num; ?>" class="btn btn-info" ><i class="fa fa-plus"></i></button>
                          <button type="button" id="actionrmv<?php echo $num++; ?>" class="btn btn-danger" ><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a onclick="window.history.back();" class="btn btn-default">Kembali</a>
                  <button type="submit" id="btnSubmit" class="btn btn-info pull-right">Submit</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
        </div>
      </div>
    </section>
</div>
