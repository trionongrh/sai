<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            echo $this->session->flashdata('msg');
        ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <!-- form start -->
            <form action="<?php echo site_url('admin/audit_acara/audit_submit')?>" class="form-horizontal" method="post" id="form_acara">
            <input type="hidden" name="id_jawaban" value="<?php echo $jawaban['id_jawaban'] ?>">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekap Jawaban Auditee</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="pertanyaan" class="col-sm-2 control-label">Pertanyaan</label>

                        <div class="col-sm-9" id="textacara">
                            <p class="form-control"><?php echo $pertanyaan['pertanyaan'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jawaban" class="col-sm-2 control-label">Penjelasan Auditee / Jawaban</label>

                        <div class="col-sm-9" id="textacara">
                            <p class="form-control"><?php echo $jawaban['jawaban'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="stsjawaban" class="col-sm-2 control-label">Status Jawaban</label>

                        <div class="col-sm-9" id="textacara">
                            <select class="form-control" name="status_jawaban">
                                <option <?php if(isset($jawaban) && $jawaban['status_jawaban']==0){echo 'selected';} ?> value="0">0 - Tidak Sesuai</option>
                                <option <?php if(isset($jawaban) && $jawaban['status_jawaban']==1){echo 'selected';} ?> value="1">1 - Sesuai</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                
                <div class="box-body">
                    <div class="form-group">
                        <label for="pertanyaan" class="col-sm-2 control-label">Catatan Auditor / Temuan</label>
                        <div class="col-sm-9" id="textacara">
                            <textarea class="form-control" name="temuan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="stsjawaban" class="col-sm-2 control-label">PIC</label>
                        <div class="col-sm-9" id="textacara">
                            <input type="text" name="pic" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jawaban" class="col-sm-2 control-label">Rencana Perbaikan dan tindak lanjut</label>
                        <div class="col-sm-9" id="textacara">
                           <input type="text" name="perbaikan" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jawaban" class="col-sm-2 control-label">Tenggat Waktu</label>
                        <div class="col-sm-9" id="textacara">
                           <input type="text" id="tanggal" name="tenggat" class="form-control">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block" <?php echo ((isset($jawaban)) ? '':'disabled') ?> >Submit</button>
                </div>
                    
            </div>
            </form>
        </div>
    </section>
</div>
