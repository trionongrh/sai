<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Input Urusan</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/buat_sotk/input_urusan')?>" class="form-horizontal" method="post" >
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputUrusan" class="col-sm-2 control-label">Nama Urusan</label>
                        
                        <div class="col-sm-10">
                            <div id="repeater">
                                <div class="repeater-heading" align="right">
                                    <button type="button" class="btn btn-primary repeater-add-btn">Tambah Urusan</button>
                                </div>
                                <div class="clearfix"></div>
                                    
                                <div class="items">
                                    <div class="item-content">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="Urusan" data-skip-name="true" data-name="urusan[]" required>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" data-skip-name="true" data-name="bagian[]">
                                                    <?php 
                                                    foreach ($bagian as $key) {
                                                        echo '<option value="'.$key->id_bagian.'">'.$key->nama_bagian.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button id="remove-btn" onclick="$(this).parents('.items').remove()" class="btn btn-danger">X</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">Next</button>
                </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
