<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Standar
        <small>list data standar</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                  <button class="btn btn-success" data-toggle="modal" data-target="#modal-tambah-iso"><i class="fa fa-plus"></i> Buat Standar</button>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataISO" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Standar</th>
                  <th>Jumlah Klausul</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $num = 1;
                  foreach ($iso as $i => $val) { ?>
                    <tr>
                      <td><?php echo $num++; ?></td>
                      <td><?php echo $val['nama_iso'] ?></td>
                      <td>
                        <?php echo sizeof($val['klausul']);?>
                      </td>
                      <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="<?php echo site_url('admin/lihat_iso/lihat_klausul/'.$val['id_iso']) ?>"><i class="fa fa-file-o"></i>Klausul</a></li>
                                <li class="divider"></li>
                                <li><a data-id="<?php echo $val['id_iso']; ?>" data-name="<?php echo $val['nama_iso'] ?>" data-toggle="modal" data-target="#modal-edit-iso"><i class="fa fa-edit"></i>Ubah Standar</a></li>
                                <li><a data-id="<?php echo $val['id_iso']; ?>" data-name="<?php echo $val['nama_iso'] ?>" data-toggle="modal" data-target="#modal-delete-iso"><i class="fa fa-trash"></i>Hapus Standar</a></li>
                            </ul>
                          </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>


<div class="modal fade" id="modal-tambah-iso">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-tambah-iso" action="<?php echo site_url('admin/buat_iso/input_iso') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Buat Standar</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Standar</label>
            <input type="text" name="nama_iso" id="inputnama_iso_tambah" class="form-control" placeholder="Masukan Nama Standar...">
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Buat</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-delete-iso">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-delete-iso" action="#" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Standar</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Standar</label>
            <p id="nama_iso_delete" class="form-control"></p>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-edit-iso">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-edit-iso" action="#" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Standar</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Standar</label>
            <input type="text" name="nama_iso" id="inputnama_iso" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>