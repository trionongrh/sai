<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat ISO</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
               <form action="<?php echo site_url('/admin/buat_iso/input_iso')?>" class="form-horizontal" method="post" id="form_iso">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pilih ISO</label>

                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="buat_baru" checked="">
                                    Buat baru
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="sudah_ada">
                                    Pilih yang sudah ada
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputiso" class="col-sm-2 control-label">ISO</label>

                            <div class="col-sm-10" id="textiso">
                                <input type="text" class="form-control" name="nama_iso" placeholder="ISO" id="inputiso" >
                            </div>
                        </div>


                            <div class="col-sm-10">
                                <div id="repeater">
                                    <div class="repeater-heading" align="right">

                                    </div>
                                    <div class="clearfix"></div>
                                        
                                    <div class="items">
                                        <div class="item-content">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                      
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Buat ISO</button>
                    </div>
                <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
