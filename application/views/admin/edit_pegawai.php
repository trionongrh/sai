<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            echo $this->session->flashdata('msg');
        ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Pegawai</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/lihat_pegawai/edit_proses')?>" class="form-horizontal" method="post" id="form_acara">
                <div class="box-body">
                    <input type="hidden" name="id_pegawai" value="<?php echo $pegawai['id_pegawai'] ?>">
                    <div class="form-group">
                        <label for="input_nama" class="col-sm-2 control-label">Nama Pegawai</label>

                        <div class="col-sm-10" id="textacara">
                            <input type="text" class="form-control" name="nama_pegawai" placeholder="Nama Pegawai" id="input_nama" value="<?php echo $pegawai['nama_pegawai'] ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="id_kantor" class="col-sm-2 control-label">Kantor</label>
                        <div class="col-sm-10">
                            <select name="id_kantor" class="form-control" id="id_kantor">
                                <?php foreach ($kantor as $key => $value) {
                                    echo '<option '.(($pegawai['id_kantor'] == $value->id_kantor) ? 'selected' : '').' value="'.$value->id_kantor.'">'.$value->nama_kantor.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">Save</button>
                </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
