<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                    <div class="box-body">
                        <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">Striped Full Width Table</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                              <table class="table table-striped">
                                <tbody><tr>
                                  <th style="width: 10px">Klausul</th>
                                  <th>Pertanyaan</th>
                                  <th>Target Auditee</th>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td>
                                        <div id="repeater">
                                            <div class="repeater-heading" align="right">
                                                <button type="button" class="btn btn-primary repeater-add-btn">Tambah pertanyaan</button>
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <div class="items">
                                                <div class="item-content">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="inputpertanyaan[]" placeholder="Pertanyaan" name="nama_pertanyaan"data-skip-name="true" data-name="pertanyaan[]" required>
                                                                <div class="input-group-btn">
                                                                    <button id="remove-btn" onclick="$(this).parents('.items').remove()" class="btn btn-danger">X</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <select class="form-control select2" multiple="multiple" id="testselect" name="test[]" data-placeholder="Select a State" style="width: 100%;">
                                            <option>Alabama</option>
                                            <option>Alaska</option>
                                            <option>California</option>
                                            <option>Delaware</option>
                                            <option>Tennessee</option>
                                            <option>Texas</option>
                                            <option>Washington</option>
                                        </select>
                                </tr>
                                <tr>
                                  <td>2.</td>
                                  <td>Clean database</td>
                                  <td>
                                    <div class="progress progress-xs">
                                      <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                                    </div>
                                  </td>
                                  <td><span class="badge bg-yellow">70%</span></td>
                                </tr>
                                <tr>
                                  <td>3.</td>
                                  <td>Cron job running</td>
                                  <td>
                                    <div class="progress progress-xs progress-striped active">
                                      <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                    </div>
                                  </td>
                                  <td><span class="badge bg-light-blue">30%</span></td>
                                </tr>
                                <tr>
                                  <td>4.</td>
                                  <td>Fix and squish bugs</td>
                                  <td>
                                    <div class="progress progress-xs progress-striped active">
                                      <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                                    </div>
                                  </td>
                                  <td><span class="badge bg-green">90%</span></td>
                                </tr>
                              </tbody></table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
