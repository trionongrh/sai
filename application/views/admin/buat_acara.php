<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            echo $this->session->flashdata('msg');
        ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Buat Acara</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/buat_acara/input_acara')?>" class="form-horizontal" method="post" id="form_acara">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-2 control-label">Nama Acara</label>

                        <div class="col-sm-10" id="textacara">
                            <input type="text" class="form-control" name="nama_acara" placeholder="Acara" id="inputacara">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-2 control-label">Kriteria Audit</label>
                        <div class="col-sm-10">
                            <select name="kriteria_audite" class="form-control">
                                <?php foreach ($dataiso as $key => $value) {
                                    echo '<option value="'.$value->id_iso.'">'.$value->nama_iso.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Tujuan Audit</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" name="tujuan_audit" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal" class="col-sm-2 control-label">Desk Evaluation</label>

                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" name="tanggal">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggalvisitasi" class="col-sm-2 control-label">Tanggal Visitasi</label>

                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggalvisitasi" name="tanggalvisitasi">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">Buat</button>
                </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
