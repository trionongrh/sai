<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
  echo $this->session->flashdata('msg');
  ?>
      <h1>
        Pegawai
        <small>list data user</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataUser" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Pegawai</th>
                      <th>Kantor</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $num = 1;
                      foreach ($pegawai as $i => $val) { ?>
                        <tr>
                          <td><?php echo $num++;?></td>
                          <td><?php echo $val['nama_pegawai'];?></td>
                          <td><?php echo $val['nama_kantor'];?></td>
                          <td>
                            <a href="<?php echo site_url('admin/lihat_pegawai/edit/'.$val['id_pegawai']) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger btn-xs" data-id="<?php echo $val['id_pegawai'] ?>" data-nama="<?php echo $val['nama_pegawai'] ?>" data-kantor="<?php echo $val['nama_kantor']; ?>" data-toggle="modal" data-target="#modal-delete-pegawai"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="box-footer">
                  <a href="<?php echo site_url('admin/buat_pegawai'); ?>" class="btn btn-default pull-right">Tambah Pegawai</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<div class="modal fade" id="modal-delete-pegawai">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus SOTK</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus pegawai : <strong id="modal-nama-pegawai"></strong></p>
      </div>
      <div class="modal-footer">
        <form action="#" id="form-delete-pegawai" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Yes</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
  var site_url = '<?php echo site_url(); ?>';
  var base_url = '<?php echo base_url(); ?>';
  $('#modal-delete-pegawai').on('show.bs.modal', function(e){
    var data = e.relatedTarget.dataset;
    console.log(data);
    $('#form-delete-pegawai').attr('action', site_url+'/admin/lihat_pegawai/delete/'+data.id);
    $('#modal-nama-pegawai').html(data.nama+' ( '+data.kantor+' )');
  })
</script>