<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Risk
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="<?php echo site_url('admin/lihat_resiko/edit')?>" method="POST">
                    <input type="hidden" name="id_risk" value="<?php echo $risk['id_risk'] ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">No Register</label>
                            <div class="col-sm-4">
                                <p class="form-control text-center"><?php echo $risk['no_reg'] ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputBagian" class="col-sm-2 control-label">Bagian</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="id_bagian" id="inputBagian">
                                    <?php
                                        foreach ($bagian as $i => $v) { ?>
                                            <option <?php echo (($risk['id_bagian']==$v->id_bagian) ? 'selected' : '') ?> value="<?php echo $v->id_bagian ?>"><?php echo $v->nama_bagian?></option>
                                    <?php
                                        } 
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPIC" class="col-sm-2 control-label">PIC</label>
                            <div class="col-sm-4">
                                <input type="text" name="pic" placeholder="Masukan PIC" class="form-control" value="<?php echo $risk['pic']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Prosedur</label>
                            <div class="col-sm-4">
                                <select id="inputProsedur" name="id_prosedur" class="form-control">
                                    <?php
                                        foreach ($prosedur as $i => $v) { ?>
                                            <option <?php echo (($risk['id_prosedur'] == $v->id_prosedur) ? 'selected' : ''); ?> value="<?php echo $v->id_prosedur ?>"><?php echo $v->nama_prosedur?></option>
                                    <?php   
                                        }
                                    ?>
                                </select>
                            </div>
                            <a href="<?php echo site_url('admin/buat_prosedur'); ?>" class="btn btn-info col-sm-2">Tambah Prosedur</a>
                        </div>
                        <div class="form-group">
                            <label for="inputResiko" class="col-sm-2 control-label">Resiko</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="resiko" placeholder="Masukkan resiko"><?php echo $risk['resiko']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHarapan" class="col-sm-2 control-label">Harapan</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="harapan" placeholder="Masukkan harapan"><?php echo $risk['harapan']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDampak" class="col-sm-2 control-label">Dampak</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="dampak" placeholder="Masukkan dampak"><?php echo $risk['dampak']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPencegahan" class="col-sm-2 control-label">Rencana Pencegahan</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" name="pencegahan" placeholder="Masukkan rencana pencegahan"><?php echo $risk['pencegahan'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="probabilitas" class="col-sm-2 control-label">Probabilitas</label>
                            <div class="col-sm-10">
                                <select id="probabilitas" name="probabilitas" class="form-control">
                                    <option value="0">Pilih Probabilitas</option>
                                    <option <?php if($risk['probabilitas']=='1'){echo 'selected';} ?> value="1">1</option>
                                    <option <?php if($risk['probabilitas']=='2'){echo 'selected';} ?> value="2">2</option>
                                    <option <?php if($risk['probabilitas']=='3'){echo 'selected';} ?> value="3">3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keparahan" class="col-sm-2 control-label">Keparahan</label>
                            <div class="col-sm-10">
                                <select id="keparahan" name="keparahan" class="form-control">
                                    <option value="0">Pilih Keparahan</option>
                                    <option <?php if($risk['keparahan']=='1'){echo 'selected';} ?> value="1">1</option>
                                    <option <?php if($risk['keparahan']=='2'){echo 'selected';} ?> value="2">2</option>
                                    <option <?php if($risk['keparahan']=='3'){echo 'selected';} ?> value="3">3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kategori" class="col-sm-2 control-label">Kategori</label>
                            <div class="col-sm-3">
                                <span id="kategori" class="form-control" ><?php echo $risk['probabilitas']*$risk['probabilitas'] ?></span>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" readonly="" id="textCategory" name="kategori" value="<?php echo $risk['kategori']; ?>">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Save</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        call_ajax($('#inputBagian').val());
    })
    $('#inputBagian').change(function(e){
        var id_bagian = e.currentTarget.value;
        // console.log(id_bagian);
        call_ajax(id_bagian);
    })
    function call_ajax(id_bagian){
        $.ajax({
            method:'POST',
            dataType: 'json',
            data: {id: id_bagian},
            url: site_url+'/admin/buat_resiko/ajax_get_bagian',
            success: function(data){
                // console.log(data);
                set_select(data);
            }
        })
    }

    function set_select(data){
        $('#inputProsedur').html('');
        $.each(data, function(i,e){
            $('#inputProsedur').append('<option value="'+e.id_prosedur+'">'+e.nama_prosedur+'</option>')
        })
    }
</script>