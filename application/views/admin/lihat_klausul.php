<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Klausul
        <small>list data klausul</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Standar : <strong><?php echo $iso['nama_iso']; ?></strong></h3>
                    <div class="form-inline pull-right" method="GET">
                      <a href="<?php echo base_url('downloads/template_db_klausul.xlsx') ?>" class="btn btn-success" ><i class="fa fa-download"></i> Download Template (Excel)</a>
                      <a data-toggle="modal" data-target="#modal-import-klausul" class="btn btn-success" ><i class="fa fa-plus"></i> Import Klausul (Excel)</a>
                      <a data-toggle="modal" data-target="#modal-tambah-klausul" class="btn btn-success" ><i class="fa fa-plus"></i> Tambah Klausul</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataKlausul" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Klausul</th>
                      <th>Deskripsi</th>
                      <th>Aksi</th>
                    </thead>
                    <tbody>
                      <?php 
                      $num = 1;
                      if(isset($klausul)){
                        foreach ($klausul as $i => $val) { ?>
                        <tr>
                          <td><?php echo $num++; ?></td>
                          <td><h4><strong><?php echo $val['kode_klausul'] ?></strong></h4></td>
                          <td>
                            <?php echo $val['deskripsi'];?>
                          </td>
                          <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <i class="fa fa-cog"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a data-id="<?php echo $val['id_klausul']; ?>" data-name="<?php echo $val['kode_klausul'] ?>" data-desc="<?php echo $val['deskripsi'] ?>" data-toggle="modal" data-target="#modal-edit-klausul"><i class="fa fa-edit"></i>Ubah Klausul</a></li>
                                    <li class="divider"></li>
                                    <li><a data-id="<?php echo $val['id_klausul']; ?>" data-name="<?php echo $val['kode_klausul'] ?>" data-desc="<?php echo $val['deskripsi'] ?>" data-toggle="modal" data-target="#modal-delete-klausul"><i class="fa fa-trash"></i>Hapus Klausul</a></li>
                                </ul>
                              </div>
                          </td>
                        </tr>
                      <?php } 

                      }?>
                      
                    </tbody>
                  </table>
                </div>

                <div class="box-footer">
                  <a href="<?php echo site_url('admin/lihat_iso'); ?>" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<div class="modal fade" id="modal-import-klausul">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-import-klausul" action="<?php echo site_url('admin/lihat_iso/import_klausul/'.$this->uri->segment(4)) ?>" method="POST" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Import Klausul</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="excel_file">File Excel ( .xls / .xlsx )</label>
            <input type="file" id="excel_file" name="excel_file" accept=".xls,.xlsx,.csv">

            <p class="help-block">Format file (.xls / .xlsx), maximum size 2MB.</p>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-tambah-klausul">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-tambah-klausul" action="<?php echo site_url('admin/lihat_iso/tambah_klausul') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Klausul</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="id_iso" value="<?php echo $iso['id_iso']; ?>">
          <div class="form-group">
            <label>Kode Klausul</label>
            <input type="text" name="kode_klausul" id="inputkode_klausul" class="form-control">
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" id="inputdeskripsi" class="form-control"></textarea>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Tambah</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-edit-klausul">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-edit-klausul" action="<?php echo site_url('admin/lihat_iso/edit_klausul/') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Klausul</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="id_iso" value="<?php echo $iso['id_iso']; ?>">
          <div class="form-group">
            <label>Kode Klausul</label>
            <input type="text" name="kode_klausul" id="editkode_klausul" class="form-control">
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" id="editdeskripsi" class="form-control"></textarea>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="modal-delete-klausul">
  <div class="modal-dialog modal-dialog-centered">
    <form id="form-delete-klausul" action="<?php echo site_url('admin/lihat_iso/delete_klausul/') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Klausul</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="id_iso" value="<?php echo $iso['id_iso']; ?>">
          <div class="form-group">
            <p>Apakah anda yakin ingin menghapus klausul : </p>
            <table>
              <tr>
                <td><strong>Kode</strong></td>
                <td>&emsp;:&ensp;</td>
                <td><span id="textkode_klausul"></span></td>
              </tr>
              <tr>
                <td><strong>Deskripsi</strong></td>
                <td>&emsp;:&ensp;</td>
                <td><span id="textdesc"></span></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Hapus</button>
        </div>
      </div>
    </form>
  </div>
</div>