<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    <?php 
    echo $this->session->flashdata('msg');
    ?>

        <h1>
            Lihat Sotk
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    	 <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <a href="<?php echo site_url('admin/lihat_sotk/download') ?>" class="btn btn-success">Download</a>
                    <form class="form-inline pull-right" method="GET">
                      <select class="form-control" name="kantor" id="filter-kantor">
                        <option value="">Semua</option>
                        <?php 
                        $kantor = $this->db->get('kantor')->result_array();
                        if(sizeof($kantor) > 0) {
                          foreach ($kantor as $v) { ?>
                            <option <?php echo (($this->input->get('kantor') == $v['id_kantor']) ? 'selected' : '') ?> value="<?php echo $v['id_kantor'] ?>"><?php echo $v['nama_kantor'] ?></option>
                        <?php 
                          }
                        }
                        ?>
                      </select>
                      <select <?php echo (($this->input->get('kantor') != '') ? 'disabled' : '') ?> class="form-control" name="sotk" id="filter-sotk">
                        <option value="">Semua</option>
                        <option <?php echo ($this->input->get('sotk')=='kantor')?'selected' : ''; ?> value="kantor">Kantor</option>
                        <option <?php echo ($this->input->get('sotk')=='direktorat')?'selected' : ''; ?> value="direktorat">Direktorat</option>
                        <option <?php echo ($this->input->get('sotk')=='bagian')?'selected' : ''; ?> value="bagian">Bagian</option>
                        <option <?php echo ($this->input->get('sotk')=='urusan')?'selected' : ''; ?> value="urusan">Urusan</option>
                      </select>
                      <input type="text" name="q" class="form-control" value="" placeholder="Nama SOTK" >
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataSOTK" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="col-xs-1">No</th>
                  <th >Level</th>
                  <th >Nama</th>
                  <th class="col-xs-2">Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                    $num =1;
                    foreach ($sotk as $i => $val) { 
                        $unique = $val['tabel'].'_'.$val['id'];
                        ?>
                        <tr>
                            <td><?php echo $num++ ?></td>
                            <td><?php echo ucfirst($val['tabel']); ?></td>
                            <td><?php echo $val['identifier'] ?></td>
                            <td>
                                <a href="<?php echo site_url('admin/lihat_sotk/edit_sotk?id_sotk='.$val['id'].'&tabel='.$val['tabel']) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger btn-xs" data-nama="<?php echo $val['identifier'] ?>" data-sotk="<?php echo $unique; ?>" data-toggle="modal" data-target="#modal-delete-sotk"><i class="fa fa-trash"></i></a>
                                <?php 
                                if($val['tabel'] == 'kantor'){?>
                                  <a href="<?php echo site_url('admin/lihat_sotk/download?kantor='.$val['id']) ?>" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<div class="modal fade" id="modal-delete-sotk">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus SOTK</h4>
      </div>
      <div class="modal-body">
        <div id="desc"></div>
        <p>Apakah anda yakin ingin menghapush sotk : <strong id="modal-nama-acara"></strong></p>
      </div>
      <div class="modal-footer">
        <form action="#" id="form-delete-sotk" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Yes</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
  $('#filter-kantor').change(function(e){
    var seldata = e.currentTarget.value;
    console.log(seldata);
    if(seldata != ''){
      $('#filter-sotk').attr('disabled', true);
      $('#filter-sotk').val('');
    }else{
      $('#filter-sotk').removeAttr('disabled');
    }
    this.form.submit();
  })
  $('#filter-sotk').change(function(){
    this.form.submit();
  })
</script>