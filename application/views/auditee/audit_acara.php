<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hasil audit
        <small>list data acara yang telah di audit oleh auditor</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ISO</th>
                  <th>Nama Acara</th>
                  <th>Tanggal Acara</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($acara as $key => $value) { ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $value['nama_iso'] ?></td>
                      <td><?php echo $value['nama_acara'] ?></td>
                      <td><?php
                        $status = "";
                        if($value['status'] == "belum_mulai"){
                          $status = '<small class="label bg-green">Belum Mulai</small>';
                        } else if ($value['status'] == "sedang_berjalan") {
                          $status = '<small class="label bg-blue">Sedang Berlangsung</small>';
                        } else if ($value['status'] == "selesai") {
                          $status = '<small class="label bg-green">Selesai</small>';
                        }
                        $date_start = date_create($value['tanggal_mulai']);
                        $date_end = date_create($value['tanggal_selesai']);
                        echo date_format($date_start, "d/m/Y").' - '.date_format($date_end, "d/m/Y").' '.$status;
                       ?></td>
                      <td>
                        <a href="<?php echo site_url('auditee/hasil_audit/acara/'.$value['id_acara']); ?>" class="btn btn-info btn-xs" title="Lihat Temuan"><i class="fa fa-eye"></i></a>
                        <a href="<?php echo site_url('auditee/hasil_audit/download_all/'.$value['id_acara']); ?>" class="btn btn-success btn-xs" title="Download Rekap"><i class="fa fa-download"></i></a>
                      </td>
                    </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
