<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php 
            echo $this->session->flashdata('msg');
        ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <!-- form start -->
            <form onsubmit="return false;" class="form-horizontal" method="post" id="form_acara">
            <input type="hidden" name="id_jawaban" value="<?php //echo $jawaban['id_jawaban'] ?>">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekap Jawaban Auditee</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pertanyaan</label>

                        <div class="col-sm-9">
                            <p class="form-control"><?php echo $pertanyaan['pertanyaan'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Penjelasan Auditee / Jawaban</label>

                        <div class="col-sm-9">
                            <p class="form-control"><?php echo $jawaban['jawaban'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status Jawaban</label>

                        <div class="col-sm-9">
                            <p class="form-control"><?php 
                            $status = "";
                            switch ($jawaban['status_jawaban']) {
                              case '0':
                                $status = 'Tidak Memenuhi Kriteria';
                                break;
                              case '1':
                                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                                break;
                              case '2':
                                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                                break;
                              case '3':
                                $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                                break;
                              case '4':
                                $status = 'Sudah Memenuhi Kriteria';
                                break;
                              case '5':
                                $status = 'Melampaui Kriteria Standar';
                                break;
                              default:
                                $status = '';
                                break;
                            }
                           echo $jawaban['status_jawaban'].' - '.$status; 
                           ?></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Hasil Audit Auditor</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Catatan Auditor / Temuan</label>
                        <div class="col-sm-9" >
                           <p class="form-control"><?php echo $rekap['temuan'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">PIC</label>
                        <div class="col-sm-9" >
                           <p class="form-control"><?php echo $rekap['pic'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Rencana Perbaikan dan tindak lanjut</label>
                        <div class="col-sm-9" >
                           <p class="form-control"><?php echo $rekap['perbaikan'] ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tenggat Waktu</label>
                        <div class="col-sm-9" >
                            <?php
                                $tanggal_mulai = explode("-", $rekap['tenggat_mulai'])[1].'/'.explode("-", $rekap['tenggat_mulai'])[2].'/'.explode("-", $rekap['tenggat_mulai'])[0];
                                $tanggal_selesai = explode("-", $rekap['tenggat_selesai'])[1].'/'.explode("-", $rekap['tenggat_selesai'])[2].'/'.explode("-", $rekap['tenggat_selesai'])[0];
                                $tanggal = $tanggal_mulai.' - '.$tanggal_selesai;
                            ?>
                           <p class="form-control"><?php echo $tanggal; ?></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a onclick="window.history.back();" class="btn btn-default">Back</a>
                    <a href="<?php echo site_url('auditee/hasil_audit/download/'.$this->uri->segment(4)); ?>" class="btn btn-primary pull-right">Download</a>
                </div>
                    
            </div>
            </form>
        </div>
    </section>
</div>
