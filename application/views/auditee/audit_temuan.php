<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hasil temuan
        <small>list data temuan yang telah di audit oleh auditor</small>
      </h1>
      <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <a href="<?php echo site_url('auditee/hasil_audit') ?>" class="btn btn-default">Back</a>
                    <a href="<?php echo site_url('auditee/hasil_audit/download_all/'.$this->uri->segment(4)) ?>" class="btn btn-success pull-right">Download Rekap</a>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <?php

                  ?>
                  <table id="dataAcara" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Klausul</th>
                  <th class="col-xs-6">Pertanyaan</th>
                  <th>Status Jawaban</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($temuan as $i => $tem) { ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><strong><?php echo $tem['kode_klausul']?></strong></td>
                      <td><?php echo $tem['pty'] ?></td>
                      <td>
                        <?php
                        $status = "";
                        switch ($tem['status_jawaban']) {
                          case '0':
                            $status = 'Tidak Memenuhi Kriteria';
                            break;
                          case '1':
                            $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                            break;
                          case '2':
                            $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                            break;
                          case '3':
                            $status = 'Belum Memenuhi Kriteria (Sesuai Rubrik)';
                            break;
                          case '4':
                            $status = 'Sudah Memenuhi Kriteria';
                            break;
                          case '5':
                            $status = 'Melampaui Kriteria Standar';
                            break;
                          
                          default:
                            $status = '';
                            break;
                        }
                        echo $tem['status_jawaban'].' - '.$status;
                        ?>
                      </td>
                      <td>
                        <a href="<?php echo site_url('auditee/hasil_audit/temuan/'.$tem['id_temuan']) ?>" class="btn btn-info btn-xs" title="Lihat Rekap"><i class="fa fa-eye"></i></a>
                        <a href="<?php echo site_url('auditee/hasil_audit/download/'.$tem['id_temuan']) ?>" class="btn btn-success btn-xs" title="Download Rekap"><i class="fa fa-download"></i></a>
                      </td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>