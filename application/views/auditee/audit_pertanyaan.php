<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit
        <small>input pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <form action="<?php echo site_url('auditee/lihat_acara/input_audit') ?>" method="POST">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>Acara</strong> : <?php echo $acara['nama_acara'] ?> <small>[ <?php echo $acara['nama_iso'] ?> ]</small></h3>
                <input type="hidden" name="id_acara" value="<?php echo $acara['id_acara'] ?>">
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Status</th>
                      </tr>
                    </thead> 
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              <!-- /.box-body -->
                <div class="box-footer">
                  <a onclick="window.history.back();" class="btn btn-default">Back</a>
                  <button type="submit" id="btnSubmit" class="btn btn-info pull-right">Simpan</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </section>
</div>
