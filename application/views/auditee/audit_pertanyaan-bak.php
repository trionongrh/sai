<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Desk Evaluation
        <small>input pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- <form action="<?php echo site_url('auditee/lihat_acara/input_audit') ?>" method="POST"> -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>Acara</strong> : <?php echo $acara['nama_acara'] ?> <small>[ <?php echo $acara['nama_iso'] ?> ]</small></h3>
                <input type="hidden" name="id_acara" value="<?php echo $acara['id_acara'] ?>">
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-8">Kolom Keterangan Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php foreach ($klausul as $i => $val) { ?>
                      <tr>
                        <td>
                          <h5><strong><?php echo $val['kode_klausul']; ?></strong></h5>
                          <p><?php echo $val['deskripsi']; ?></p>
                        </td>
                        <td>
                          <table class="table table-bordered table-hover">
                            <thead>
                              <th class="col-xs-8">Pertanyaan</th>
                              <th>Nilai Jawaban</th>
                            </thead>
                            <tbody>
                              <?php 
                              $num_soal = 1;
                              foreach ($val['pertanyaan'] as $j => $v) {
                                $query = $this->db->get_where('jawaban', array('id_pertanyaan' => $j, 'sotk' => $this->session->userdata('sotk')))->row_array();
                                if($query){
                                ?>
                                <form action="<?php echo site_url('auditee/lihat_acara/update_selected_jawaban') ?>" method="POST">
                                <tr>
                                  <input type="hidden" name="id_jawaban" value="<?php echo $query['id_jawaban'] ?>">
                                  <input type="hidden" name="id_acara" value="<?php echo $this->uri->segment(4); ?>">
                                  <td><p><strong><?php echo $num_soal++." )"; ?></strong> <?php echo $v['pertanyaan']; ?></p></td>
                                  <td>
                                    <select name="statusjawab[]" class="form-control" required="">
                                      <option value="" disabled="">Pilih Status</option>
                                      <option value="0" <?php echo ($query['status_jawaban'] == 0 ? 'selected' : '') ?> >0 Tidak Memenuhi Kriteria</option>
                                      <option value="1" <?php echo ($query['status_jawaban'] == 1 ? 'selected' : '') ?> >1 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="2" <?php echo ($query['status_jawaban'] == 2 ? 'selected' : '') ?> >2 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="3" <?php echo ($query['status_jawaban'] == 3 ? 'selected' : '') ?> >3 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="4" <?php echo ($query['status_jawaban'] == 4 ? 'selected' : '') ?> >4 Sudah Memenuhi Kriteria</option>
                                      <option value="5" <?php echo ($query['status_jawaban'] == 5 ? 'selected' : '') ?> >5 Melampaui Kriteria Standar</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td><textarea name="jawaban[]" class="form-control" style="width: 100%" placeholder="Keterangan..." ><?php echo $query['jawaban'] ?></textarea></td>
                                  <td>
                                    <?php echo $this->session->flashdata('msg-edit-'.$query['id_jawaban']) ?>
                                    <?php echo $this->session->flashdata('msg-simpan-'.$j) ?>
                                    <button type="submit" class="btn btn-success pull-right">Perbaharui</button>
                                  </td>
                                </tr>
                                </form>
                                <?php
                                }else{
                                ?>
                                <form action="<?php echo site_url('auditee/lihat_acara/simpan_selected_jawaban') ?>" method="POST">
                                <tr>
                                  <input type="hidden" name="id_pertanyaan[]" value="<?php echo $j ?>">
                                  <input type="hidden" name="id_acara" value="<?php echo $this->uri->segment(4); ?>">
                                  <td><p><strong><?php echo $num_soal++." )"; ?></strong> <?php echo $v['pertanyaan']; ?></p></td>
                                  <td>
                                    <select name="statusjawab[]" class="form-control" required="">
                                      <option value="" disabled="" selected="">Pilih Status</option>
                                      <option value="0">0 Tidak Memenuhi Kriteria</option>
                                      <option value="1">1 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="2">2 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="3">3 Belum Memenuhi Kriteria (Sesuai Rubrik)</option>
                                      <option value="4">4 Sudah Memenuhi Kriteria</option>
                                      <option value="5">5 Melampaui Kriteria Standar</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td><textarea name="jawaban[]" class="form-control" style="width: 100%" placeholder="Keterangan..." ></textarea></td>
                                  <td>
                                    <button type="submit" class="btn btn-info pull-right">Simpan</button>
                                  </td>
                                </tr>
                                </form>
                                <?php
                                }
                              }
                              ?>
                            </tbody>
                          </table>
                        </td>
                        <td>
                          
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('auditee/audit_visitasi') ?>" class="btn btn-default">Back</a>
                  <!-- <button type="submit" id="btnSubmit" class="btn btn-info pull-right">Simpan</button> -->
                </div>
            </div>
          <!-- </form> -->
        </div>
      </div>
    </section>
</div>
