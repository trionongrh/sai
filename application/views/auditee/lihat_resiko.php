<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Risk Register
        <small>list risk</small>
      </h1>


    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header form-inline">
                    <a class="btn btn-default" href="<?php echo site_url('auditee/buat_resiko') ?>">Buat Resiko</a>
                    <form method="GET" class="form-inline pull-right">
                      <input type="text" name="no_reg" class="form-control" placeholder="No Reg">
                      <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataRisk" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>NO REG</th>
                  <th>Bagian</th>
                  <th>Prosedur</th>
                  <th class="col-sm-4">Resiko</th>
                  <th class="col-sm-3">Probabilitas Keparahan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($risk as $i => $val) { ?>
                    <tr>
                      <td><?php echo $val->no_reg; ?></td>
                      <td><?php echo $val->nama_bagian; ?></td>
                      <td><?php echo $val->nama_prosedur ?></td>
                      <td><?php echo $val->resiko ?></td>
                      <td class="text-center">
                        <?php 
                        if($val->kategori == 'rendah') {
                          echo '<span class="label bg-green">Rendah</span>';
                        } else if($val->kategori == 'sedang') { 
                          echo '<span class="label bg-yellow">Sedang</span>';
                        } else if($val->kategori == 'tinggi') { 
                          echo '<span class="label bg-red">Tinggi</span>';
                        } 
                        ?>
                      </td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a href="<?php echo site_url('auditee/lihat_resiko/update/'.$val->id_risk) ?>"><i class="fa fa-edit"></i>Ubah Risk</a></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>