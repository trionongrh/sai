<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php echo $this->session->flashdata('msg'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-8">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Prosedur Risk</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table id="dataProsedur" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Prosedur</th>
                      <th>Bagian</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $num = 1;
                      foreach ($prosedur as $i => $val) { ?>
                        <tr>
                          <td><?php echo $num++;?></td>
                          <td><?php echo $val['nama_prosedur'];?></td>
                          <td><?php echo $val['nama_bagian'];?></td>
                          <td>
                            <a data-id="<?php echo $val['id_prosedur'] ?>" data-nama="<?php echo $val['nama_prosedur'] ?>" data-bagian="<?php echo $val['id_bagian'] ?>" data-toggle="modal" data-target="#modal-edit-prosedur" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger btn-xs" data-id="<?php echo $val['id_prosedur'] ?>" data-nama="<?php echo $val['nama_prosedur'] ?>" data-toggle="modal" data-target="#modal-delete-prosedur"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php 
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Prosedur Risk</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="<?php echo site_url('auditee/buat_prosedur/buat')?>" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Prosedur</label>
                            <div class="col-sm-10">
                                <input type="type" name="nama_prosedur" class="form-control" required="">
                            </div>
                        </div>
                        <input type="hidden" name="id_bagian" value="<?php echo $this->session->userdata('sotk_name')['id'] ?>">
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>


<div class="modal fade" id="modal-delete-prosedur">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Hapus Prosedur</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapush prosedur : <strong id="modal-nama-prosedur"></strong></p>
      </div>
      <div class="modal-footer">
        <form action="#" id="form-delete-prosedur" method="POST">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Yes</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-edit-prosedur">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Prosedur</h4>
      </div>
        <form action="<?php echo site_url('auditee/buat_prosedur/update') ?>" id="form-edit-prosedur" method="POST" class="form-horizontal">
      <div class="modal-body">
        <input type="hidden" name="id_prosedur" id="edit-id_prosedur">
        <div class="form-group">
            <label for="inputProsedur" class="col-sm-2 control-label">Prosedur</label>
            <div class="col-sm-10">
                <input type="type" id="edit-nama_prosedur" name="nama_prosedur" class="form-control" required="">
            </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>';
    var base_url = '<?php echo base_url() ?>';
    $('#modal-delete-prosedur').on('show.bs.modal', function(e){
        var data = e.relatedTarget.dataset;
        var id = data.id;
        console.log(data);
        $('#form-delete-prosedur').attr('action', site_url+'/auditee/buat_prosedur/delete/'+id);
        $('#modal-nama-prosedur').html(data.nama);
    })
    $('#modal-edit-prosedur').on('show.bs.modal', function(e){
        var data = e.relatedTarget.dataset;
        var id = data.id;
        console.log(data);
        $('#edit-nama_prosedur').val(data.nama);
        $('#edit-id_prosedur').val(id);
        $('#edit-id_bagian').val(data.bagian);
    })
</script>