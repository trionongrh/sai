<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit
        <small>update pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <form action="<?php echo site_url('auditee/lihat_acara/update_audit') ?>" method="POST">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>Acara</strong> : <?php echo $acara['nama_acara'] ?> <small>[ <?php echo $acara['nama_iso'] ?> ]</small></h3>
                <input type="hidden" name="id_acara" value="<?php echo $acara['id_acara'] ?>">
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Status</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php foreach ($klausul as $i => $val) { ?>
                      <tr>
                        <td>
                          <h5><strong><?php echo $val['kode_klausul']; ?></strong></h5>
                          <p><?php echo $val['deskripsi']; ?></p>
                        </td>
                        <td>
                          <?php 
                          $num_soal = 1;
                          foreach ($val['pertanyaan'] as $j => $pt) { ?>
                              <?php if(strpos($pt['target_auditee'], $_SESSION['sotk'])==true) { ?>
                              <div class="kolomPertanyaan">
                                <input type="hidden" name="id_jawaban[]" value="<?php echo $pt['id_jawaban'] ?>">
                                <p><strong><?php echo $num_soal++." )"; ?></strong> <?php echo $pt['pertanyaan']; ?></p>
                                <textarea name="jawaban[]" class="form-control jwbn" style="width: 100%" placeholder="Keterangan..." required=""><?php echo $pt['jawaban']; ?></textarea>
                              </div>
                              <?php } ?>
                          <?php } ?>
                        </td>
                        <td>
                          <?php 
                          foreach ($val['pertanyaan'] as $j => $pt) { ?>
                              <?php if(strpos($pt['target_auditee'], $_SESSION['sotk'])==true) { ?>
                              <div class="kolomStatus">
                                <select name="statusjawab[]" class="form-control">
                                  <option <?php if($pt['status_jawaban']=='0'){echo 'selected';}; ?> value="0">0 Tidak Memenuhi Kriteria</option>
                                  <option <?php if($pt['status_jawaban']=='1'){echo 'selected';}; ?> value="1">1 Belum memenuhi Kriteria (Sesuai Rubrik)</option>
                                  <option <?php if($pt['status_jawaban']=='2'){echo 'selected';}; ?> value="2">2 Belum memenuhi Kriteria (Sesuai Rubrik)</option>
                                  <option <?php if($pt['status_jawaban']=='3'){echo 'selected';}; ?> value="3">3 Belum memenuhi Kriteria (Sesuai Rubrik)</option>
                                  <option <?php if($pt['status_jawaban']=='4'){echo 'selected';}; ?> value="4">4 Sudah Memenuhi Kriteria</option>
                                  <option <?php if($pt['status_jawaban']=='5'){echo 'selected';}; ?> value="5">5 Melampaui Kriteria Standar</option>
                                </select>
                              </div>
                              <?php } ?>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              <!-- /.box-body -->
                <div class="box-footer">
                  <a onclick="window.history.back();" class="btn btn-default">Back</a>
                  <button disabled="" type="submit" id="btnSubmit" class="btn btn-info pull-right">Simpan</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </section>
</div>
