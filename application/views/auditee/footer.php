   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">PROFILE</li>

        <!-- Sidebar user panel -->
        <!-- <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/images/logo/profile-2.png'); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo ucfirst($_SESSION['sotk_name']['tabel']).' '.$_SESSION['sotk_name']['nama_sotk'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->
        <div style="width: 100%">
          <center>
            <img src="<?php echo base_url('assets/images/logo/profile-2.png'); ?>" class="img-circle" alt="User Image" style="width: 60px; margin: 5px;">
            <p style="color: white; word-wrap: break-word; white-space: initial; margin-bottom: 0px"><?php echo ucfirst($_SESSION['sotk_name']['tabel']).' '.$_SESSION['sotk_name']['nama_sotk'] ?></p>
            <a style="font-size: 8pt" href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </center>
        </div>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-user"></i> <span>Menu</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo site_url('logout')?>"><i class="fa fa-sign-out"></i>Logout</a></li>
            </ul>
        </li>
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo site_url('auditee/dashboard')?>"><i class="fa fa-home"></i> <span>Halaman Utama</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Acara</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('auditee/lihat_acara'); ?>"><i class="fa fa-circle-o"></i> Lihat acara</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i> <span>Audit</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('auditee/audit_visitasi'); ?>"><i class="fa fa-circle-o"></i> Desk Evaluation</a></li>
            <li><a href="<?php echo site_url('auditee/hasil_audit'); ?>"><i class="fa fa-circle-o"></i> Hasil Audit</a></li>
          </ul>
        </li>
        <?php if ($_SESSION['sotk_name']['tabel']=='bagian') { ?>
          
        <li class="treeview">
          <a href="#">
              <i class="fa fa-exclamation-circle"></i> <span>Daftar Resiko</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo site_url('auditee/lihat_resiko')?>"><i class="fa fa-circle-o"></i> Lihat resiko</a></li>
              <li><a href="<?php echo site_url('auditee/buat_resiko')?>"><i class="fa fa-circle-o"></i> Buat resiko</a></li>
              <li><a href="<?php echo site_url('auditee/buat_prosedur')?>"><i class="fa fa-circle-o"></i> Prosedur</a></li>

          </ul>

        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<div class="content-wrapper">
  
</div>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/bootstrap/dist/js/bootstrap.min.js'); ?> "> </script>
<!-- DataTable -->
<script src="<?php echo base_url ('assets/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url ('assets/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/select2/dist/js/select2.full.min.js');?>"></script>
<script src="<?php echo base_url ('assets/fastclick/lib/fastclick.js'); ?> "> </script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/dist/js/adminlte.min.js') ?> "> </script>
<!-- Sparkline -->
<script src="<?php echo base_url ('assets/jquery-sparkline/dist/jquery.sparkline.min.js') ?> "> </script>
<!-- jvectormap  -->
<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?> "> </script>
<script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?> "> </script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/jquery-slimscroll/jquery.slimscroll.min.js'); ?> "> </script>
<!-- ChartJS -->
<script src="<?php echo base_url ('assets/chart.js/Chart.js'); ?> "> </script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/dist/js/demo.js'); ?> "> </script>

<?php if(isset($url_page) && $url_page=="lihat_acara") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="audit_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            // $('#dataPertanyaan').DataTable({
            //   'paging'      : true,
            //   'lengthChange': false,
            //   'searching'   : false,
            //   'ordering'    : false,
            //   'info'        : true,
            //   'autoWidth'   : false
            // })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            <?php } ?>

            $('.kolomStatus').height($('.kolomPertanyaan').height());
        })

    </script>
<?php } else if(isset($url_page) && $url_page=="update_audit_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            // $('#dataPertanyaan').DataTable({
            //   'paging'      : true,
            //   'lengthChange': false,
            //   'searching'   : false,
            //   'ordering'    : true,
            //   'info'        : true,d
            //   'autoWidth'   : false
            // })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            <?php } ?>

            $('.kolomStatus').height($('.kolomPertanyaan').height());
        })

        $('.jwbn').each(function(){
            $(this).bind('input propertychange', function() {
              $('#btnSubmit').removeAttr('disabled');
              if($(this).val()==$(this).html()) {
                $('#btnSubmit').attr('disabled',true);
                //console.log('balik lagi');
              }
              //console.log($(this).val());
              //console.log($(this).html());
            })
        });
        $('select[name="statusjawab[]"]').each(function(){
            $(this).change(function() {
              $('#btnSubmit').removeAttr('disabled');
              console.log($(this).val());
            })
        });
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_resiko") { ?>
    <script type="text/javascript">
        $(function(){

        })
        $('#keparahan').on('click', function(){
          var keparahan = $('#keparahan').val();
          var probabilitas = $('#probabilitas').val();

          var hasil = keparahan * probabilitas;
          if (keparahan == 0 || probabilitas == 0) {
            $('#kategori').html('Input probabilitas atau keparahan');
            $('#textCategory').val('Error');    
          }else{
            $('#kategori').html(hasil);
            $('#textCategory').val(fuzzy(probabilitas, keparahan));
          }
        })
        $('#probabilitas').on('click', function(){
          var keparahan = $('#keparahan').val();
          var probabilitas = $('#probabilitas').val();

          var hasil = keparahan * probabilitas;
          if (keparahan == 0 || probabilitas == 0) {
            $('#kategori').html('Input probabilitas atau keparahan');
            $('#textCategory').val('Error');    
          }else{
            $('#kategori').html(hasil);
            $('#textCategory').val(fuzzy(probabilitas, keparahan));
          }
        })
        function fuzzy(l, s){
          var hasil = '';
          if (l == 1 && s == 1) {
            hasil = 'rendah';
          }else if (l == 1 && s == 2) {
            hasil = 'rendah';
          }else if (l == 1 && s == 3) {
            hasil = 'rendah';
          }else if (l == 2 && s == 1) {
            hasil = 'rendah';
          }else if (l == 2 && s == 2) {
            hasil = 'sedang';
          }else if (l == 2 && s == 3) {
            hasil = 'sedang';
          }else if (l == 3 && s == 1) {
            hasil = 'rendah';
          }else if (l == 3 && s == 2) {
            hasil = 'tinggi';
          }else if (l == 3 && s == 3) {
            hasil = 'tinggi';
          }
          return hasil;
        }
    </script>
<?php } else if(isset($url_page) && $url_page=="audit_visitasi") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false 
            })
        })
    </script>
<?php } ?>
<script>
$(document).ready(function(){
    $("#notif_alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#notif_alert").slideUp(500);
    });
});
</script>