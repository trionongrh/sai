<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> 
        Desk Evaluation
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ISO</th>
                  <th>Nama Acara</th>
                  <th>Tanggal Acara</th>
                  <th>Status Audit</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $num = 1;
                  foreach ($acara as $i => $val) { ?>
                    <tr>
                      <td><?php echo $num++; ?></td>
                      <td><?php echo $val['nama_iso'] ?></td>
                      <td>
                        <h5><strong><?php echo $val['nama_acara']; ?></strong></h5>
                        <p><?php echo $val['tujuan']; ?></p>
                      </td>
                      <td>
                      <?php
                        $status = "";
                        if($val['status'] == "belum_mulai"){
                          $status = '<small class="label bg-green">Belum Mulai</small>';
                        } else if ($val['status'] == "sedang_berjalan") {
                          $status = '<small class="label bg-blue">Sedang Berlangsung</small>';
                        } else if ($val['status'] == "selesai") {
                          $status = '<small class="label bg-green">Selesai</small>';
                        }
                        $date_start = date_create($val['tanggal_mulai']);
                        $date_end = date_create($val['tanggal_selesai']);
                        echo date_format($date_start, "d/m/Y").' - '.date_format($date_end, "d/m/Y").' '.$status;
                      ?>
                      </td>
                      <td>
                        <?php 
                        if(isset($jawaban[$i])) {
                          echo '<span class="label bg-green">Sudah</span>';
                        } else { 
                          echo '<span class="label bg-yellow">Belum</span>';
                        } 
                        ?>
                      </td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right" role="menu">
                          <?php if($_SESSION['level'] == 'auditee') { ?>
                            <?php if(isset($jawaban[$i])) { ?>
                              <li><a href="<?php echo site_url('auditee/lihat_acara/audit_pertanyaan/'.$val['id_acara']) ?>"><i class="fa fa-file-o"></i>Lihat / Ubah Jawaban</a></li>
                            <?php } else { ?>
                              <li><a href="<?php echo site_url('auditee/lihat_acara/audit_pertanyaan/'.$val['id_acara']) ?>"><i class="fa fa-file-o"></i>Input Jawaban</a></li>
                            <?php } ?>
                          <?php } ?>
                            </ul>
                          </div>
                        </td>
                    </tr>    
                  <?php } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>