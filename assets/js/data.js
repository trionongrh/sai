function getKantor(){
	$.ajax({
		type : "GET",
		url : site_url+"/admin/buat_sotk/get_all_kantor",
		dataType:"json",
		success: function(result) {
			var ipkntr = $("#inputkantor");
			ipkntr.empty();
			result.forEach(function(data){
				var option = $('<option></option>').attr("value",data.id_kantor).text(data.nama_kantor);
				ipkntr.append(option);	
			});
		}
	})
}

function getIso(){
	$.ajax({
		type : "GET",
		url : site_url+"/admin/buat_iso/get_all_iso",
		dataType:"json",
		success: function(result) {
			var iptiso = $("#inputiso");
			iptiso.empty();
			result.forEach(function(data){
				var option = $('<option></option>').attr("value",data.id_iso).text(data.nama_iso);
				iptiso.append(option);	
			});
		}
	})
}